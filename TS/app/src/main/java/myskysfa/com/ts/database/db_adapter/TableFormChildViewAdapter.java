package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.ts.database.TableFormChildView;
import myskysfa.com.ts.utils.DatabaseManager;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/14/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableFormChildViewAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableFormChildViewAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableFormChildView> getDatabyCondition(Context context, String condition, Object param) {
        List<TableFormChildView> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormChildView.class);

            QueryBuilder<TableFormChildView, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFormChildView, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableFormChildView> getAllData(Context context) {
        List<TableFormChildView> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormChildView.class);

            QueryBuilder<TableFormChildView, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableFormChildView table, String no_ticket, String value_profile, String value_emergency,
                           String value_payment, String value_package) {
        try {
            table.setfNO_TICKET(no_ticket);
            table.setfVALUE_PROFILE(value_profile);
            table.setfVALUE_EMERGENCY(value_emergency);
            table.setfVALUE_PAYMENT(value_payment);
            table.setfVALUE_PACKAGE(value_package);
            getHelper().getTableFormChildViewDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormChildView.class);

            UpdateBuilder<TableFormChildView, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String no_ticket, String value_profile, String value_emergency,
                              String value_payment, String value_package, String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormChildView.class);

            UpdateBuilder<TableFormChildView, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFormChildView.fNO_TICKET, no_ticket);
            updateBuilder.updateColumnValue(TableFormChildView.fVALUE_PROFILE, value_profile);
            updateBuilder.updateColumnValue(TableFormChildView.fVALUE_EMERGENCY, value_emergency);
            updateBuilder.updateColumnValue(TableFormChildView.fVALUE_PAYMENT, value_payment);
            updateBuilder.updateColumnValue(TableFormChildView.fVALUE_PACKAGE, value_package);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormChildView.class);

            DeleteBuilder<TableFormChildView, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableFormChildView> listTable = null;
        try {
            listTable = getHelper().getTableFormChildViewDAO().queryForAll();
            getHelper().getTableFormChildViewDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
