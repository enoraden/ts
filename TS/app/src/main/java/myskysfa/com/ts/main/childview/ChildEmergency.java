package myskysfa.com.ts.main.childview;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.MasterAPI;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/16/2016.
|---------------------------------------------------------------------------------------------------
*/
public class ChildEmergency {
    private static Context context;
    private Utils utils;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private TextView statusdata;
    private TextView no_ticket, nama_depan, nama_tengah, nama_belakang, hubungan, notlp, alamat;

    public ChildEmergency(Context _context) {
        this.context = _context;
    }

    public void emergencyView(View root) {
        loadResorce(root);
        loadListener();
        new getChildEmergency().execute();
    }
    private void loadResorce(View root) {
        statusdata      = (TextView) root.findViewById(R.id.chdemr_statusdata);
        //------------------------------------------------------------------------------------------
        no_ticket       = (TextView) root.findViewById(R.id.chdemr_no_ticket);
        nama_depan      = (TextView) root.findViewById(R.id.chdemr_nama_depan);
        nama_tengah     = (TextView) root.findViewById(R.id.chdemr_nama_tengah);
        nama_belakang   = (TextView) root.findViewById(R.id.chdemr_nama_belakang);
        hubungan        = (TextView) root.findViewById(R.id.chdemr_hubungan);
        notlp           = (TextView) root.findViewById(R.id.chdemr_notlp);
        alamat          = (TextView) root.findViewById(R.id.chdemr_alamat);
    }

    private void loadListener() {
        no_ticket.setText("-");
        nama_depan.setText("-");
        nama_tengah.setText("-");
        nama_belakang.setText("-");
        hubungan.setText("-");
        notlp.setText("-");
        alamat.setText("-");
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data Child Emergency Server
    |-----------------------------------------------------------------------------------------------
    */
    private class getChildEmergency extends AsyncTask<String, String, String> {
        private Boolean _status;
        private String _imei;
        private int _signal;

        private SharedPreferences preferences;
        private String _ticket_number;

        private String js_prospect_nbr, js_emergency_first_name, js_emergency_middle_name, js_emergency_last_name,
                js_emergency_phone, js_emergency_address, js_relation;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils       = new Utils(context);
            preferences = context.getSharedPreferences(context.getString(R.string.detail_id), Context.MODE_PRIVATE);
            _signal     = utils.checkSignal();
            _imei       = utils.getIMEI();
            _status     = false;

            statusdata.setText("Loading . . .");
            statusdata.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(context);
                listLogLogin        = dbAdapterLogLogin.getAllData(context);

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_DETAIL_PROFILE;
                    _ticket_number      = preferences.getString("ticket_number", null);
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl    = _url + "/" + _ticket_number;

                    String _response = ConnectionManager.requestDataMaster(_paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status = jsonObject.getBoolean("status");

                        if (_status) {
                            String _data        = jsonObject.getString("data");
                            JSONObject objData  = new JSONObject(_data.toString());

                            js_prospect_nbr     = objData.getString("prospect_nbr");
                            //----------------------------------------------------------------------
                            String _emergency_contact       = jsonObject.getString("emergency_contact");
                            JSONObject objEmergencyContact  = new JSONObject(_emergency_contact.toString());

                            js_emergency_first_name     = objEmergencyContact.getString("emergency_first_name");
                            js_emergency_middle_name    = objEmergencyContact.getString("emergency_middle_name");
                            js_emergency_last_name      = objEmergencyContact.getString("emergency_last_name");
                            js_emergency_phone          = objEmergencyContact.getString("emergency_phone");
                            js_emergency_address        = objEmergencyContact.getString("emergency_address");
                            js_relation                 = objEmergencyContact.getString("relation");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_status) {
                    statusdata.setText("Finish");
                    statusdata.setVisibility(View.GONE);
                    //------------------------------------------------------------------------------
                    no_ticket.setText(_ticket_number);
                    nama_depan.setText(js_emergency_first_name);
                    nama_tengah.setText(js_emergency_middle_name);
                    nama_belakang.setText(js_emergency_last_name);
                    hubungan.setText(js_relation);
                    notlp.setText(js_emergency_phone);
                    alamat.setText(js_emergency_address);
                } else {
                    statusdata.setText("Gagal terhubung ke server, silahkan dicoba kembali.");
                    statusdata.setVisibility(View.VISIBLE);
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "detail_inst_troub");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}