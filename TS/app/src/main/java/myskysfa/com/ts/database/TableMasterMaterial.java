package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 1/11/2017.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "m_material")
public class TableMasterMaterial {
    public static final String TABLE_NAME           = "m_material";

    public static final String fNEW_ID              = "new_id";
    public static final String fITEM_ID             = "item_id";
    public static final String fITEM_CODE           = "item_code";
    public static final String fITEM_DESCR          = "item_descr";
    public static final String fITEM_CLASS_CODE     = "item_class_code";
    public static final String fITEM_TYPE_CODE      = "item_type_code";
    public static final String fUOM_CODE            = "uom_code";
    public static final String fSTATUS              = "status";
    public static final String fSERIALIZED          = "serialized";
    public static final String fSELLABLE            = "sellable";
    public static final String fLOANABLE            = "loanable";
    public static final String fRENTABLE            = "rentable";
    public static final String fWARRANTY            = "warranty";
    public static final String fCREATE_BY           = "created_by";
    public static final String fCREATE_DATE         = "created_date";
    public static final String fLAST_UPD_BY         = "last_upd_by";
    public static final String fLAST_UPD_DATE       = "last_upd_date";
    public static final String fITEM_TYPE_ID        = "item_type_id";
    public static final String fPLAN_ID             = "plan_id";
    public static final String fUSED_ITEM_WARRANTY  = "used_item_warranty";
    public static final String fREPAIRABLE          = "repairable";
    public static final String fIS_TAX_INCLUDED     = "is_tax_included";
    public static final String fCHARGE_COMPONENT    = "charge_component";
    public static final String fCHARGE_DESC         = "charge_desc";
    public static final String fRATE                = "rate";

    @DatabaseField(generatedId = true)
    private int new_id;

    @DatabaseField
    private String item_id, item_code, item_descr, item_class_code, item_type_code, uom_code,
            status, serialized, sellable, loanable, rentable, warranty, created_by, created_date,
            last_upd_by, last_upd_date, item_type_id, plan_id, used_item_warranty, repairable,
            is_tax_included, charge_component, charge_desc, rate;

    public void setfNEW_ID(int new_id) {
        this.new_id = new_id;
    }
    public int getfNEW_ID() {
        return new_id;
    }

    public void setfITEM_ID(String item_id) {
        this.item_id = item_id;
    }
    public String getfITEM_ID() {
        return item_id;
    }

    public void setfITEM_CODE(String item_code) {
        this.item_code = item_code;
    }
    public String getfITEM_CODE() {
        return item_code;
    }

    public void setfITEM_DESCR(String item_descr) {
        this.item_descr = item_descr;
    }
    public String getfITEM_DESCR() {
        return item_descr;
    }

    public void setfITEM_CLASS_CODE(String item_class_code) {
        this.item_class_code = item_class_code;
    }
    public String getfITEM_CLASS_CODE() {
        return item_class_code;
    }

    public void setfITEM_TYPE_CODE(String item_type_code) {
        this.item_type_code = item_type_code;
    }
    public String getfITEM_TYPE_CODE() {
        return item_type_code;
    }

    public void setfUOM_CODE(String uom_code) {
        this.uom_code = uom_code;
    }
    public String getfUOM_CODE() {
        return uom_code;
    }

    public void setfSTATUS(String status) {
        this.status = status;
    }
    public String getfSTATUS() {
        return status;
    }

    public void setfSERIALIZED(String serialized) {
        this.serialized = serialized;
    }
    public String getfSERIALIZED() {
        return serialized;
    }

    public void setfSELLABLE(String sellable) {
        this.sellable = sellable;
    }
    public String getfSELLABLE() {
        return sellable;
    }

    public void setfLOANABLE(String loanable) {
        this.loanable = loanable;
    }
    public String getfLOANABLE() {
        return loanable;
    }

    public void setfRENTABLE(String rentable) {
        this.rentable = rentable;
    }
    public String getfRENTABLE() {
        return rentable;
    }

    public void setfWARRANTY(String warranty) {
        this.warranty = warranty;
    }
    public String getfWARRANTY() {
        return warranty;
    }

    public void setfCREATE_BY(String created_by) {
        this.created_by = created_by;
    }
    public String getfCREATE_BY() {
        return created_by;
    }

    public void setfCREATE_DATE(String created_date) {
        this.created_date = created_date;
    }
    public String getfCREATE_DATE() {
        return created_date;
    }

    public void setfLAST_UPD_BY(String last_upd_by) {
        this.last_upd_by = last_upd_by;
    }
    public String getfLAST_UPD_BY() {
        return last_upd_by;
    }

    public void setfLAST_UPD_DATE(String last_upd_date) {
        this.last_upd_date = last_upd_date;
    }
    public String getfLAST_UPD_DATE() {
        return last_upd_date;
    }

    public void setfITEM_TYPE_ID(String item_type_id) {
        this.item_type_id = item_type_id;
    }
    public String getfITEM_TYPE_ID() {
        return item_type_id;
    }

    public void setfPLAN_ID(String plan_id) {
        this.plan_id = plan_id;
    }
    public String getfPLAN_ID() {
        return plan_id;
    }

    public void setfUSED_ITEM_WARRANTY(String used_item_warranty) {
        this.used_item_warranty = used_item_warranty;
    }
    public String getfUSED_ITEM_WARRANTY() {
        return used_item_warranty;
    }

    public void setfREPAIRABLE(String repairable) {
        this.repairable = repairable;
    }
    public String getfREPAIRABLE() {
        return repairable;
    }

    public void setfIS_TAX_INCLUDED(String is_tax_included) {
        this.is_tax_included = is_tax_included;
    }
    public String getfIS_TAX_INCLUDED() {
        return is_tax_included;
    }

    public void setfCHARGE_COMPONENT(String charge_component) {
        this.charge_component = charge_component;
    }
    public String getfCHARGE_COMPONENT() {
        return charge_component;
    }

    public void setfCHARGE_DESC(String charge_desc) {
        this.charge_desc = charge_desc;
    }
    public String getfCHARGE_DESC() {
        return charge_desc;
    }

    public void setfRATE(String rate) {
        this.rate = rate;
    }
    public String getfRATE() {
        return rate;
    }
}