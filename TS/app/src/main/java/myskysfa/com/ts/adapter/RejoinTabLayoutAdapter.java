package myskysfa.com.ts.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import myskysfa.com.ts.main.rejoin.BAP;
import myskysfa.com.ts.main.rejoin.Profile;
import myskysfa.com.ts.main.rejoin.Result;
import myskysfa.com.ts.main.rejoin.Signature;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/27/2016.
|---------------------------------------------------------------------------------------------------
*/
public class RejoinTabLayoutAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public RejoinTabLayoutAdapter(FragmentManager manager, int NumOfTabs) {
        super(manager);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Profile profile = new Profile();
                return profile;

            case 1:
                BAP bap = new BAP();
                return bap;

            case 2:
                Signature signature = new Signature();
                return signature;

            case 3:
                Result result = new Result();
                return result;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
