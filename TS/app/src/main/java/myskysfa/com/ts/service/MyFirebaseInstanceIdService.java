package myskysfa.com.ts.service;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import myskysfa.com.ts.R;

/**
 * Created by admin on 10/4/2016.
 */
public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {
    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        //Displaying token on logcat
        Log.d(TAG, "Refreshed fcm_token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);

    }

    private void sendRegistrationToServer(String fcm_token) {
        //You can implement this method to store the token on your server
        //Not required for current project
        SharedPreferences preferences           = getSharedPreferences("fcm_token", Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor    = preferences.edit();
        shareEditor.putString("fcm_token", fcm_token);
        shareEditor.commit();
    }
}
