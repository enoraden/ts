package myskysfa.com.ts.main.sliding;

import android.app.Dialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import myskysfa.com.ts.R;
import myskysfa.com.ts.utils.Utils;
import myskysfa.com.ts.widget.SignatureView;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Signature extends Fragment {
    private Utils utils;
    private ImageView btnttd, preview;
    private TextView percentage;
    private Button send;

    private SignatureView _signature;
    private Button _cancel, _clear, _ok;
    private static Bitmap bitmap;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.signature, container, false);

        loadResources(viewGroup);
        loadListener();

        return viewGroup;
    }

    private void loadResources(ViewGroup root) {
        utils = new Utils(getActivity());

        btnttd      = (ImageView) root.findViewById(R.id.signature_btnttd);
        preview     = (ImageView) root.findViewById(R.id.signature_preview);
        percentage  = (TextView) root.findViewById(R.id.signature_percentage);
        send        = (Button) root.findViewById(R.id.signature_send);

        btnttd.setOnClickListener(btnttd_listener);
    }

    private void loadListener() {

    }

    private View.OnClickListener btnttd_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            signature();
        }
    };

    private void signature() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sign_activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        WindowManager.LayoutParams lp   = new WindowManager.LayoutParams();
        Window window                   = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        _cancel      = (Button) dialog.findViewById(R.id.signact_cancel);
        _clear       = (Button) dialog.findViewById(R.id.signact_clear);
        _ok          = (Button) dialog.findViewById(R.id.signact_ok);
        _signature   = (SignatureView) dialog.findViewById(R.id.signact_signature);

        _cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        _clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _signature.clear();
            }
        });

        _ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_signature.isClear()) {
                    Toast.makeText(getActivity(), "Silahkan tanda tangan!", Toast.LENGTH_SHORT).show();
                } else {
                    StringBuilder fileName  = new StringBuilder();
                    SimpleDateFormat sdf    = new SimpleDateFormat("yyyy-MM-dd");

                    Date now        = new Date();
                    String strDate  = sdf.format(now);
                    String yy       = strDate.substring(2, 4);
                    String mm       = strDate.substring(5, 7);
                    String dd       = strDate.substring(8, 10);

                    if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                        Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_SHORT).show();
                    } else {
                        fileName.append("numberform" + "_");
                        fileName.append("SIGNATURE_");
                        fileName.append(yy);
                        fileName.append(mm);
                        fileName.append(dd);
                        fileName.append(".jpg");

                        String _filename    = fileName.toString();
                        String path         = Environment.getExternalStorageDirectory()+ File.separator + "Pictures";
                        _signature.exportFile(path, _filename);
                        dialog.dismiss();

                        Toast.makeText(getActivity(), "Gambar tersimpan", Toast.LENGTH_SHORT).show();

                        String imagePath = path+File.separator+fileName.toString();
                        decodeFile(imagePath);
                    }
                }
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public void decodeFile(final String filePath) {
        // Decode ukuran gambar
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds    = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp   = o.outWidth, height_tmp = o.outHeight;
        int scale       = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;

            width_tmp   /= 2;
            height_tmp  /= 2;
            scale       *= 2;
        }

        // Decode with inSampleSize
        final BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        bitmap          = BitmapFactory.decodeFile(filePath, o2);
        preview.setImageBitmap(bitmap);
    }
}