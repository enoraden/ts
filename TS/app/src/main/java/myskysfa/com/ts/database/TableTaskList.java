package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/05/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "task_list")
public class TableTaskList {
    public static final String TABLE_NAME           = "task_list";

    public static final String fFORM_ID             = "form_id";
    public static final String fID_RETRIEVAL        = "id_retrieval";
    public static final String fPROSPECT_NBR        = "prospect_nbr";
    public static final String fCUSTOMER_NBR        = "customer_nbr";
    public static final String fTICKET_NBR          = "ticket_nbr";
    public static final String fJOB_TYPE            = "job_type";
    public static final String fSCHEDULE_PENARIKAN  = "schedule_penarikan";
    public static final String fSTATUS_PENARIKAN    = "status_penarikan";
    public static final String fUSERNAME            = "username";

    @DatabaseField(id = true)
    private String form_id;

    @DatabaseField
    private String id_retrieval, prospect_nbr, customer_nbr, ticket_nbr, job_type, schedule_penarikan,
            status_penarikan, username;

    public void setfFORM_ID(String form_id) {
        this.form_id = form_id;
    }
    public String getfFORM_ID() {
        return form_id;
    }

    public void setfID_RETRIEVAL(String id_retrieval) {
        this.id_retrieval = id_retrieval;
    }
    public String getfID_RETRIEVAL() {
        return id_retrieval;
    }

    public void setfPROSPECT_NBR(String prospect_nbr) {
        this.prospect_nbr = prospect_nbr;
    }
    public String getfPROSPECT_NBR() {
        return prospect_nbr;
    }

    public void setfCUSTOMER_NBR(String customer_nbr) {
        this.customer_nbr = customer_nbr;
    }
    public String getfCUSTOMER_NBR() {
        return customer_nbr;
    }

    public void setfTICKET_NBR(String ticket_nbr) {
        this.ticket_nbr = ticket_nbr;
    }
    public String getfTICKET_NBR() {
        return ticket_nbr;
    }

    public void setfJOB_TYPE(String job_type) {
        this.job_type = job_type;
    }
    public String getfJOB_TYPE() {
        return job_type;
    }

    public void setfSCHEDULE_PENARIKAN(String schedule_penarikan) {
        this.schedule_penarikan = schedule_penarikan;
    }
    public String getfSCHEDULE_PENARIKAN() {
        return schedule_penarikan;
    }

    public void setfSTATUS_PENARIKAN(String status_penarikan) {
        this.status_penarikan = status_penarikan;
    }
    public String getfSTATUS_PENARIKAN() {
        return status_penarikan;
    }

    public void setfUSERNAME(String username) {
        this.username = username;
    }
    public String getfUSERNAME() {
        return username;
    }
}