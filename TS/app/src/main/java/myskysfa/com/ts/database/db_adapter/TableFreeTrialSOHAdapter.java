package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import myskysfa.com.ts.database.TableFreeTrialSOH;
import myskysfa.com.ts.utils.DatabaseManager;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/4/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableFreeTrialSOHAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableFreeTrialSOHAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableFreeTrialSOH> getDatabyCondition(Context context, String condition, Object param) {
        List<TableFreeTrialSOH> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrialSOH.class);

            QueryBuilder<TableFreeTrialSOH, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrialSOH, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableFreeTrialSOH> getAllData(Context context) {
        List<TableFreeTrialSOH> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrialSOH.class);

            QueryBuilder<TableFreeTrialSOH, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableFreeTrialSOH table, String item_id, String item_code, String item_descr,
                           String serial_no, String employee_id, String qty) {
        try {
            table.setfITEM_ID(item_id);
            table.setfITEM_CODE(item_code);
            table.setfITEM_DESCR(item_descr);
            table.setfSERIAL_NO(serial_no);
            table.setfEMPLOYEE_ID(employee_id);
            table.setfQTY(qty);
            getHelper().getTableFreeTrialSOHDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrialSOH.class);

            UpdateBuilder<TableFreeTrialSOH, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String item_id, String item_code, String item_descr,
                              String serial_no, String employee_id, String qty, String condition,
                              Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrialSOH.class);

            UpdateBuilder<TableFreeTrialSOH, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFreeTrialSOH.fITEM_ID, item_id);
            updateBuilder.updateColumnValue(TableFreeTrialSOH.fITEM_CODE, item_code);
            updateBuilder.updateColumnValue(TableFreeTrialSOH.fITEM_DESCR, item_descr);
            updateBuilder.updateColumnValue(TableFreeTrialSOH.fSERIAL_NO, serial_no);
            updateBuilder.updateColumnValue(TableFreeTrialSOH.fEMPLOYEE_ID, employee_id);
            updateBuilder.updateColumnValue(TableFreeTrialSOH.fQTY, qty);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrialSOH.class);

            DeleteBuilder<TableFreeTrialSOH, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableFreeTrialSOH> listTable = null;
        try {
            listTable = getHelper().getTableFreeTrialSOHDAO().queryForAll();
            getHelper().getTableFreeTrialSOHDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}