package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.ts.database.TableMasterMaterial;
import myskysfa.com.ts.utils.DatabaseManager;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 1/11/2017.
|---------------------------------------------------------------------------------------------------
*/
public class TableMasterMaterialAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableMasterMaterialAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }


    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableMasterMaterial> getDatabyCondition(Context context, String condition, Object param) {
        List<TableMasterMaterial> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterMaterial.class);

            QueryBuilder<TableMasterMaterial, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterMaterial, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableMasterMaterial> getAllData(Context context) {
        List<TableMasterMaterial> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterMaterial.class);

            QueryBuilder<TableMasterMaterial, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableMasterMaterial table, String item_id, String item_code, String item_descr,
                           String item_class_code, String item_type_code, String uom_code, String status,
                           String serialized, String sellable, String loanable, String rentable, String warranty,
                           String created_by, String created_date, String last_upd_by, String last_upd_date,
                           String item_type_id, String plan_id, String used_item_warranty, String repairable,
                           String is_tax_included, String charge_component, String charge_desc, String rate) {
        try {
            table.setfITEM_ID(item_id);
            table.setfITEM_CODE(item_code);
            table.setfITEM_DESCR(item_descr);
            table.setfITEM_CLASS_CODE(item_class_code);
            table.setfITEM_TYPE_CODE(item_type_code);
            table.setfUOM_CODE(uom_code);
            table.setfSTATUS(status);
            table.setfSERIALIZED(serialized);
            table.setfSELLABLE(sellable);
            table.setfLOANABLE(loanable);
            table.setfRENTABLE(rentable);
            table.setfWARRANTY(warranty);
            table.setfCREATE_BY(created_by);
            table.setfCREATE_DATE(created_date);
            table.setfLAST_UPD_BY(last_upd_by);
            table.setfLAST_UPD_DATE(last_upd_date);
            table.setfITEM_TYPE_ID(item_type_id);
            table.setfPLAN_ID(plan_id);
            table.setfUSED_ITEM_WARRANTY(used_item_warranty);
            table.setfREPAIRABLE(repairable);
            table.setfIS_TAX_INCLUDED(is_tax_included);
            table.setfCHARGE_COMPONENT(charge_component);
            table.setfCHARGE_DESC(charge_desc);
            table.setfRATE(rate);
            getHelper().getTableMasterMaterialDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterMaterial.class);

            UpdateBuilder<TableMasterMaterial, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String item_id, String item_code, String item_descr,
                              String item_class_code, String item_type_code, String uom_code, String status,
                              String serialized, String sellable, String loanable, String rentable, String warranty,
                              String created_by, String created_date, String last_upd_by, String last_upd_date,
                              String item_type_id, String plan_id, String used_item_warranty, String repairable,
                              String is_tax_included, String charge_component, String charge_desc, String rate,
                              String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterMaterial.class);

            UpdateBuilder<TableMasterMaterial, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableMasterMaterial.fITEM_ID, item_id);
            updateBuilder.updateColumnValue(TableMasterMaterial.fITEM_CODE, item_code);
            updateBuilder.updateColumnValue(TableMasterMaterial.fITEM_DESCR, item_descr);
            updateBuilder.updateColumnValue(TableMasterMaterial.fITEM_CLASS_CODE, item_class_code);
            updateBuilder.updateColumnValue(TableMasterMaterial.fITEM_TYPE_CODE, item_type_code);
            updateBuilder.updateColumnValue(TableMasterMaterial.fUOM_CODE, uom_code);
            updateBuilder.updateColumnValue(TableMasterMaterial.fSTATUS, status);
            updateBuilder.updateColumnValue(TableMasterMaterial.fSERIALIZED, serialized);
            updateBuilder.updateColumnValue(TableMasterMaterial.fSELLABLE, sellable);
            updateBuilder.updateColumnValue(TableMasterMaterial.fLOANABLE, loanable);
            updateBuilder.updateColumnValue(TableMasterMaterial.fRENTABLE, rentable);
            updateBuilder.updateColumnValue(TableMasterMaterial.fWARRANTY, warranty);
            updateBuilder.updateColumnValue(TableMasterMaterial.fCREATE_BY, created_by);
            updateBuilder.updateColumnValue(TableMasterMaterial.fCREATE_DATE, created_date);
            updateBuilder.updateColumnValue(TableMasterMaterial.fLAST_UPD_BY, last_upd_by);
            updateBuilder.updateColumnValue(TableMasterMaterial.fLAST_UPD_DATE, last_upd_date);
            updateBuilder.updateColumnValue(TableMasterMaterial.fITEM_TYPE_ID, item_type_id);
            updateBuilder.updateColumnValue(TableMasterMaterial.fPLAN_ID, plan_id);
            updateBuilder.updateColumnValue(TableMasterMaterial.fUSED_ITEM_WARRANTY, used_item_warranty);
            updateBuilder.updateColumnValue(TableMasterMaterial.fREPAIRABLE, repairable);
            updateBuilder.updateColumnValue(TableMasterMaterial.fIS_TAX_INCLUDED, is_tax_included);
            updateBuilder.updateColumnValue(TableMasterMaterial.fCHARGE_COMPONENT, charge_component);
            updateBuilder.updateColumnValue(TableMasterMaterial.fCHARGE_DESC, charge_desc);
            updateBuilder.updateColumnValue(TableMasterMaterial.fRATE, rate);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterMaterial.class);

            DeleteBuilder<TableMasterMaterial, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableMasterMaterial> listTable = null;
        try {
            listTable = getHelper().getTableMasterMaterialDAO().queryForAll();
            getHelper().getTableMasterMaterialDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}