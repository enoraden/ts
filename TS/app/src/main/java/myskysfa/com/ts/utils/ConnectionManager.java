package myskysfa.com.ts.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
public class ConnectionManager {
    public static final String CM_URL_LOGIN             = Config.makeUrlLocalString("sfapi-dev/user/login");
    public static final String CM_URL_LOGOUT            = Config.makeUrlLocalString("sfapi/user/logout");
    public static final String CM_URL_MASTER_PROMOTION  = Config.makeUrlLocalString("sfapi/master/promotion/list");
    public static final String CM_URL_MASTER_PACKAGE    = Config.makeUrlLocalString("sfapi/master/package/list");
    public static final String CM_URL_FTDTD_PLAN_ALL    = Config.makeUrlLocalString("sfapi-dev/ftdtd/plan/all");
    public static final String CM_URL_FTDTD_ACTIVATION  = Config.makeUrlLocalString("sfapi-dev/ftdtd/activate");
    public static final String CM_URL_RETRIEVE          = Config.makeUrlLocalString("sfapi-dev/ftdtd/retrieve/tasklist");
    public static final String CM_URL_DETAIL            = Config.makeUrlLocalString("sfapi-dev/ftdtd/detail");
    public static final String CM_URL_FT_SOH            = Config.makeUrlLocalString("sfapi-dev/ftdtd/soh");
    public static final String CM_URL_FTDTD_LIST        = Config.makeUrlLocalString("sfapi-dev/ftdtd/list");

    public static final String CM_URL_MASTER_HARDWARE   = Config.makeUrlLocalString("yummy-dev/ts/hw");
    public static final String CM_URL_MASTER_MATERIAL   = Config.makeUrlLocalString("yummy-dev/master/material/hw");

    public static final String CM_URL_GET_TICKET        = Config.makeUrlLocalString("yummy-dev/ts/ticket");
    public static final String CM_URL_GET_REJOIN        = Config.makeUrlLocalString("yummy-dev/ts/rejoin");

    public static final String CM_URL_DETAIL_PROFILE    = Config.makeUrlLocalString("yummy-dev/ts/profile");
    public static final String CM_URL_UPLOAD_IMAGE      = Config.makeUrlLocalString("yummy-dev/upload/imageft");
    public static final String CM_URL_SEND_RESULT       = Config.makeUrlLocalString("yummy-dev/ts/entry");

    private static String executeHttpClientPost(String requestURL, HashMap<String, String> params) throws IOException {
        URL url;
        String response = "";
        try {
            url = new URL(requestURL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setReadTimeout(35000);
            conn.setConnectTimeout(35000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(params));

            writer.flush();
            writer.close();
            os.close();

            int responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

                while ((line = br.readLine()) != null) {
                    response += line;
                }

            } else {
                response = "";

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return response;
    }

    private static String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result    = new StringBuilder();
        boolean first           = true;

        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    public static String connectHttpPost(String url, HashMap<String, String> params) throws IOException {
        return executeHttpClientPost(url, params);
    }

    public static String requestLogin(String url, String username, String password, String imei, String version, String signal, String fcm_token) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("username", username);
        param.put("password", password);
        param.put("imei", imei);
        param.put("version", version);
        param.put("signal", signal);
        param.put("fcm_token", fcm_token);
        return connectHttpPost(url, param);
    }

    public static String requestLogout(String url, String username, String imei, String version, String signal) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("username", username);
        param.put("imei", imei);
        param.put("version", version);
        param.put("signal", signal);
        return connectHttpPost(url, param);
    }

    public static String requestDataMaster(String url, String imei,  String token, String version, String signal) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("version", version);
        param.put("signal", signal);
        return connectHttpPost(url, param);
    }

    public static String sendResult(String url, String imei,  String token, String value, String version, String signal) throws IOException {
        HashMap<String, String> param = new HashMap<>();
        param.put("imei", imei);
        param.put("token", token);
        param.put("data", value);
        param.put("version", version);
        param.put("signal", signal);
        return connectHttpPost(url, param);
    }
}