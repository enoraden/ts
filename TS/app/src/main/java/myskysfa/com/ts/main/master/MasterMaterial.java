package myskysfa.com.ts.main.master;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.master.MasterMaterialAdapter;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.TableMasterMaterial;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.database.db_adapter.TableMasterMaterialAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 1/11/2017.
|---------------------------------------------------------------------------------------------------
*/
public class MasterMaterial extends Fragment {
    private MasterMaterialAdapter adapter;
    private TableMasterMaterialAdapter dbAdapterMasterMaterial;
    private List<TableMasterMaterial> listMasterMaterial;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;

    private TextView info;
    private RecyclerView recyclerview;
    private SwipeRefreshLayout swiperefresh;

    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.list_menu, container, false);

        info            = (TextView) viewGroup.findViewById(R.id.listmenu_info);
        recyclerview    = (RecyclerView) viewGroup.findViewById(R.id.listmenu_recyclerview);
        swiperefresh    = (SwipeRefreshLayout) viewGroup.findViewById(R.id.listmenu_swiperefresh);

        swiperefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swiperefresh.setEnabled(true);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        refreshItems();
        return viewGroup;
    }

    private void refreshItems() {
        utils = new Utils(getActivity());
        info.setVisibility(View.GONE);

        connDetect          = new ConnectionDetector(getActivity());
        isInternetPresent   = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            new getDataMaterial().execute();
        } else {
            if (swiperefresh.isShown()) {
                swiperefresh.setRefreshing(false);
            }

            utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
        }
    }

    /*
    |---------------------------------------------------------------------------------------------------
    | Get Data Material From Server
    |---------------------------------------------------------------------------------------------------
    */
    private class getDataMaterial extends AsyncTask<String, String, String> {
        private Boolean _status;
        private JSONArray _dataarray;
        private int _signal;
        private String _imei;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swiperefresh.setRefreshing(true);

            utils   = new Utils(getActivity());
            _signal = utils.checkSignal();
            _imei   = utils.getIMEI();
            _status = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(getActivity());
                listLogLogin        = dbAdapterLogLogin.getAllData(getActivity());

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_MASTER_MATERIAL;
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();

                    String _response    = ConnectionManager.requestDataMaster(_url, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status     = jsonObject.getBoolean("status");
                        _dataarray  = jsonObject.getJSONArray("data");

                        if (_status) {
                            dbAdapterMasterMaterial = new TableMasterMaterialAdapter(getActivity());
                            dbAdapterMasterMaterial.deleteAllData();

                            for (int i = 0; i < _dataarray.length(); i++) {
                                JSONObject objData      = _dataarray.getJSONObject(i);

                                String item_id              = objData.getString("item_id");
                                String item_code            = objData.getString("item_code");
                                String item_descr           = objData.getString("item_descr");
                                String item_class_code      = objData.getString("item_class_code");
                                String item_type_code       = objData.getString("item_type_code");
                                String uom_code             = objData.getString("uom_code");
                                String status               = objData.getString("status");
                                String serialized           = objData.getString("serialized");
                                String sellable             = objData.getString("sellable");
                                String loanable             = objData.getString("loanable");
                                String rentable             = objData.getString("rentable");
                                String warranty             = objData.getString("warranty");
                                String created_by           = objData.getString("created_by");
                                String created_date         = objData.getString("created_date");
                                String last_upd_by          = objData.getString("last_upd_by");
                                String last_upd_date        = objData.getString("last_upd_date");
                                String item_type_id         = objData.getString("item_type_id");
                                String plan_id              = objData.getString("plan_id");
                                String used_item_warranty   = objData.getString("used_item_warranty");
                                String repairable           = objData.getString("repairable");
                                String is_tax_included      = objData.getString("is_tax_included");
                                String charge_component     = objData.getString("charge_component");
                                String charge_desc          = objData.getString("charge_desc");
                                String rate                 = objData.getString("rate");

                                dbAdapterMasterMaterial = new TableMasterMaterialAdapter(getActivity());
                                dbAdapterMasterMaterial.insertData(new TableMasterMaterial(), item_id,
                                        item_code, item_descr, item_class_code, item_type_code, uom_code,
                                        status, serialized, sellable, loanable, rentable, warranty, created_by,
                                        created_date, last_upd_by, last_upd_date, item_type_id, plan_id,
                                        used_item_warranty, repairable, is_tax_included, charge_component,
                                        charge_desc, rate);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (swiperefresh.isShown()) {
                    swiperefresh.setRefreshing(false);
                }

                if (_status) {
                    showList();

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "MasterPackage");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void showList() {
            LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
            layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerview.setLayoutManager(layoutParams);

            dbAdapterMasterMaterial = new TableMasterMaterialAdapter(getActivity());
            listMasterMaterial      = dbAdapterMasterMaterial.getAllData(getActivity());
            adapter                 = new MasterMaterialAdapter(listMasterMaterial);
            recyclerview.setAdapter(adapter);

            if(listMasterMaterial.size() > 0){
                info.setVisibility(View.GONE);
            } else {
                info.setVisibility(View.VISIBLE);
                info.setText("Tidak Ada Data Promo");
            }
        }
    }
}
