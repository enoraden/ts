package myskysfa.com.ts.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import myskysfa.com.ts.database.TableMasterPromo;
import myskysfa.com.ts.utils.Utils;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/4/2016.
|---------------------------------------------------------------------------------------------------
*/
public class SpinnerPromoAdapter extends ArrayAdapter<TableMasterPromo> {
    private Context context;
    private List<TableMasterPromo> itemsStatus;
    private Utils utils;

    public SpinnerPromoAdapter(Context _context, int textViewResourceId, List<TableMasterPromo> _itemsStatus) {
        super(_context, textViewResourceId, _itemsStatus);
        this.context        = _context;
        this.itemsStatus    = _itemsStatus;
        utils               = new Utils(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtSpinner = new TextView(context);
        txtSpinner.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        txtSpinner.setTextSize(14);
        txtSpinner.setPadding(0, 0, 20, 0);
        txtSpinner.setTextColor(Color.BLACK);
        txtSpinner.setText(itemsStatus.get(position).getfPROMOTION_DESC());

        return txtSpinner;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txtSpinner = (TextView) super.getView(position, convertView, parent);
        txtSpinner.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        txtSpinner.setTextSize(14);
        txtSpinner.setPadding(10, 10, 20, 10);
        txtSpinner.setTextColor(Color.BLACK);
        txtSpinner.setText(itemsStatus.get(position).getfPROMOTION_DESC());

        return txtSpinner;
    }
}