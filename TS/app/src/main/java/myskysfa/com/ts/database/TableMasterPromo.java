package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/4/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "m_promo")
public class TableMasterPromo {
    public static final String TABLE_NAME       = "m_promo";

    public static final String fPROMOTION_ID    = "promotion_id";
    public static final String fPROMOTION_CODE  = "promotion_code";
    public static final String fPROMOTION_DESC  = "promotion_desc";
    public static final String fSTART_DATE      = "start_date";
    public static final String fEND_DATE        = "end_date";
    public static final String fSTATUS          = "status";
    public static final String fFLAG_PROMO      = "flag_promo";

    @DatabaseField(id = true)
    private String promotion_id;

    @DatabaseField
    private String promotion_code, promotion_desc, start_date, end_date, status, flag_promo;

    public void setfPROMOTION_ID(String promotion_id) {
        this.promotion_id = promotion_id;
    }
    public String getfPROMOTION_ID() {
        return promotion_id;
    }

    public void setfPROMOTION_CODE(String promotion_code) {
        this.promotion_code = promotion_code;
    }
    public String getfPROMOTION_CODE() {
        return promotion_code;
    }

    public void setfPROMOTION_DESC(String promotion_desc) {
        this.promotion_desc = promotion_desc;
    }
    public String getfPROMOTION_DESC() {
        return promotion_desc;
    }

    public void setfSTART_DATE(String start_date) {
        this.start_date = start_date;
    }
    public String getfSTART_DATE() {
        return start_date;
    }

    public void setfEND_DATE(String end_date) {
        this.end_date = end_date;
    }
    public String getfEND_DATE() {
        return end_date;
    }

    public void setfSTATUS(String status) {
        this.status = status;
    }
    public String getfSTATUS() {
        return status;
    }

    public void setfFLAG_PROMO(String flag_promo) {
        this.flag_promo = flag_promo;
    }
    public String getfFLAG_PROMO() {
        return flag_promo;
    }
}
