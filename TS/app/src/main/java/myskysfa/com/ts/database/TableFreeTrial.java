package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/7/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "free_trial")
public class TableFreeTrial {
    public static final String TABLE_NAME       = "free_trial";

    public static final String fPROSPECT_NBR    = "prospect_nbr";
    public static final String fSFL_CODE        = "sfl_code";
    public static final String fREGISTER_DATE   = "register_date";
    public static final String fIS_MULTI        = "is_multi";
    public static final String fPROSPECT_NAME   = "prospect_name";
    public static final String fMOBILE_PHONE    = "mobile_phone";
    public static final String fBRAND_CODE      = "brand_code";
    public static final String fCREATED_DATE    = "created_date";
    public static final String fSTATUS          = "status";
    public static final String fUSER_NAME       = "user_name";
    public static final String fPLAN_ID         = "plan_id";
    public static final String fALAMAT          = "alamat";
    public static final String fPRODUCT_NAME    = "product_name";
    public static final String fPRODUCT_ID      = "product_id";
    public static final String fVALUE           = "value";

    @DatabaseField(id = true)
    private String prospect_nbr;

    @DatabaseField
    private String sfl_code, register_date, is_multi, prospect_name, mobile_phone, brand_code, created_date,
            status, user_name, plan_id, alamat, product_name, product_id, value;

    public void setfPROSPECT_NBR(String prospect_nbr) {
        this.prospect_nbr = prospect_nbr;
    }
    public String getfPROSPECT_NBR() {
        return prospect_nbr;
    }

    public void setfSFL_CODE(String sfl_code) {
        this.sfl_code = sfl_code;
    }
    public String getfSFL_CODE() {
        return sfl_code;
    }

    public void setfREGISTER_DATE(String register_date) {
        this.register_date = register_date;
    }
    public String getfREGISTER_DATE() {
        return register_date;
    }

    public void setfIS_MULTI(String is_multi) {
        this.is_multi = is_multi;
    }
    public String getfIS_MULTI() {
        return is_multi;
    }

    public void setfPROSPECT_NAME(String prospect_name) {
        this.prospect_name = prospect_name;
    }
    public String getfPROSPECT_NAME() {
        return prospect_name;
    }

    public void setfMOBILE_PHONE(String mobile_phone) {
        this.mobile_phone = mobile_phone;
    }
    public String getfMOBILE_PHONE() {
        return mobile_phone;
    }

    public void setfBRAND_CODE(String brand_code) {
        this.brand_code = brand_code;
    }
    public String getfBRAND_CODE() {
        return brand_code;
    }

    public void setfCREATED_DATE(String created_date) {
        this.created_date = created_date;
    }
    public String getfCREATED_DATE() {
        return created_date;
    }

    public void setfSTATUS(String status) {
        this.status = status;
    }
    public String getfSTATUS() {
        return status;
    }

    public void setfUSER_NAME(String user_name) {
        this.user_name = user_name;
    }
    public String getfUSER_NAME() {
        return user_name;
    }

    public void setfPLAN_ID(String plan_id) {
        this.plan_id = plan_id;
    }
    public String getfPLAN_ID() {
        return plan_id;
    }

    public void setfALAMAT(String alamat) {
        this.alamat = alamat;
    }
    public String getfALAMAT() {
        return alamat;
    }

    public void setfPRODUCT_NAME(String product_name) {
        this.product_name = product_name;
    }
    public String getfPRODUCT_NAME() {
        return product_name;
    }

    public void setfPRODUCT_ID(String product_id) {
        this.product_id = product_id;
    }
    public String getfPRODUCT_ID() {
        return product_id;
    }

    public void setfVALUE(String value) {
        this.value = value;
    }
    public String getfVALUE() {
        return value;
    }
}