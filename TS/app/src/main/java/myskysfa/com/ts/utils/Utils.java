package myskysfa.com.ts.utils;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.TimePicker;
import myskysfa.com.ts.R;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Locale;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Utils {
    private static Context context;

    public Dialog progressdialog, dialog;
    public TextView dialog_tittle, dialog_message;
    public Button _onebtn_center, _twobtn_left, _twobtn_right;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter;
    private TimePickerDialog timePickerDialog;

    public double latitude = 0, longitude = 0;
    public Utils(Context _context) {
        this.context = _context;
    }

    public String getIMEI(){
        TelephonyManager mngr = (TelephonyManager) context.getSystemService(context.TELEPHONY_SERVICE);
        String imei = mngr.getDeviceId();
        return imei;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Cek Signal
    |-----------------------------------------------------------------------------------------------
    */
    public int checkSignal() {
        MyPhoneStateListener listenerSignal = new MyPhoneStateListener();
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(listenerSignal, PhoneStateListener.LISTEN_SIGNAL_STRENGTHS);
        return listenerSignal.setSignal();
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Load Resource Alert Dialog
    |-----------------------------------------------------------------------------------------------
    */
    public Typeface setFontType(String fontName, String fontType) {
        switch (fontName) {
            case "Steelfish" :
                switch (fontType) {
                    case "NORMAL" :
                        return Typeface.createFromAsset(context.getAssets(), "fonts/Steelfish.ttf");
                }
                return null;
            case "ChampagneLimousines" :
                switch (fontType) {
                    case "NORMAL" :
                        return Typeface.createFromAsset(context.getAssets(), "fonts/Champagne Limousines.ttf");
                    case "BOLD" :
                        return Typeface.createFromAsset(context.getAssets(), "fonts/Champagne Limousines Bold.ttf");
                }
                return null;
        }
        return null;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Load Progress Dialog
    |-----------------------------------------------------------------------------------------------
    */
    public void showDialogLoading(String message) {
        final int[] pStatus = {0};
        final ProgressBar progressbar;
        final Handler handler = new Handler();
        TextView text;

        progressdialog = new Dialog(context);
        progressdialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressdialog.setContentView(R.layout.progressdialog);
        progressdialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressdialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);
        WindowManager.LayoutParams lp   = new WindowManager.LayoutParams();
        Window window                   = progressdialog.getWindow();
        lp.copyFrom(window.getAttributes());

        progressbar = (ProgressBar) progressdialog.findViewById(R.id.progdialog_progressbar);
        text        = (TextView) progressdialog.findViewById(R.id.progdialog_text);

        text.setText(message);

        new Thread(new Runnable() {
            @Override
            public void run() {
                while (pStatus[0] <= 100) {
                    if (pStatus[0] >= 97){
                        pStatus[0] = 100;
                    }

                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            progressbar.setProgress(pStatus[0]);
                        }
                    });

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    if (pStatus[0] >= 100){
                        pStatus[0] = 0;
                    }

                    pStatus[0] = pStatus[0] + 3;
                }
            }
        }).start();

        progressdialog.show();
        progressdialog.setCancelable(false);
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Load Resource Alert Dialog
    |-----------------------------------------------------------------------------------------------
    */
    public void loadAlertDialog(Boolean onebtn, String tittle, String message, String
            onebtn_center, String twobtn_left, String twobtn_right) {

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        WindowManager.LayoutParams lp   = new WindowManager.LayoutParams();
        Window window                   = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        LinearLayout dialog_onebtn, dialog_twobtn;
        dialog_onebtn   = (LinearLayout) dialog.findViewById(R.id.dialog_onebtn);
        dialog_twobtn   = (LinearLayout) dialog.findViewById(R.id.dialog_twobtn);

        if (onebtn) {
            dialog_twobtn.setVisibility(View.GONE);
        } else {
            dialog_onebtn.setVisibility(View.GONE);
        }

        dialog_tittle   = (TextView) dialog.findViewById(R.id.dialog_tittle);
        dialog_message  = (TextView) dialog.findViewById(R.id.dialog_message);
        _onebtn_center  = (Button) dialog.findViewById(R.id.onebtn_center);
        _twobtn_left    = (Button) dialog.findViewById(R.id.twobtn_left);
        _twobtn_right   = (Button) dialog.findViewById(R.id.twobtn_right);

        dialog_tittle.setText(tittle);
        dialog_message.setText(message);
        _onebtn_center.setText(onebtn_center);
        _twobtn_left.setText(twobtn_left);
        _twobtn_right.setText(twobtn_right);
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Error Alert Dialog
    |-----------------------------------------------------------------------------------------------
    */
    public void showErrorDialog(String tittle, String message) {

        loadAlertDialog(true, tittle, message, "OK", "", "");

        _onebtn_center.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                dialog.dismiss();
            }
        });

        //dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.show();
        dialog.setCancelable(false);
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Method for set date field
    |-----------------------------------------------------------------------------------------------
    */
    public void setDateField(TextView textView) {
        final TextView date                      = textView;
        dateFormatter                            = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
        final SimpleDateFormat  simpleDateFormat = dateFormatter;

        Calendar newCalendar    = Calendar.getInstance();
        datePickerDialog        = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                date.setText(simpleDateFormat.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Method to get current date
    |-----------------------------------------------------------------------------------------------
    */
    public String getCurrentDate(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String formattedDate = df.format(c.getTime());
        return  formattedDate;
    }

    /*
	|-----------------------------------------------------------------------------------------------
	| Method for Location
	|-----------------------------------------------------------------------------------------------
	*/
    public void getLatLong() {
        try {
            GPSTracker gpsTracker = new GPSTracker(context);
            if (gpsTracker.getIsGPSTrackingEnabled()) {
                latitude    = gpsTracker.getLatitude();
                longitude   = gpsTracker.getLongitude();
            } else {
                gpsTracker.showSettingsAlert();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Method for set time field
    |-----------------------------------------------------------------------------------------------
    */
    public void setTimeField(TextView textView) {
        final TextView time     = textView;
        Calendar newCalendar    = Calendar.getInstance();
        timePickerDialog        = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int hourOfDay, int minuteOfDay) {
                time.setText(hourOfDay + ":" + minuteOfDay);
            }
        }, newCalendar.get(Calendar.HOUR_OF_DAY), newCalendar.get(Calendar.MINUTE),true);
        timePickerDialog.show();
    }

    public void setHideKeyboard(Context _context, View view){
        if (view != null){
            //hide Keyboard
            InputMethodManager imm = (InputMethodManager)_context.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    public boolean findStringImage(String source,String keyword){
        Boolean found = Arrays.asList(source.split("_")).contains(keyword);
        if(found){
            return true;
        }else{
            return false;
        }
    }
}