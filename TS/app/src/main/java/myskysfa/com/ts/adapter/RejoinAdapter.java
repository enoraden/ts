package myskysfa.com.ts.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableMainRejoin;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/05/2016.
|---------------------------------------------------------------------------------------------------
*/
public class RejoinAdapter extends RecyclerView.Adapter<RejoinAdapter.ViewHolder> {
    public static List<TableMainRejoin> itemData = new ArrayList<>();
    private View itemLayoutView;

    public RejoinAdapter(List<TableMainRejoin> list) {
        this.itemData   = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView          = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_penarikan, parent, false);
        ViewHolder viewHolder   = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableMainRejoin item = itemData.get(position);

        if (item != null) {
            holder.icon.setText(item.getfJOB_TYPE().substring(0, 2).toUpperCase());
            holder.prospectnbr.setText(item.getfPROSPECT_NBR());
            holder.dateschedule.setText("Date Schedule : " + item.getfSCHEDULE_PENARIKAN());
            holder.status.setText("NULL");

            if (item.getfSTATUS_PENARIKAN() != null
                    && item.getfSTATUS_PENARIKAN().equalsIgnoreCase("0")) {
                holder.status.setText("NEW");

            } else if (item.getfSTATUS_PENARIKAN()!=null
                    && item.getfSTATUS_PENARIKAN().equalsIgnoreCase("1")) {
                holder.status.setText("BERHASIL");

            } else if (item.getfSTATUS_PENARIKAN()!=null
                    && item.getfSTATUS_PENARIKAN().equalsIgnoreCase("2")) {
                holder.status.setText("RESCHEDULE");

            } else if (item.getfSTATUS_PENARIKAN()!=null
                    && item.getfSTATUS_PENARIKAN().equalsIgnoreCase("3")) {
                holder.status.setText("BERLANGGANAN");

            } else {
                holder.status.setText("NULL");
            }
        }
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView icon, prospectnbr, dateschedule, status;

        public ViewHolder(View itemView) {
            super(itemView);

            icon            = (TextView) itemLayoutView.findViewById(R.id.item_pnr_icon);
            prospectnbr     = (TextView) itemLayoutView.findViewById(R.id.item_pnr_prospectnbr);
            dateschedule    = (TextView) itemLayoutView.findViewById(R.id.item_pnr_dateschedule);
            status          = (TextView) itemLayoutView.findViewById(R.id.item_pnr_status);
        }
    }
}
