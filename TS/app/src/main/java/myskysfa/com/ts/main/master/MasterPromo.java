package myskysfa.com.ts.main.master;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.master.MasterPromoAdapter;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.TableMasterPromo;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.database.db_adapter.TableMasterPromoAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterPromo extends Fragment {
    private MasterPromoAdapter adapter;
    private TableMasterPromoAdapter dbAdapterMasterPromo;
    private List<TableMasterPromo> listMasterPromo;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;

    private TextView info;
    private RecyclerView recyclerview;
    private SwipeRefreshLayout swiperefresh;

    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.list_menu, container, false);

        info            = (TextView) viewGroup.findViewById(R.id.listmenu_info);
        recyclerview    = (RecyclerView) viewGroup.findViewById(R.id.listmenu_recyclerview);
        swiperefresh    = (SwipeRefreshLayout) viewGroup.findViewById(R.id.listmenu_swiperefresh);

        swiperefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swiperefresh.setEnabled(true);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        refreshItems();
        return viewGroup;
    }

    private void refreshItems() {
        utils = new Utils(getActivity());
        info.setVisibility(View.GONE);

        connDetect          = new ConnectionDetector(getActivity());
        isInternetPresent   = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            new getDataPromo().execute();
        } else {
            if (swiperefresh.isShown()) {
                swiperefresh.setRefreshing(false);
            }

            utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
        }
    }

    /*
    |---------------------------------------------------------------------------------------------------
    | Get Data Promo From Server
    |---------------------------------------------------------------------------------------------------
    */
    private class getDataPromo extends AsyncTask<String, String, String> {
        private Boolean _status;
        private JSONArray _dataarray;
        private int _signal;
        private String _imei;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swiperefresh.setRefreshing(true);

            utils   = new Utils(getActivity());
            _signal = utils.checkSignal();
            _imei   = utils.getIMEI();
            _status = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(getActivity());
                listLogLogin        = dbAdapterLogLogin.getAllData(getActivity());

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_MASTER_PROMOTION;
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();

                    String _response    = ConnectionManager.requestDataMaster(_url, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status     = jsonObject.getBoolean("status");
                        _dataarray  = jsonObject.getJSONArray("data");

                        if (_status) {
                                dbAdapterMasterPromo = new TableMasterPromoAdapter(getActivity());
                                dbAdapterMasterPromo.deleteAllData();

                            for (int i = 0; i < _dataarray.length(); i++) {
                                JSONObject objData      = _dataarray.getJSONObject(i);

                                String promotion_id     = objData.getString("id");
                                String promotion_code   = objData.getString("code");
                                String promotion_desc   = objData.getString("description");
                                String start_date       = objData.getString("start_date");
                                String end_date         = objData.getString("end_date");

                                dbAdapterMasterPromo = new TableMasterPromoAdapter(getActivity());
                                dbAdapterMasterPromo.insertData(new TableMasterPromo(), promotion_id,
                                        promotion_code, promotion_desc.toUpperCase(), start_date, end_date, "A", "1");
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (swiperefresh.isShown()) {
                    swiperefresh.setRefreshing(false);
                }

                if (_status) {
                    showList();

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "MasterPackage");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void showList() {
            LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
            layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerview.setLayoutManager(layoutParams);

            dbAdapterMasterPromo    = new TableMasterPromoAdapter(getActivity());
            listMasterPromo         = dbAdapterMasterPromo.getAllData(getActivity());
            adapter                 = new MasterPromoAdapter(listMasterPromo);
            recyclerview.setAdapter(adapter);

            if(listMasterPromo.size() > 0){
                info.setVisibility(View.GONE);
            } else {
                info.setVisibility(View.VISIBLE);
                info.setText("Tidak Ada Data Promo");
            }
        }
    }
}