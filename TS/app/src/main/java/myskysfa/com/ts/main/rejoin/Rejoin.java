package myskysfa.com.ts.main.rejoin;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.RejoinAdapter;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.TableMainRejoin;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.database.db_adapter.TableMainRejoinAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.Utils;
import myskysfa.com.ts.widget.RecyclerItemClickListener;

import static myskysfa.com.ts.adapter.RejoinAdapter.itemData;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/05/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Rejoin extends Fragment {
    private RejoinAdapter adapter;
    private TableMainRejoinAdapter dbAdapterMainRejoin;
    private List<TableMainRejoin> listMainRejoin;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;

    private TextView info;
    private RecyclerView recyclerview;
    private SwipeRefreshLayout swiperefresh;

    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.list_menu, container, false);

        info            = (TextView) viewGroup.findViewById(R.id.listmenu_info);
        recyclerview    = (RecyclerView) viewGroup.findViewById(R.id.listmenu_recyclerview);
        swiperefresh    = (SwipeRefreshLayout) viewGroup.findViewById(R.id.listmenu_swiperefresh);

        swiperefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swiperefresh.setEnabled(true);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        refreshItems();
        loadListener();

        return viewGroup;
    }

    private void refreshItems() {
        utils = new Utils(getActivity());
        info.setVisibility(View.GONE);

        connDetect          = new ConnectionDetector(getActivity());
        isInternetPresent   = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            new getDataTaskList().execute();
        } else {
            if (swiperefresh.isShown()) {
                swiperefresh.setRefreshing(false);
            }

            utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
        }
    }

    /*
    |---------------------------------------------------------------------------------------------------
    | Get Data Task List From Server
    |---------------------------------------------------------------------------------------------------
    */
    private class getDataTaskList extends AsyncTask<String, String, String> {
        private Boolean _status;
        private JSONArray _dataarray;
        private int _signal;
        private String _imei;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swiperefresh.setRefreshing(true);

            utils   = new Utils(getActivity());
            _signal = utils.checkSignal();
            _imei   = utils.getIMEI();
            _status = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(getActivity());
                listLogLogin        = dbAdapterLogLogin.getAllData(getActivity());

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_GET_REJOIN;
                    String _username    = listLogLogin.get(listLogLogin.size() - 1).getfUSER_NAME();
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl    = _url + "/" + _username;

                    String _response    = ConnectionManager.requestDataMaster(_paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status     = jsonObject.getBoolean("status");
                        _dataarray  = jsonObject.getJSONArray("data");

                        if (_status) {
                            dbAdapterMainRejoin = new TableMainRejoinAdapter(getActivity());
                            dbAdapterMainRejoin.deleteAllData();

                            for (int i = 0; i < _dataarray.length(); i++) {
                                JSONObject objData          = _dataarray.getJSONObject(i);

                                String id_retrieval         = objData.getString("id_retrieval");
                                String prospect_nbr         = objData.getString("prospect_nbr");
                                String customer_nbr         = objData.getString("customer_nbr");
                                String ticket_nbr           = objData.getString("ticket_nbr");
                                String job_type             = objData.getString("job_type");
                                String schedule_penarikan   = objData.getString("schedule_penarikan");
                                String status_penarikan     = objData.getString("status_penarikan");
                                String username             = objData.getString("username");

                                dbAdapterMainRejoin = new TableMainRejoinAdapter(getActivity());
                                dbAdapterMainRejoin.insertData(new TableMainRejoin(), id_retrieval,
                                        prospect_nbr, customer_nbr, ticket_nbr, job_type, schedule_penarikan,
                                        status_penarikan, username);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (swiperefresh.isShown()) {
                    swiperefresh.setRefreshing(false);
                }

                if (_status) {
                    showList();

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "Data Instalasi");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutParams);

        dbAdapterMainRejoin = new TableMainRejoinAdapter(getActivity());
        listMainRejoin      = dbAdapterMainRejoin.getAllData(getActivity());
        adapter             = new RejoinAdapter(listMainRejoin);
        recyclerview.setAdapter(adapter);

        if(listMainRejoin.size() > 0){
            info.setVisibility(View.GONE);
        } else {
            info.setVisibility(View.VISIBLE);
            info.setText("Tidak Ada Data");
        }
    }


    private void loadListener() {
        recyclerview.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerview,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String status = itemData.get(position).getfSTATUS_PENARIKAN();
                        if (status == null
                                || status.equalsIgnoreCase("1") //BERHASIL
                                || status.equalsIgnoreCase("2")) //RESCHEDULE
                        {
                            Toast.makeText(getActivity(), "Data dengan status Berhasil dan Reschedule tidak dapat dipilih.", Toast.LENGTH_LONG).show();

                        } else {
                            Vibrator vibrate = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                            vibrate.vibrate(100);

                            Intent intent = new Intent(getActivity(), RejoinTabLayout.class);
                            putExtraRejoin(intent, position);
                            startActivity(intent);
                        }
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                        // ----------------------
                    }
                }));
    }

    public void putExtraRejoin(Intent _intent, int _position) {
        _intent.putExtra("id_retrieval", itemData.get(_position).getfID_RETRIEVAL());
        _intent.putExtra("prospect_nbr", itemData.get(_position).getfPROSPECT_NBR());
        _intent.putExtra("customer_nbr", itemData.get(_position).getfCUSTOMER_NBR());
        _intent.putExtra("ticket_nbr", itemData.get(_position).getfTICKET_NBR());
        _intent.putExtra("job_type", itemData.get(_position).getfJOB_TYPE());
        _intent.putExtra("schedule_penarikan", itemData.get(_position).getfSCHEDULE_PENARIKAN());
        _intent.putExtra("status_penarikan", itemData.get(_position).getfSTATUS_PENARIKAN());
        _intent.putExtra("username", itemData.get(_position).getfUSERNAME());
    }
}