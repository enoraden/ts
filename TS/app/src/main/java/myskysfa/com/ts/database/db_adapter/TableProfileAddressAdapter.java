package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.ts.database.TableProfileAddress;
import myskysfa.com.ts.utils.DatabaseManager;

/*
|---------------------------------------------------------------------------------------------------
| Created by Taufik Hidayat on 1/17/2017.
|---------------------------------------------------------------------------------------------------
*/
public class TableProfileAddressAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableProfileAddressAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableProfileAddress> getDatabyCondition(Context context, String condition, Object param) {
        List<TableProfileAddress> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableProfileAddress.class);

            QueryBuilder<TableProfileAddress, Integer> queryBuilder = dao.queryBuilder();
            Where<TableProfileAddress, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableProfileAddress> getAllData(Context context) {
        List<TableProfileAddress> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableProfileAddress.class);

            QueryBuilder<TableProfileAddress, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableProfileAddress table, String address_type, String prospect_nbr,
                           String building_status_id, String building_type, String street, String rt,
                           String rw, String no_rumah, String kelurahan, String kecamatan, String kota,
                           String province, String zipcode, String created_by, String created_date,
                           String last_upd_by, String last_upd_date) {
        try {
            table.setfADDRESS_TYPE(address_type);
            table.setfPROSPECT_NBR(prospect_nbr);
            table.setfBUILDING_STATUS_ID(building_status_id);
            table.setfBUILDING_TYPE(building_type);
            table.setfSTREET(street);
            table.setfRT(rt);
            table.setfRW(rw);
            table.setfNO_RUMAH(no_rumah);
            table.setfKELURAHAN(kelurahan);
            table.setfKECAMATAN(kecamatan);
            table.setfKOTA(kota);
            table.setfPROVINCE(province);
            table.setfZIPCODE(zipcode);
            table.setfCREATED_BY(created_by);
            table.setfCREATED_DATE(created_date);
            table.setfLAST_UPD_BY(last_upd_by);
            table.setfLAST_UPD_DATE(last_upd_date);
            getHelper().getTableProfileAddressDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableProfileAddress.class);

            UpdateBuilder<TableProfileAddress, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String address_type, String prospect_nbr,
                              String building_status_id, String building_type, String street, String rt,
                              String rw, String no_rumah, String kelurahan, String kecamatan, String kota,
                              String province, String zipcode, String created_by, String created_date,
                              String last_upd_by, String last_upd_date, String condition,
                              Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableProfileAddress.class);

            UpdateBuilder<TableProfileAddress, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableProfileAddress.fADDRESS_TYPE, address_type);
            updateBuilder.updateColumnValue(TableProfileAddress.fPROSPECT_NBR, prospect_nbr);
            updateBuilder.updateColumnValue(TableProfileAddress.fBUILDING_STATUS_ID, building_status_id);
            updateBuilder.updateColumnValue(TableProfileAddress.fBUILDING_TYPE, building_type);
            updateBuilder.updateColumnValue(TableProfileAddress.fSTREET, street);
            updateBuilder.updateColumnValue(TableProfileAddress.fRT, rt);
            updateBuilder.updateColumnValue(TableProfileAddress.fRW, rw);
            updateBuilder.updateColumnValue(TableProfileAddress.fNO_RUMAH, no_rumah);
            updateBuilder.updateColumnValue(TableProfileAddress.fKELURAHAN, kelurahan);
            updateBuilder.updateColumnValue(TableProfileAddress.fKECAMATAN, kecamatan);
            updateBuilder.updateColumnValue(TableProfileAddress.fKOTA, kota);
            updateBuilder.updateColumnValue(TableProfileAddress.fPROVINCE, province);
            updateBuilder.updateColumnValue(TableProfileAddress.fZIPCODE, zipcode);
            updateBuilder.updateColumnValue(TableProfileAddress.fCREATED_BY, created_by);
            updateBuilder.updateColumnValue(TableProfileAddress.fCREATED_DATE, created_date);
            updateBuilder.updateColumnValue(TableProfileAddress.fLAST_UPD_BY, last_upd_by);
            updateBuilder.updateColumnValue(TableProfileAddress.fLAST_UPD_DATE, last_upd_date);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableProfileAddress.class);

            DeleteBuilder<TableProfileAddress, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableProfileAddress> listTable = null;
        try {
            listTable = getHelper().getTableProfileAddressDAO().queryForAll();
            getHelper().getTableProfileAddressDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}