package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import myskysfa.com.ts.database.TableMasterPackage;
import myskysfa.com.ts.utils.DatabaseManager;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/4/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableMasterPackageAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableMasterPackageAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableMasterPackage> getDatabyCondition(Context context, String condition, Object param) {
        List<TableMasterPackage> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPackage.class);

            QueryBuilder<TableMasterPackage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterPackage, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableMasterPackage> getAllData(Context context) {
        List<TableMasterPackage> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPackage.class);

            QueryBuilder<TableMasterPackage, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableMasterPackage> getDatabyProfile(Context context, Object alacarte, Object brand) {
        List<TableMasterPackage> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPackage.class);

            QueryBuilder<TableMasterPackage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterPackage, Integer> where = queryBuilder.where();
            where.eq("alacarte", alacarte).and().eq("brand", brand);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableMasterPackage> getDatabyBasic(Context context, Object param) {
        List<TableMasterPackage> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPackage.class);

            QueryBuilder<TableMasterPackage, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterPackage, Integer> where = queryBuilder.where();
            where.eq("alacarte", 0).and().eq("brand", param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableMasterPackage table, String product_id, String product_type,
                           String product_name, int price, int alacarte, int flag_product,
                           String brand) {
        try {
            table.setfPRODUCT_ID(product_id);
            table.setfPRODUCT_TYPE(product_type);
            table.setfPRODUCT_NAME(product_name);
            table.setfPRICE(price);
            table.setfALACARTE(alacarte);
            table.setfFLAG_PRODUCT(flag_product);
            table.setfBRAND(brand);
            getHelper().getTableMasterPackageDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPackage.class);

            UpdateBuilder<TableMasterPackage, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String product_id, String product_type,
                              String product_name, int price, int alacarte, int flag_product,
                              String brand, String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPackage.class);

            UpdateBuilder<TableMasterPackage, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableMasterPackage.fPRODUCT_ID, product_id);
            updateBuilder.updateColumnValue(TableMasterPackage.fPRODUCT_TYPE, product_type);
            updateBuilder.updateColumnValue(TableMasterPackage.fPRODUCT_NAME, product_name);
            updateBuilder.updateColumnValue(TableMasterPackage.fPRICE, price);
            updateBuilder.updateColumnValue(TableMasterPackage.fALACARTE, alacarte);
            updateBuilder.updateColumnValue(TableMasterPackage.fFLAG_PRODUCT, flag_product);
            updateBuilder.updateColumnValue(TableMasterPackage.fBRAND, brand);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPackage.class);

            DeleteBuilder<TableMasterPackage, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableMasterPackage> listTable = null;
        try {
            listTable = getHelper().getTableMasterPackageDAO().queryForAll();
            getHelper().getTableMasterPackageDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}