package myskysfa.com.ts.main.rejoin;

import android.app.Dialog;
import android.app.NotificationManager;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableFormRejoin;
import myskysfa.com.ts.database.TableTaskList;
import myskysfa.com.ts.database.db_adapter.TableFormRejoinAdapter;
import myskysfa.com.ts.database.db_adapter.TableTaskListAdapter;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.widget.SignatureView;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Signature extends Fragment {
    private String form_id, id_retrieval, prospect_nbr, customer_nbr, ticket_nbr, job_type,
            schedule_penarikan, status_penarikan, username;

    private ImageView btnttd, preview;
    private TextView percentage;
    private ProgressBar progressbar;
    private Button send;

    private SignatureView _signature;
    private Button _cancel, _clear, _ok;
    private static Bitmap bitmap;
    private String imagePath;

    private NotificationManager notifManager;
    private NotificationCompat.Builder notifBuilder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.signature, container, false);

        loadResources(viewGroup);
        getDataTaskList();

        return viewGroup;
    }

    private void loadResources(ViewGroup root) {
        btnttd      = (ImageView) root.findViewById(R.id.signature_btnttd);
        preview     = (ImageView) root.findViewById(R.id.signature_preview);
        percentage  = (TextView) root.findViewById(R.id.signature_percentage);
        progressbar = (ProgressBar) root.findViewById(R.id.signature_progressbar);
        send        = (Button) root.findViewById(R.id.signature_send);

        btnttd.setOnClickListener(btnttd_listener);
        send.setOnClickListener(send_listener);
    }

    private void getDataTaskList() {
        TableTaskListAdapter dbAdapterTaskList  = new TableTaskListAdapter(getActivity());
        List<TableTaskList> listTaskList        = dbAdapterTaskList.getAllData(getActivity());

        if (listTaskList.size() > 0) {
            form_id             = listTaskList.get(listTaskList.size() - 1).getfFORM_ID();
            id_retrieval        = listTaskList.get(listTaskList.size() - 1).getfID_RETRIEVAL();
            prospect_nbr        = listTaskList.get(listTaskList.size() - 1).getfPROSPECT_NBR();
            customer_nbr        = listTaskList.get(listTaskList.size() - 1).getfCUSTOMER_NBR();
            ticket_nbr          = listTaskList.get(listTaskList.size() - 1).getfTICKET_NBR();
            job_type            = listTaskList.get(listTaskList.size() - 1).getfJOB_TYPE();
            schedule_penarikan  = listTaskList.get(listTaskList.size() - 1).getfSCHEDULE_PENARIKAN();
            status_penarikan    = listTaskList.get(listTaskList.size() - 1).getfSTATUS_PENARIKAN();
            username            = listTaskList.get(listTaskList.size() - 1).getfUSERNAME();
        }
    }

    private View.OnClickListener btnttd_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TableFormRejoinAdapter dbAdapterFormRejoin;
            List<TableFormRejoin> listFormRejoin;

            dbAdapterFormRejoin = new TableFormRejoinAdapter(getActivity());
            listFormRejoin      = dbAdapterFormRejoin.getDatabyCondition(getActivity(), TableFormRejoin.fFORMID, form_id);

            if (listFormRejoin.size() == 0) {
                preview.setImageResource(R.mipmap.noimage);
                Toast.makeText(getActivity(), "Silahkan isi data BAP terlebih dahulu!!", Toast.LENGTH_SHORT).show();
            } else {
                signature();
            }
        }
    };

    private void signature() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.sign_activity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.getWindow().setType(WindowManager.LayoutParams.TYPE_SYSTEM_ALERT);

        WindowManager.LayoutParams lp   = new WindowManager.LayoutParams();
        Window window                   = dialog.getWindow();
        lp.copyFrom(window.getAttributes());

        _cancel      = (Button) dialog.findViewById(R.id.signact_cancel);
        _clear       = (Button) dialog.findViewById(R.id.signact_clear);
        _ok          = (Button) dialog.findViewById(R.id.signact_ok);
        _signature   = (SignatureView) dialog.findViewById(R.id.signact_signature);

        _cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        _clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                _signature.clear();
            }
        });

        _ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (_signature.isClear()) {
                    Toast.makeText(getActivity(), "Silahkan tanda tangan!", Toast.LENGTH_SHORT).show();
                } else {
                    StringBuilder fileName  = new StringBuilder();
                    SimpleDateFormat sdf    = new SimpleDateFormat("yyyy-MM-dd");

                    Date now        = new Date();
                    String strDate  = sdf.format(now);
                    String yy       = strDate.substring(2, 4);
                    String mm       = strDate.substring(5, 7);
                    String dd       = strDate.substring(8, 10);

                    if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
                        Toast.makeText(getActivity(), "Error! No SDCARD Found!", Toast.LENGTH_SHORT).show();
                    } else {
                        fileName.append(id_retrieval + "_");
                        fileName.append("SIGNATURE_");
                        fileName.append(yy);
                        fileName.append(mm);
                        fileName.append(dd);
                        fileName.append(".jpg");

                        String _filename    = fileName.toString();
                        String path         = Environment.getExternalStorageDirectory()+ File.separator + "Pictures";
                        _signature.exportFile(path, _filename);
                        dialog.dismiss();

                        Toast.makeText(getActivity(), "Gambar tersimpan", Toast.LENGTH_SHORT).show();

                        imagePath = path+File.separator+fileName.toString();
                        decodeFile(imagePath);
                    }
                }
            }
        });

        dialog.setCancelable(false);
        dialog.show();
    }

    public void decodeFile(final String filePath) {
        // Decode ukuran gambar
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds    = true;
        BitmapFactory.decodeFile(filePath, o);

        // The new size we want to scale to
        final int REQUIRED_SIZE = 1024;

        // Find the correct scale value. It should be the power of 2.
        int width_tmp   = o.outWidth, height_tmp = o.outHeight;
        int scale       = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;

            width_tmp   /= 2;
            height_tmp  /= 2;
            scale       *= 2;
        }

        // Decode with inSampleSize
        final BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        bitmap          = BitmapFactory.decodeFile(filePath, o2);
        preview.setImageBitmap(bitmap);
    }

    private View.OnClickListener send_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (imagePath == null || imagePath.length() <= 0) {
                Toast.makeText(getActivity(), "Silahkan tanda tangan!", Toast.LENGTH_SHORT).show();
            } else {
                uploadImage(imagePath, getFileName(imagePath));
            }
        }
    };

    private void uploadImage(final String image_path, final String image_name) {
        try {
            String url = ConnectionManager.CM_URL_UPLOAD_IMAGE + "/" + prospect_nbr;
            System.out.println(url);

            final File myFile       = new File(image_path);
            RequestParams params    = new RequestParams();
            params.put("image", myFile);

            AsyncHttpClient client  = new AsyncHttpClient();
            client.setTimeout(300000);
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);

                    notifManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                    notifBuilder = new NotificationCompat.Builder(getActivity());
                    notifBuilder.setContentTitle("Signature Upload")
                            .setContentText("Upload in progress")
                            .setSmallIcon(R.drawable.stat_sys_upload);

                    int writen              = (int) bytesWritten;
                    float proportionCorrect = ((float) writen) / ((float) myFile.length());
                    float percentFl         = proportionCorrect * 100;
                    String percent          = String.format("%.0f", percentFl);

                    Log.i("uploadimage", "percent "+percent);
                    if (Integer.valueOf(percent) > Integer.valueOf(percentage.getText().toString().replace(" %", ""))) {
                        Log.i("uploadimage", "if "+percent);

                        percentage.setText(percent + " %");
                        progressbar.setProgress(Integer.valueOf(percent));
                    }

                    if (Integer.valueOf(percent) > 100) {
                        Log.i("uploadimage", "if 100 "+percent);
                        percentage.setText("100 %");
                        progressbar.setProgress(100);
                    }

                    notifBuilder.setProgress(100, Integer.parseInt(percent), false);
                    notifManager.notify(1, notifBuilder.build());
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] bytes) {
                    notifBuilder.setContentText("Upload complete")
                            .setProgress(0, 0, false)
                            .setSmallIcon(R.mipmap.stat_sys_upload_anim25);
                    notifManager.notify(1, notifBuilder.build());

                    Toast.makeText(getContext(), "Success upload " + image_name, Toast.LENGTH_LONG).show();

                    TableFormRejoinAdapter dbAdapter = new TableFormRejoinAdapter(getActivity());
                    dbAdapter.updateDatabyCondition(getActivity(), TableFormRejoin.fSIGNATURE_PATH, myFile.getName(),
                            TableFormRejoin.fFORMID, form_id);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {
                    Log.i("uploadimage", "onFailure "+String.valueOf(myFile));
                    percentage.setText(image_name + " 0 %");
                    progressbar.setProgress(0);
                    String responseString = "Error occurred! Http Status Code: " + statusCode + " " + image_name;
                    Toast.makeText(getContext(), responseString, Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getFileName(String path) {
        String filename = path.substring(path.lastIndexOf("/") + 1);
        return filename;
    }
}