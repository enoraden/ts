package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.ts.database.TableTaskList;
import myskysfa.com.ts.utils.DatabaseManager;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/05/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableTaskListAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableTaskListAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableTaskList> getDatabyCondition(Context context, String condition, Object param) {
        List<TableTaskList> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableTaskList.class);

            QueryBuilder<TableTaskList, Integer> queryBuilder = dao.queryBuilder();
            Where<TableTaskList, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableTaskList> getAllData(Context context) {
        List<TableTaskList> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableTaskList.class);

            QueryBuilder<TableTaskList, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableTaskList table, String form_id, String id_retrieval, String prospect_nbr,
                           String customer_nbr, String ticket_nbr, String job_type, String schedule_penarikan,
                           String status_penarikan, String username) {
        try {
            table.setfFORM_ID(form_id);
            table.setfID_RETRIEVAL(id_retrieval);
            table.setfPROSPECT_NBR(prospect_nbr);
            table.setfCUSTOMER_NBR(customer_nbr);
            table.setfTICKET_NBR(ticket_nbr);
            table.setfJOB_TYPE(job_type);
            table.setfSCHEDULE_PENARIKAN(schedule_penarikan);
            table.setfSTATUS_PENARIKAN(status_penarikan);
            table.setfUSERNAME(username);
            getHelper().getTableTaskListDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableTaskList.class);

            UpdateBuilder<TableTaskList, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String form_id, String id_retrieval, String prospect_nbr,
                              String customer_nbr, String ticket_nbr, String job_type, String schedule_penarikan,
                              String status_penarikan, String username, String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableTaskList.class);

            UpdateBuilder<TableTaskList, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableTaskList.fFORM_ID, form_id);
            updateBuilder.updateColumnValue(TableTaskList.fID_RETRIEVAL, id_retrieval);
            updateBuilder.updateColumnValue(TableTaskList.fPROSPECT_NBR, prospect_nbr);
            updateBuilder.updateColumnValue(TableTaskList.fCUSTOMER_NBR, customer_nbr);
            updateBuilder.updateColumnValue(TableTaskList.fTICKET_NBR, ticket_nbr);
            updateBuilder.updateColumnValue(TableTaskList.fJOB_TYPE, job_type);
            updateBuilder.updateColumnValue(TableTaskList.fSCHEDULE_PENARIKAN, schedule_penarikan);
            updateBuilder.updateColumnValue(TableTaskList.fSTATUS_PENARIKAN, status_penarikan);
            updateBuilder.updateColumnValue(TableTaskList.fUSERNAME, username);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableTaskList.class);

            DeleteBuilder<TableTaskList, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableTaskList> listTable = null;
        try {
            listTable = getHelper().getTableTaskListDAO().queryForAll();
            getHelper().getTableTaskListDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}