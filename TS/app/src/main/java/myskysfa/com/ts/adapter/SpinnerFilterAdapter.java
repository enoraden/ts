package myskysfa.com.ts.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/4/2016.
|---------------------------------------------------------------------------------------------------
*/
public class SpinnerFilterAdapter extends ArrayAdapter<String> {
    private Context context;
    private String[] itemsStatus;
    private Utils utils;

    public SpinnerFilterAdapter(Context _context, int textViewResourceId, String[] _itemsStatus) {
        super(_context, textViewResourceId, _itemsStatus);
        this.context        = _context;
        this.itemsStatus    = _itemsStatus;
        utils               = new Utils(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtSpinner = new TextView(context);
        txtSpinner.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
        txtSpinner.setTextSize(22);
        txtSpinner.setTextColor(Color.WHITE);
        txtSpinner.setText(itemsStatus[position]);

        return txtSpinner;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txtSpinner = new TextView(context);
        txtSpinner.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        txtSpinner.setTextSize(20);
        txtSpinner.setPadding(10, 5, 10, 5);
        txtSpinner.setTextColor(Color.BLACK);
        txtSpinner.setText(itemsStatus[position]);

        return txtSpinner;
    }
}