package myskysfa.com.ts.main.rejoin;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.RejoinTabLayoutAdapter;
import myskysfa.com.ts.database.TableFormRejoin;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.TableTaskList;
import myskysfa.com.ts.database.db_adapter.TableFormRejoinAdapter;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.database.db_adapter.TableTaskListAdapter;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/05/2016.
|---------------------------------------------------------------------------------------------------
*/
public class RejoinTabLayout extends AppCompatActivity {
    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;
    private LinearLayout tittlelayout, back, send;
    private TextView tittle;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_layout);

        saveTaskList();
        loadResources();
    }

    private void loadResources() {
        utils = new Utils(RejoinTabLayout.this);

        tittlelayout    = (LinearLayout) findViewById(R.id.tablayout_tittlelayout);
        back            = (LinearLayout) findViewById(R.id.tablayout_back);
        send            = (LinearLayout) findViewById(R.id.tablayout_send);
        tittle          = (TextView) findViewById(R.id.tablayout_tittle);

        tittlelayout.setVisibility(View.VISIBLE);
        back.setOnClickListener(back_listener);
        tittle.setText("Form Rejoin");
        send.setVisibility(View.GONE);

        tabLayout = (TabLayout) findViewById(R.id.tablayout_tabs);
        tabLayout.addTab(tabLayout.newTab().setText("PROFILE"));
        tabLayout.addTab(tabLayout.newTab().setText("BAP"));
        tabLayout.addTab(tabLayout.newTab().setText("SIGNATURE"));
        tabLayout.addTab(tabLayout.newTab().setText("RESULT"));

        final ViewPager viewpager               = (ViewPager) findViewById(R.id.tablayout_viewpager);
        final RejoinTabLayoutAdapter adapter    = new RejoinTabLayoutAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewpager.setOffscreenPageLimit(4);

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount       = viewGroup.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab     = (ViewGroup) viewGroup.getChildAt(j);
            int tabChildsCount  = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);

                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
                }
            }
        }
    }

    private String form_id() {
        dbAdapterLogLogin   = new TableLogLoginAdapter(RejoinTabLayout.this);
        listLogLogin        = dbAdapterLogLogin.getAllData(RejoinTabLayout.this);

        String employee_id = "", yy, mm, dd, hh, ss;

        if (listLogLogin.size() > 0) {
            employee_id = listLogLogin.get(listLogLogin.size() - 1).getfEMPLOYEE_ID();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
        Date now        = new Date();
        String strDate  = sdf.format(now);
        yy = strDate.substring(2, 4);
        mm = strDate.substring(5, 7);
        dd = strDate.substring(8, 10);
        hh = strDate.substring(11, 13);
        ss = strDate.substring(14, 16);

        StringBuilder sb = new StringBuilder();
        sb.append("P");
        sb.append(employee_id);
        sb.append(yy);
        sb.append(mm);
        sb.append(dd);
        sb.append(hh);
        sb.append(ss);

        return sb.toString();
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Shared Preferences
    |-----------------------------------------------------------------------------------------------
    */
    private void saveTaskList (){
        Bundle bundle               = getIntent().getExtras();
        String id_retrieval         = bundle.getString("id_retrieval");
        String prospect_nbr         = bundle.getString("prospect_nbr");
        String customer_nbr         = bundle.getString("customer_nbr");
        String ticket_nbr           = bundle.getString("ticket_nbr");
        String job_type             = bundle.getString("job_type");
        String schedule_penarikan   = bundle.getString("schedule_penarikan");
        String status_penarikan     = bundle.getString("status_penarikan");
        String username             = bundle.getString("username");

        TableTaskListAdapter dbAdapterTaskList = new TableTaskListAdapter(RejoinTabLayout.this);
        dbAdapterTaskList.deleteAllData();
        dbAdapterTaskList.insertData(new TableTaskList(), form_id(), id_retrieval, prospect_nbr, customer_nbr,
                ticket_nbr, job_type, schedule_penarikan, status_penarikan, username);

        //no_ticket untuk childview
        SharedPreferences preferences           = getSharedPreferences(getString(R.string.detail_id), Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor    = preferences.edit();
        shareEditor.putString("ticket_number", prospect_nbr);
        shareEditor.commit();
    }

    private void deleteTaskList() {
        TableTaskListAdapter dbAdapterTaskList = new TableTaskListAdapter(RejoinTabLayout.this);
        dbAdapterTaskList.deleteAllData();
        TableFormRejoinAdapter dbAdapterFormRejoin = new TableFormRejoinAdapter(RejoinTabLayout.this);
        dbAdapterFormRejoin.deleteAllData();

        SharedPreferences preferences           = getSharedPreferences(getString(R.string.detail_id), Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor    = preferences.edit();
        shareEditor.clear();
        shareEditor.commit();

        finish();
    }

    private View.OnClickListener back_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            exitFormDialog();
        }
    };

    @Override
    public void onBackPressed() {
        exitFormDialog();
    }

    private void exitFormDialog() {
        utils.loadAlertDialog(false, "Keluar Form!", "Apakah anda yakin ingin keluar dari form ?\n", "", "TIDAK", "YA");

        utils._twobtn_left.setBackgroundResource(R.drawable.button_green);
        utils._twobtn_right.setBackgroundResource(R.drawable.button_red);
        utils._twobtn_left.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
            }
        });

        utils._twobtn_right.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
                deleteTaskList();
            }
        });

        //utils.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        utils.dialog.setCancelable(false);
        utils.dialog.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        for (Fragment fragment : getSupportFragmentManager().getFragments()) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }
}
