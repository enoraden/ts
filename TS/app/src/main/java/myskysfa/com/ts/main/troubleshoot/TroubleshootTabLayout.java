package myskysfa.com.ts.main.troubleshoot;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.TroubleshootTabLayoutAdapter;
import myskysfa.com.ts.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TroubleshootTabLayout extends AppCompatActivity {
    private Utils utils;
    private LinearLayout tittlelayout, back, send;
    private TextView tittle;
    private TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tab_layout);

        saveTaskList();
        loadResources();
    }

    private void loadResources() {
        utils = new Utils(TroubleshootTabLayout.this);

        tittlelayout    = (LinearLayout) findViewById(R.id.tablayout_tittlelayout);
        back            = (LinearLayout) findViewById(R.id.tablayout_back);
        send            = (LinearLayout) findViewById(R.id.tablayout_send);
        tittle          = (TextView) findViewById(R.id.tablayout_tittle);

        tittlelayout.setVisibility(View.VISIBLE);
        back.setOnClickListener(back_listener);
        tittle.setText("Instalasi");
        send.setVisibility(View.GONE);

        tabLayout = (TabLayout) findViewById(R.id.tablayout_tabs);
        tabLayout.addTab(tabLayout.newTab().setText("PACKAGE"));
        tabLayout.addTab(tabLayout.newTab().setText("PAYMENT"));
        tabLayout.addTab(tabLayout.newTab().setText("LKT"));
        tabLayout.addTab(tabLayout.newTab().setText("CHECKLIST"));
        tabLayout.addTab(tabLayout.newTab().setText("SIGNATURE"));
        tabLayout.addTab(tabLayout.newTab().setText("STATUS"));

        final ViewPager viewpager               = (ViewPager) findViewById(R.id.tablayout_viewpager);
        final TroubleshootTabLayoutAdapter adapter    = new TroubleshootTabLayoutAdapter(getSupportFragmentManager(), tabLayout.getTabCount());
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        viewpager.setOffscreenPageLimit(6);

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount       = viewGroup.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab     = (ViewGroup) viewGroup.getChildAt(j);
            int tabChildsCount  = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);

                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
                }
            }
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Shared Preferences
    |-----------------------------------------------------------------------------------------------
    */
    private void saveTaskList (){
    }

    private void deleteTaskList() {
        TableFormAppAdapter dbAdapterFormApp = new TableFormAppAdapter(TroubleshootTabLayout.this);
        dbAdapterFormApp.deleteAllData();
        finish();
    }

    private View.OnClickListener back_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            exitFormDialog();
        }
    };

    @Override
    public void onBackPressed() {
        exitFormDialog();
    }

    private void exitFormDialog() {
        utils.loadAlertDialog(false, "Keluar Form!", "Apakah anda yakin ingin keluar dari form ?\n", "", "TIDAK", "YA");

        utils._twobtn_left.setBackgroundResource(R.drawable.button_green);
        utils._twobtn_right.setBackgroundResource(R.drawable.button_red);
        utils._twobtn_left.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
            }
        });

        utils._twobtn_right.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
                deleteTaskList();
            }
        });

        //utils.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        utils.dialog.setCancelable(false);
        utils.dialog.show();
    }
}