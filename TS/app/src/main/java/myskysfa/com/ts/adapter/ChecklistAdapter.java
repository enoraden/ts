package myskysfa.com.ts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import myskysfa.com.ts.R;
import myskysfa.com.ts.utils.CheckList;
import myskysfa.com.ts.utils.Utils;

import java.util.List;


public class ChecklistAdapter extends RecyclerView.Adapter<ChecklistAdapter.ViewHolder> {
    private List<CheckList> list;
    View itemLayoutView;
    ViewGroup mParent;
    private Utils utils;
    private Context mContext;
    public ChecklistAdapter(Context mContext, List<CheckList> list) {
        this.list = list;
        this.mContext = mContext;
        utils         = new Utils(mContext);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParent = parent;
        itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.checklist_items, parent, false);
//        ViewHolder viewHolder = new ViewHolder(itemLayoutView, new MyCustomEditTextListener());
        ViewHolder viewHolder = new ViewHolder(itemLayoutView);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        CheckList itemChecklist = new CheckList(mContext);
        itemChecklist = list.get(position);
        String labelS = itemChecklist.getCHECKLIST_LABEL();
        String label_editS = itemChecklist.getCHECKLIST_LABEL_EDIT();
        final String statusS = itemChecklist.getCHECKLIST_STATUS();
        String IdS = itemChecklist.getID_CHECKLIST();
        holder.checklistCB.setText(labelS);
//            holder.checklistET.setText(NoFormS);
        holder.checklistET.setText(label_editS);
        holder.checklistET.setHint(labelS);
        //in some cases, it will prevent unwanted situations
//        holder.checklistCB.setOnCheckedChangeListener(null);
//        holder.myCustomEditTextListener.updatePosition(holder.getAdapterPosition());
        holder.checklistET.setText(label_editS);
        //if true, your checkbox will be selected, else unselected
        if (statusS.equals("1")){
            holder.checklistCB.setChecked(true);
        } else {
            holder.checklistCB.setChecked(false);
        }

        holder.checklistCB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                //set your object's last status
//                holder.checklistCB.setChecked(isChecked);


                    if (holder.checklistCB.isChecked()){
                        Toast.makeText(mContext, "Checked", Toast.LENGTH_SHORT).show();
                        holder.checklistCB.setChecked(isChecked);
                        list.get(position).setCHECKLIST_STATUS("1");
                    } else {
                        Toast.makeText(mContext, "UnCheck", Toast.LENGTH_SHORT).show();
                        holder.checklistCB.setChecked(false);
                        list.get(position).setCHECKLIST_STATUS("0");
                    }

            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements TextWatcher {
        private EditText checklistET;
        private CheckBox checklistCB;
        //        public MyCustomEditTextListener myCustomEditTextListener;
//        public ViewHolder(View itemView, MyCustomEditTextListener myCustomEditTextListener) {
        public ViewHolder(View itemView) {
            super(itemView);
            this.checklistET = (EditText) itemLayoutView.findViewById(R.id.checklistET);
            this.checklistCB = (CheckBox) itemLayoutView.findViewById(R.id.checklistCB);
            this.checklistET.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
            this.checklistCB.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
//            this.myCustomEditTextListener = new MyCustomEditTextListener();
            this.checklistET.addTextChangedListener(this);
        }
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int position = getAdapterPosition();
            if (s.toString().trim().length()<1){
                checklistCB.setChecked(false);
                list.get(position).setCHECKLIST_STATUS("0");

            } else {
                list.get(position).setCHECKLIST_LABEL_EDIT(s.toString());
                list.get(position).setCHECKLIST_STATUS("1");
                checklistCB.setChecked(true);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    }


}

