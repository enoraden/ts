package myskysfa.com.ts.main;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.SpinnerFilterAdapter;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.main.instalasi.Instalasi;
import myskysfa.com.ts.main.master.MasterTabLayout;
import myskysfa.com.ts.main.rejoin.Rejoin;
import myskysfa.com.ts.main.troubleshoot.Troubleshoot;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.Utils;

import org.json.JSONObject;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MainMenuActivity extends AppCompatActivity {
    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;

    private Spinner spinnerfilter;
    private String[] array_spinnerfilter;
    private SpinnerFilterAdapter mAdapterFilter;
    private FragmentManager fragmentManager;

    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;
    private Boolean backPressAgain = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mainmenu_activity);

        loadResources();
        loadToolbarMenu();
    }

    private void loadResources() {
        utils = new Utils(MainMenuActivity.this);

        array_spinnerfilter = getResources().getStringArray(R.array.array_spinnerfilter);
        mAdapterFilter      = new SpinnerFilterAdapter(MainMenuActivity.this, R.layout.spinner_textview, array_spinnerfilter);
        mAdapterFilter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinnerfilter = (Spinner) findViewById(R.id.main_spinnerfilter);
        spinnerfilter.setAdapter(mAdapterFilter);
        spinnerfilter.setSelection(0);
        spinnerfilter.setOnItemSelectedListener(_spinnerfilter);
    }

    private void loadToolbarMenu() {
        ImageView imguser, imglogout;
        TextView username, userdetailone, userdetailtwo;
        String region_name = null;

        imguser       = (ImageView) findViewById(R.id.tbrmenu_imguser);
        username      = (TextView) findViewById(R.id.tbrmenu_username);
        userdetailone = (TextView) findViewById(R.id.tbrmenu_userdetailone);
        userdetailtwo = (TextView) findViewById(R.id.tbrmenu_userdetailtwo);
        imglogout     = (ImageView) findViewById(R.id.tbrmenu_imglogout);

        imglogout.setOnClickListener(imglogout_listener);

        dbAdapterLogLogin   = new TableLogLoginAdapter(MainMenuActivity.this);
        listLogLogin        = dbAdapterLogLogin.getAllData(MainMenuActivity.this);

        if (listLogLogin.size() > 0) {
            TableLogLogin item = listLogLogin.get(listLogLogin.size() - 1);
            String employeeid   = item.getfEMPLOYEE_ID();
            String fullname     = item.getfFULL_NAME();
            String usertype     = item.getfUSER_TYPE().toUpperCase();
            if (item.getfREGION_NAME() != null && !item.getfREGION_NAME().equalsIgnoreCase("")){
                region_name  = item.getfREGION_NAME().toUpperCase().substring(0, 1) +
                        item.getfREGION_NAME().toLowerCase().substring(1, item.getfREGION_NAME().length());
            }
            switch (usertype){
                case "S" :
                    usertype = "Sales";

                    break;
                case "T" :
                    usertype = "Teknisi";

                    break;
                case "V" :
                    usertype = "Verifikator";

                    break;
            }

            imguser.setImageResource(R.mipmap.profile);
            username.setText(fullname);
            userdetailone.setText(employeeid+" - "+region_name);
            userdetailtwo.setText(usertype);
        } else {
            username.setText("USERNAME");
            userdetailone.setText("USERDETAIL 1");
            userdetailtwo.setText("USERDETAIL 2");
        }
    }

    private View.OnClickListener imglogout_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            connDetect          = new ConnectionDetector(MainMenuActivity.this);
            isInternetPresent   = connDetect.isConnectingToInternet();

            if (isInternetPresent) {
                showLogoutDialog();
            } else {
                utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
            }
        }
    };

    public void showLogoutDialog() {
        utils.loadAlertDialog(false, "Logout!", "Apakah anda yakin ingin keluar aplikasi ?\n", "", "TIDAK", "YA");

        utils._twobtn_left.setBackgroundResource(R.drawable.button_green);
        utils._twobtn_right.setBackgroundResource(R.drawable.button_red);
        utils._twobtn_left.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
            }
        });

        utils._twobtn_right.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
                new AttempLogoutTask().execute();
            }
        });

        //utils.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        utils.dialog.show();
        utils.dialog.setCancelable(false);
    }

    private AdapterView.OnItemSelectedListener _spinnerfilter = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Fragment detail;
            String filterParam = parent.getItemAtPosition(position).toString();

            switch (filterParam){
                case "Instalasi" :
                    detail = new Instalasi();
                    loadResourceFrag(detail);
                    break;

                case "Troubleshoot" :
                    detail = new Troubleshoot();
                    loadResourceFrag(detail);
                    break;

                case "Rejoin" :
                    detail = new Rejoin();
                    loadResourceFrag(detail);
                    break;

                case "Data Master" :
                    detail = new MasterTabLayout();
                    loadResourceFrag(detail);
                    break;

                default :
                    Toast.makeText(MainMenuActivity.this, filterParam + " On Progress Bro", Toast.LENGTH_SHORT).show();
                    break;
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    public void onBackPressed() {
        if (backPressAgain){
            finish();
        }else{
            Toast.makeText(MainMenuActivity.this, "Tekan sekali lagi untuk keluar", Toast.LENGTH_SHORT).show();
            backPressAgain = true;
        }

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                backPressAgain = false;
            }
        }, 2000);
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Method for set fragment view
    |-----------------------------------------------------------------------------------------------
    */
    private void loadResourceFrag(Fragment fragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.main_framelayout, fragment).commit();
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get data from server
    |-----------------------------------------------------------------------------------------------
    */
    private class AttempLogoutTask extends AsyncTask<String, String, String> {
        private Boolean _status;

        private int _signal;
        private String _imei, _message;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils   = new Utils(MainMenuActivity.this);
            _signal = utils.checkSignal();
            _imei   = utils.getIMEI();
            _status = false;

            utils.showDialogLoading("Loading . . .");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                String _url         = ConnectionManager.CM_URL_LOGOUT;
                String _username    = "";
                String _response    = ConnectionManager.requestLogout(_url, _username, _imei, Config.version, String.valueOf(_signal));
                System.out.println("response: " + _response);

                JSONObject jsonObject = new JSONObject(_response.toString());

                if (jsonObject != null) {
                    _status  = jsonObject.getBoolean("status");
                    _message = jsonObject.getString("message");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return _message;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (utils.progressdialog.isShowing())
                utils.progressdialog.dismiss();

            dbAdapterLogLogin = new TableLogLoginAdapter(MainMenuActivity.this);
            dbAdapterLogLogin.deleteAllData();

            Intent exit = new Intent(MainMenuActivity.this, LoginActivity.class);
            exit.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS
                    | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(exit);
            finish();
            System.exit(0);
        }
    }
}