package myskysfa.com.ts.main.childview;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.MasterAPI;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/16/2016.
|---------------------------------------------------------------------------------------------------
*/
public class ChildPackage {
    private static Context context;
    private static View root;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;
    private int signal;
    private String imei;
    private TextView statusdata;
    private TextView no_form, brand, type, promo;

    private int totalPackage;
    private LinearLayout layoutgrey;

    private ArrayList<String> arrayAlacarte = new ArrayList<String>();

    public ChildPackage(Context _context, View _root) {
        this.context    = _context;
        this.root       = _root;
    }

    public void packageView() {
        loadResorce();
        loadListener();
        new getChildPackage().execute();

        Toast.makeText(context, "Please Unmark response getChildPackage", Toast.LENGTH_LONG).show();
    }

    private void loadResorce() {
        utils       = new Utils(context);

        statusdata  = (TextView) root.findViewById(R.id.chdpkg_statusdata);
        //------------------------------------------------------------------------------------------
        no_form     = (TextView) root.findViewById(R.id.chdpkg_no_form);
        brand       = (TextView) root.findViewById(R.id.chdpkg_brand);
        type        = (TextView) root.findViewById(R.id.chdpkg_type);
        promo       = (TextView) root.findViewById(R.id.chdpkg_promo);
        //------------------------------------------------------------------------------------------
        totalPackage    = 0;
        layoutgrey      = (LinearLayout) root.findViewById(R.id.chdpkg_layoutpackage);
        layoutgrey.removeAllViews();
    }

    private void loadListener() {
        no_form.setText("-");
        brand.setText("-");
        type.setText("-");
        promo.setText("-");
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data Child Package Server
    |-----------------------------------------------------------------------------------------------
    */
    private class getChildPackage extends AsyncTask<String, String, String> {
        private SharedPreferences preferences;
        private Boolean status;
        private String data;
        private String js_no_form, js_brand, js_type, js_promo;

        private JSONArray js_data_package, js_data_alacarte;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            statusdata.setText("Loading . . .");
            statusdata.setVisibility(View.VISIBLE);

            preferences = context.getSharedPreferences(context.getString(R.string.detail_id), Context.MODE_PRIVATE);
            utils       = new Utils(context);
            signal      = utils.checkSignal();
            imei        = utils.getIMEI();
            status      = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(context);
                listLogLogin        = dbAdapterLogLogin.getAllData(context);

                if (listLogLogin.size() > 0) {
                    String url          = ConnectionManager.CM_URL_FTDTD_LIST;
                    String no_ticket    = preferences.getString("no_ticket", null);
                    String token        = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String paramURL     = url + "/" + no_ticket;

                    //TDT String response     = ConnectionManager.requestDataMaster(paramURL, imei, token, Config.version, String.valueOf(signal));
                    MasterAPI api   = new MasterAPI();
                    String response = api.detail_data_package();

                    Log.d("TSApp", "response= " + response);

                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        status  = jsonObject.getBoolean("status");
                        data    = jsonObject.getString("data");

                        if (status) {
                            JSONObject objData = new JSONObject(data.toString());
                            //----------------------------------------------------------------------
                            js_no_form  = objData.getString("no_form");
                            js_brand    = objData.getString("brand");
                            js_type     = objData.getString("type");
                            js_promo    = objData.getString("promo");

                            js_data_package = objData.getJSONArray("data_package");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (status) {
                    statusdata.setText("Finish");
                    statusdata.setVisibility(View.GONE);
                    //------------------------------------------------------------------------------
                    no_form.setText(js_no_form);
                    brand.setText(js_brand);
                    type.setText(js_type);
                    promo.setText(js_promo);
                    //------------------------------------------------------------------------------
                    for(int i = 0; i < js_data_package.length(); i++)
                    {
                        JSONObject objPackage   = js_data_package.getJSONObject(i);
                        String name_basic       = objPackage.getString("name_basic");

                        js_data_alacarte        = objPackage.getJSONArray("data_alacarte");
                        for(int b = 0; b < js_data_alacarte.length(); b++)
                        {
                            JSONObject objAlacarte  = js_data_alacarte.getJSONObject(b);
                            String name_alacarte    = objAlacarte.getString("name_alacarte");

                            addListAlacarte(name_alacarte);
                        }

                        addListPackage(i, name_basic);
                    }
                } else {
                    statusdata.setText("Gagal terhubung ke server, silahkan dicoba kembali.");
                    statusdata.setVisibility(View.VISIBLE);
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "detail_inst_troub");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //7.5   = 5dp, 15.0  = 10dp, 22.5  = 15dp
    private void addListPackage(int total_data, String name_basic) {
        totalPackage = total_data+1;

        //Layout Grey
        LinearLayout.LayoutParams pLinearLayoutGrey = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout LinearLayoutGrey = new LinearLayout(context);
        LinearLayoutGrey.setLayoutParams(pLinearLayoutGrey);
        LinearLayoutGrey.setOrientation(LinearLayout.VERTICAL);
        LinearLayoutGrey.setPadding((int) 7.5, 0, (int) 7.5, (int) 7.5);
        LinearLayoutGrey.setBackgroundResource(R.color.grey_5);
        ((LinearLayout) layoutgrey).addView(LinearLayoutGrey);

        //Layout White
        LinearLayout.LayoutParams pLinearLayoutWhite = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout LinearLayoutWhite = new LinearLayout(context);
        LinearLayoutWhite.setLayoutParams(pLinearLayoutWhite);
        LinearLayoutWhite.setOrientation(LinearLayout.VERTICAL);
        LinearLayoutWhite.setPadding((int) 22.5, (int) 22.5, (int) 22.5, (int) 22.5);
        LinearLayoutWhite.setBackgroundResource(R.color.white);
        ((LinearLayout) LinearLayoutGrey).addView(LinearLayoutWhite);

        //Layout Package
        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout LinearPackage = new LinearLayout(context);
        LinearPackage.setLayoutParams(pLinearPackage);
        LinearPackage.setOrientation(LinearLayout.VERTICAL);
        LinearPackage.setGravity(Gravity.CENTER);
        ((LinearLayout) LinearLayoutWhite).addView(LinearPackage);

            LinearLayout.LayoutParams p_chdpkg_package = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView chdpkg_package = new TextView(context);
            chdpkg_package.setLayoutParams(p_chdpkg_package);
            chdpkg_package.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
            chdpkg_package.setTextColor(Color.BLACK);
            chdpkg_package.setTextSize(16);
            chdpkg_package.setText("Package "+totalPackage);
            ((LinearLayout) LinearPackage).addView(chdpkg_package);

        //Layout Basic
        LinearLayout.LayoutParams pLinearBasic = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearBasic.setMargins((int) 22.5, (int) 15.0, 0, 0);
        LinearLayout LinearBasic = new LinearLayout(context);
        LinearBasic.setLayoutParams(pLinearBasic);
        LinearBasic.setOrientation(LinearLayout.HORIZONTAL);
        LinearBasic.setGravity(Gravity.CENTER);
        ((LinearLayout) LinearLayoutWhite).addView(LinearBasic);

            LinearLayout.LayoutParams p_chdpkg_basic = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
            TextView chdpkg_basic = new TextView(context);
            chdpkg_basic.setLayoutParams(p_chdpkg_basic);
            chdpkg_basic.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
            chdpkg_basic.setTextColor(Color.BLACK);
            chdpkg_basic.setTextSize(16);
            chdpkg_basic.setText("Basic");
            ((LinearLayout) LinearBasic).addView(chdpkg_basic);

            LinearLayout.LayoutParams p_adt_basic = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            p_adt_basic.setMargins((int) 15.0, 0, (int) 15.0, 0);
            TextView adt_basic = new TextView(context);
            adt_basic.setLayoutParams(p_adt_basic);
            adt_basic.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
            adt_basic.setTextColor(Color.BLACK);
            adt_basic.setTextSize(16);
            adt_basic.setText(":");
            ((LinearLayout) LinearBasic).addView(adt_basic);

            LinearLayout.LayoutParams p_chdpkg_dtl_basic = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
            TextView chdpkg_dtl_basic = new TextView(context);
            chdpkg_dtl_basic.setLayoutParams(p_chdpkg_dtl_basic);
            chdpkg_dtl_basic.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
            chdpkg_dtl_basic.setTextColor(Color.BLACK);
            chdpkg_dtl_basic.setTextSize(16);
            chdpkg_dtl_basic.setText(name_basic);
            ((LinearLayout) LinearBasic).addView(chdpkg_dtl_basic);

        //Layout Alacarte
        LinearLayout.LayoutParams pLinearAlacarte = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearAlacarte.setMargins((int) 22.5, (int) 15.0, 0, 0);
        LinearLayout LinearAlacarte = new LinearLayout(context);
        LinearAlacarte.setLayoutParams(pLinearAlacarte);
        LinearAlacarte.setOrientation(LinearLayout.HORIZONTAL);
        LinearAlacarte.setGravity(Gravity.CENTER | Gravity.RIGHT);
        ((LinearLayout) LinearLayoutWhite).addView(LinearAlacarte);

            LinearLayout.LayoutParams p_chdpkg_alacarte = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            TextView chdpkg_alacarte = new TextView(context);
            chdpkg_alacarte.setLayoutParams(p_chdpkg_alacarte);
            chdpkg_alacarte.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
            chdpkg_alacarte.setTextColor(Color.WHITE);
            chdpkg_alacarte.setTextSize(16);
            chdpkg_alacarte.setBackgroundResource(R.color.blue_10);
            chdpkg_alacarte.setPadding((int) 15.0, (int) 7.5, (int) 15.0, (int) 7.5);
            chdpkg_alacarte.setText("Alacarte");
            ((LinearLayout) LinearAlacarte).addView(chdpkg_alacarte);

        //Layout Name Alacarte
        LinearLayout.LayoutParams pLinearNameAlacarte = new LinearLayout.LayoutParams
                (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearNameAlacarte.setMargins((int) 22.5, (int) 15.0, 0, 0);
        LinearLayout LinearNameAlacarte = new LinearLayout(context);
        LinearNameAlacarte.setLayoutParams(pLinearNameAlacarte);
        LinearNameAlacarte.setOrientation(LinearLayout.HORIZONTAL);
        LinearNameAlacarte.setGravity(Gravity.CENTER | Gravity.RIGHT);
        LinearNameAlacarte.setTag("LIST_ALACARTE");
        ((LinearLayout) LinearLayoutWhite).addView(LinearNameAlacarte);
    }

    private void addListAlacarte(final String name_alacarte) {

        layoutgrey.post(new Runnable() {
            @Override
            public void run() {
                Log.d("tdt", "name_alacarte = " + String.valueOf(name_alacarte));

                LinearLayout mLayoutList = (LinearLayout) layoutgrey.findViewWithTag("LIST_ALACARTE");
                LinearLayout.LayoutParams p_chdpkg_namealacarte = new LinearLayout.LayoutParams
                        (LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

                try {
                    //mLayoutList.removeAllViews();

                    LinearLayout.LayoutParams pLinearNameAlacarte = new LinearLayout.LayoutParams
                            (LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    pLinearNameAlacarte.setMargins((int) 22.5, (int) 15.0, 0, 0);
                    LinearLayout LinearNameAlacarte = new LinearLayout(context);
                    LinearNameAlacarte.setLayoutParams(pLinearNameAlacarte);
                    LinearNameAlacarte.setOrientation(LinearLayout.HORIZONTAL);
                    LinearNameAlacarte.setGravity(Gravity.CENTER | Gravity.RIGHT);
                    ((LinearLayout) mLayoutList).addView(LinearNameAlacarte);

                    TextView chdpkg_namealacarte = new TextView(context);
                    chdpkg_namealacarte.setLayoutParams(p_chdpkg_namealacarte);
                    chdpkg_namealacarte.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
                    chdpkg_namealacarte.setTextColor(Color.WHITE);
                    chdpkg_namealacarte.setTextSize(16);
                    chdpkg_namealacarte.setBackgroundResource(R.color.grey_8);
                    chdpkg_namealacarte.setPadding((int) 15.0, (int) 7.5, (int) 15.0, (int) 7.5);
                    chdpkg_namealacarte.setText(name_alacarte);
                    ((LinearLayout) LinearNameAlacarte).addView(chdpkg_namealacarte);

                } catch (NullPointerException e) {
                }


            }
        });



    }
}