package myskysfa.com.ts.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableListMain;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class ListMainAdapter extends RecyclerView.Adapter<ListMainAdapter.ViewHolder> {
    public static List<TableListMain> itemData = new ArrayList<>();
    private View itemLayoutView;

    public ListMainAdapter(List<TableListMain> list) {
        this.itemData   = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView          = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_ticket, parent, false);
        ViewHolder viewHolder   = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableListMain item = itemData.get(position);

        if (item != null) {
            holder.icon.setText(item.getfNATURE_CATEGORY().toString().toUpperCase().substring(0, 3));
            holder.number.setText(item.getfTICKET_NUMBER());

            if (item.getfSTATUS_TICKET().equals("0")){
                holder.status.setText("NEW");
            }else if (item.getfSTATUS_TICKET().equals("1")){
                holder.status.setText("RESOLVE");
            }else{
                holder.status.setText("ACTIVE");
            }

            holder.detail_one.setText(item.getfCUSTOMER_NAME()+" - "+item.getfCUSTOMER_ID()); //CUSTNAME - CUSTNBR
            holder.detail_two.setText(item.getfNATURE_CODE()+" - "+item.getfNATURE_DESCR()); //NATURECODE - NATUREDESCR
            holder.detail_three.setText(item.getfBRAND()+" - "+"Schedule Date : "+item.getfSCHEDULE_DATE()); //BRAND - SCHEDULEDATE
        }
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView icon, number, detail_one, detail_two, status, detail_three;

        public ViewHolder(View itemView) {
            super(itemView);
            icon            = (TextView) itemLayoutView.findViewById(R.id.itemticket_icon);
            number          = (TextView) itemLayoutView.findViewById(R.id.itemticket_number);
            status          = (TextView) itemLayoutView.findViewById(R.id.itemticket_status);
            detail_one      = (TextView) itemLayoutView.findViewById(R.id.itemticket_detail_one);
            detail_two      = (TextView) itemLayoutView.findViewById(R.id.itemticket_detail_two);
            detail_three    = (TextView) itemLayoutView.findViewById(R.id.itemticket_detail_three);
        }
    }
}