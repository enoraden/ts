package myskysfa.com.ts.main.troubleshoot;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.ListMainAdapter;
import myskysfa.com.ts.database.TableListMain;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableListMainAdapter;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.InitData;
import myskysfa.com.ts.utils.Utils;
import myskysfa.com.ts.widget.RecyclerItemClickListener;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import static myskysfa.com.ts.adapter.ListMainAdapter.itemData;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Troubleshoot extends Fragment {
    private ListMainAdapter adapter;
    private TableListMainAdapter dbAdapterTroubleshoot;
    private List<TableListMain> listTroubleshoot;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;

    private TextView info;
    private RecyclerView recyclerview;
    private SwipeRefreshLayout swiperefresh;

    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.list_menu, container, false);

        info = (TextView) viewGroup.findViewById(R.id.listmenu_info);
        recyclerview = (RecyclerView) viewGroup.findViewById(R.id.listmenu_recyclerview);
        swiperefresh = (SwipeRefreshLayout) viewGroup.findViewById(R.id.listmenu_swiperefresh);

        swiperefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swiperefresh.setEnabled(true);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        refreshItems();
        loadListener();
        return viewGroup;
    }


    private void refreshItems() {
        utils = new Utils(getActivity());
        info.setVisibility(View.GONE);

        connDetect = new ConnectionDetector(getActivity());
        isInternetPresent = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            new getDataTroubleshoot().execute();
        } else {
            if (swiperefresh.isShown()) {
                swiperefresh.setRefreshing(false);
            }

            utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
        }
    }

    /*
    |---------------------------------------------------------------------------------------------------
    | Get Data Troubleshoot From Server
    |---------------------------------------------------------------------------------------------------
    */
    private class getDataTroubleshoot extends AsyncTask<String, String, String> {
        private Boolean _status;
        private JSONArray _dataarray;
        private int _signal;
        private String _imei;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swiperefresh.setRefreshing(true);

            utils = new Utils(getActivity());
            _signal = utils.checkSignal();
            _imei = utils.getIMEI();
            _status = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin = new TableLogLoginAdapter(getActivity());
                listLogLogin = dbAdapterLogLogin.getAllData(getActivity());

                if (listLogLogin.size() > 0) {
                    String _url = ConnectionManager.CM_URL_GET_TICKET;
                    String _employee_id = listLogLogin.get(listLogLogin.size() - 1).getfEMPLOYEE_ID();
                    String _token = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String paramurl = _url + "/" + _employee_id + "/" + "TEC";

                    String _response = ConnectionManager.requestDataMaster(paramurl, _imei, _token, Config.version, String.valueOf(_signal));

                    /*
                    MasterAPI api       = new MasterAPI();
                    String _response    = api.inst_troub();
                    */

                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status = jsonObject.getBoolean("status");
                        _dataarray = jsonObject.getJSONArray("data");

                        if (_status) {
                            dbAdapterTroubleshoot = new TableListMainAdapter(getActivity());
                            dbAdapterTroubleshoot.deleteAll(getActivity());

                            for (int i = 0; i < _dataarray.length(); i++) {
                                JSONObject objData = _dataarray.getJSONObject(i);

                                String ticket_number = objData.getString("ticket_number");
                                String customer_id = objData.getString("customer_id");
                                String customer_nbr = objData.getString("customer_nbr");
                                String customer_name = objData.getString("customer_name");
                                String assign_to = objData.getString("assign_to");
                                String nature_code = objData.getString("nature_code");
                                String nature_descr = objData.getString("nature_descr");
                                String nature_category = objData.getString("nature_category");
                                String brand = objData.getString("brand");
                                String notes = objData.getString("notes");
                                String schedule_date = objData.getString("schedule_date");
                                String status_ticket = objData.getString("status_ticket");

                                dbAdapterTroubleshoot = new TableListMainAdapter(getActivity());
                                dbAdapterTroubleshoot.insertData(new TableListMain(), ticket_number, customer_id,
                                        customer_nbr, customer_name, assign_to, nature_code, nature_descr,
                                        nature_category, brand, notes, schedule_date, status_ticket);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (swiperefresh.isShown()) {
                    swiperefresh.setRefreshing(false);
                }

                if (_status) {
                    showList();

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "Data Troubleshoot");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerview.setLayoutManager(layoutParams);

        dbAdapterTroubleshoot = new TableListMainAdapter(getActivity());
        listTroubleshoot = dbAdapterTroubleshoot.getAllData(getActivity());
        adapter = new ListMainAdapter(listTroubleshoot);
        recyclerview.setAdapter(adapter);

        if (listTroubleshoot.size() > 0) {
            info.setVisibility(View.GONE);
        } else {
            info.setVisibility(View.VISIBLE);
            info.setText("Tidak Ada Data Troubleshoot");
        }
    }

    private void loadListener() {
        recyclerview.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), recyclerview,
                new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        Vibrator vibrate = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrate.vibrate(100);

                        Intent intent = new Intent(getActivity(), TroubleshootActivity.class);
                        putExtraInstalasi(intent, position, "troubleshoot");

                        startActivity(intent);
                    }

                    @Override
                    public void onItemLongClick(View view, int position) {
                        Vibrator vibrate = (Vibrator) getActivity().getSystemService(Context.VIBRATOR_SERVICE);
                        vibrate.vibrate(100);

                        dbAdapterTroubleshoot = new TableListMainAdapter(getActivity());
                        listTroubleshoot = dbAdapterTroubleshoot.getAllData(getActivity());
                        TableListMain item = listTroubleshoot.get(position);
                        String status_ticket = item.getfSTATUS_TICKET();
                        String ticket_number = item.getfTICKET_NUMBER();

                        if (status_ticket.equalsIgnoreCase("0")) { // NEW
                            Toast.makeText(getActivity(), "Silahkan pilih data yang lain", Toast.LENGTH_LONG).show();

                        } else if (status_ticket.equalsIgnoreCase("1")) { // RESOLVE
                            InitData initData = new InitData(getActivity());
                            initData.activationTaskDialog(ticket_number);

                        } else {
                            Intent intent = new Intent(getActivity(), TroubleshootActivity.class);
                            putExtraInstalasi(intent, position, "tutup_tugas");

                            startActivity(intent);
                        }
                    }
                }));
    }

    private void putExtraInstalasi(Intent _intent, int _position, String case_item) {
        _intent.putExtra("case_item", case_item);
        _intent.putExtra("ticket_number", itemData.get(_position).getfTICKET_NUMBER());
        _intent.putExtra("customer_id", itemData.get(_position).getfCUSTOMER_ID());
        _intent.putExtra("customer_nbr", itemData.get(_position).getfCUSTOMER_NBR());
        _intent.putExtra("customer_name", itemData.get(_position).getfCUSTOMER_NAME());
        _intent.putExtra("assign_to", itemData.get(_position).getfASSIGN_TO());
        _intent.putExtra("nature_code", itemData.get(_position).getfNATURE_CODE());
        _intent.putExtra("nature_descr", itemData.get(_position).getfNATURE_DESCR());
        _intent.putExtra("nature_category", itemData.get(_position).getfNATURE_CATEGORY());
        _intent.putExtra("brand", itemData.get(_position).getfBRAND());
        _intent.putExtra("notes", itemData.get(_position).getfNOTES());
        _intent.putExtra("schedule_date", itemData.get(_position).getfSCHEDULE_DATE());
        _intent.putExtra("status_ticket", itemData.get(_position).getfSTATUS_TICKET());
    }
}