package myskysfa.com.ts.adapter.master;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableFreeTrialSOH;

import java.util.ArrayList;
import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterFreeTrialSOHAdapter extends RecyclerView.Adapter<MasterFreeTrialSOHAdapter.ViewHolder> {
    public static List<TableFreeTrialSOH> itemData = new ArrayList<>();
    private View itemLayoutView;
    private ViewGroup mViewGroup;

    public MasterFreeTrialSOHAdapter(List<TableFreeTrialSOH> list) {
        this.itemData = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView          = LayoutInflater.from(parent.getContext()).inflate(R.layout.master_item, parent, false);
        ViewHolder viewHolder   = new ViewHolder(itemLayoutView);
        mViewGroup              = parent;
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableFreeTrialSOH item = itemData.get(position);

        if (item != null) {
            holder.icon.setText(item.getfITEM_CODE().substring(0, 1));
            holder.id.setText(item.getfSERIAL_NO());
            holder.detail.setText(item.getfITEM_DESCR());
            holder.date.setText(item.getfQTY());
        }

        switch (item.getfITEM_CODE()){
            case "VC" :
                holder.icon.setBackgroundColor(mViewGroup.getResources().getColor(R.color.green_10));
                break;
            case "LNB" :
                holder.icon.setBackgroundColor(mViewGroup.getResources().getColor(R.color.blue_10));
                break;
            case "ODU" :
                holder.icon.setBackgroundColor(mViewGroup.getResources().getColor(R.color.yellow_10));
                break;
            case "DSD" :
                holder.icon.setBackgroundColor(mViewGroup.getResources().getColor(R.color.red_10));
                break;
        }
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView icon, id, detail, date;

        public ViewHolder(View itemView) {
            super(itemView);

            icon        = (TextView) itemLayoutView.findViewById(R.id.masteritem_icon);
            id          = (TextView) itemLayoutView.findViewById(R.id.masteritem_id);
            detail      = (TextView) itemLayoutView.findViewById(R.id.masteritem_detail);
            date        = (TextView) itemLayoutView.findViewById(R.id.masteritem_date);
        }
    }
}