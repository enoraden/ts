package myskysfa.com.ts.main.rejoin;

import android.Manifest;
import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import cz.msebera.android.httpclient.Header;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.SpinnerArrayAdapter;
import myskysfa.com.ts.database.TableFormRejoin;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.TableTaskList;
import myskysfa.com.ts.database.db_adapter.TableFormRejoinAdapter;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.database.db_adapter.TableTaskListAdapter;
import myskysfa.com.ts.utils.AlbumStorageDirFactory;
import myskysfa.com.ts.utils.BaseAlbumDirFactory;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.PermissionUtils;
import myskysfa.com.ts.utils.Utils;
import myskysfa.com.ts.widget.WorkaroundMapFragment;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/05/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Result extends Fragment implements OnMapReadyCallback {
    private static String id_retrieval, prospect_nbr = "";
    private String form_id, customer_nbr = "", ticket_nbr = "", job_type,
            schedule_penarikan, status_penarikan, username = "";

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private String _token, _imei;
    private int _signal;

    private Utils utils;

    private ScrollView scrollview;
    private TextView longitude, latitude;
    private ImageButton btnkoordinat, btnupload;
    private Switch success, subscribe, reschedule;
    private LinearLayout reasonlayout;
    private TextView date, time, percentage;
    private String[] array_reason;
    private Spinner reason;
    private SpinnerArrayAdapter spinnerReason;
    private ImageView preview;
    private ProgressBar progressbar;
    private Button send;

    private GoogleMap gMap;
    private SupportMapFragment mapFragment;
    private Marker marker;
    private Location location;

    private String addressLine;
    private TextView txtAddressLine;
    private boolean mPermissionDenied = false;

    private static String result_code;
    private Object result_note;

    private static final int CAMERA_TAKE_REQUEST_CODE = 333;
    private static final int GALLERY_CAPTURE_IMAGE_REQUEST_CODE = 313;
    private File myFile;
    private String image_path = null;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private Uri fileUri;
    private static AlbumStorageDirFactory mAlbumStorageDirFactory = null;

    private NotificationManager notifManager;
    private NotificationCompat.Builder notifBuilder;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.r_result, container, false);
        loadResources(viewGroup);
        loadListener();
        loadMap();
        setLatLong();
        return viewGroup;
    }

    private void loadResources(ViewGroup root) {
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();

        utils           = new Utils(getActivity());
        scrollview      = (ScrollView) root.findViewById(R.id.rslt_scrollview);
        longitude       = (TextView) root.findViewById(R.id.rslt_longitude);
        latitude        = (TextView) root.findViewById(R.id.rslt_latitude);
        btnkoordinat    = (ImageButton) root.findViewById(R.id.rslt_btnkoordinat);
        success         = (Switch) root.findViewById(R.id.rslt_berhasil);
        subscribe       = (Switch) root.findViewById(R.id.rslt_berlangganan);
        reschedule      = (Switch) root.findViewById(R.id.rslt_reschedule);
        reasonlayout    = (LinearLayout) root.findViewById(R.id.rslt_reasonlayout);
        date            = (TextView) root.findViewById(R.id.rslt_date);
        time            = (TextView) root.findViewById(R.id.rslt_time);
        reason          = (Spinner) root.findViewById(R.id.rslt_reason);
        btnupload       = (ImageButton) root.findViewById(R.id.rslt_btnupload);
        preview         = (ImageView) root.findViewById(R.id.rslt_preview);
        percentage      = (TextView) root.findViewById(R.id.rslt_percentage);
        progressbar     = (ProgressBar) root.findViewById(R.id.rslt_progressbar);
        send            = (Button) root.findViewById(R.id.rslt_send);

        success.setOnCheckedChangeListener(success_listener);
        subscribe.setOnCheckedChangeListener(subscribe_listener);
        reschedule.setOnCheckedChangeListener(reschedule_listener);
        date.setOnClickListener(date_listener);
        time.setOnClickListener(time_listener);
        btnupload.setOnClickListener(btnupload_listener);
        send.setOnClickListener(send_listener);
    }

    private void loadListener() {
        TableTaskListAdapter dbAdapterTaskList  = new TableTaskListAdapter(getActivity());
        List<TableTaskList> listTaskList        = dbAdapterTaskList.getAllData(getActivity());

        if (listTaskList.size() > 0) {
            form_id             = listTaskList.get(listTaskList.size() - 1).getfFORM_ID();
            id_retrieval        = listTaskList.get(listTaskList.size() - 1).getfID_RETRIEVAL();
            prospect_nbr        = listTaskList.get(listTaskList.size() - 1).getfPROSPECT_NBR();
            customer_nbr        = listTaskList.get(listTaskList.size() - 1).getfCUSTOMER_NBR();
            ticket_nbr          = listTaskList.get(listTaskList.size() - 1).getfTICKET_NBR();
            job_type            = listTaskList.get(listTaskList.size() - 1).getfJOB_TYPE();
            schedule_penarikan  = listTaskList.get(listTaskList.size() - 1).getfSCHEDULE_PENARIKAN();
            status_penarikan    = listTaskList.get(listTaskList.size() - 1).getfSTATUS_PENARIKAN();
            username            = listTaskList.get(listTaskList.size() - 1).getfUSERNAME();
        }

        array_reason    = getResources().getStringArray(R.array.array_reason);
        spinnerReason   = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_reason);
        spinnerReason.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        reason.setAdapter(spinnerReason);
        reason.setSelection(0);

        reason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                result_note     = parent.getItemAtPosition(position);
                int pos         = parent.getSelectedItemPosition();

                if (pos == 0) {
                    result_code = "FT-R01";
                } else if (pos == 1) {
                    result_code = "FT-R02";
                } else if (pos == 2) {
                    result_code = "FT-R03";
                } else {
                    result_code = "FT-R04";
                }

                Log.d("result_code", result_code);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void loadMap() {
        if (gMap == null) {
            FragmentManager fm  = getChildFragmentManager();
            mapFragment         = (WorkaroundMapFragment) fm.findFragmentById(R.id.rslt_map);

            if (mapFragment == null) {
                mapFragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.rslt_map, mapFragment).commit();
            }

            ((WorkaroundMapFragment) fm.findFragmentById(R.id.rslt_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
                @Override
                public void onTouch() {
                    scrollview.requestDisallowInterceptTouchEvent(true);
                }
            });

            mapFragment.getMapAsync(this);
        }
    }

    private double change_latitude, change_longitude;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap = googleMap;
        enableMyLocation();

        gMap.getUiSettings().setZoomControlsEnabled(false);

        gMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener(){
            @Override
            public boolean onMyLocationButtonClick() {
                return false;
            }
        });

        gMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener(){
            @Override
            public boolean onMarkerClick(Marker marker) {
                return false;
            }
        });

        gMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
            @Override
            public void onMyLocationChange(Location loc) {
                change_latitude  = loc.getLatitude();
                change_longitude = loc.getLongitude();

                location                = loc;
                List<Address> addresses = null;
                Geocoder geocoder       = new Geocoder(getActivity(), Locale.getDefault());

                if (marker == null) {
                    marker = gMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),
                            location.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .draggable(true).alpha(0.7f));

                    gMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                            location.getLongitude()), 17.0f));

                    try {
                        addresses = geocoder.getFromLocation(change_latitude, change_longitude, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (addresses != null && addresses.size() != 0) {
                        addressLine = addresses.get(0).getAddressLine(0);
                    } else {
                        addressLine = "";
                    }
                }
            }
        });

        gMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener(){

            @Override
            public void onMarkerDragStart(Marker mark) {

            }

            @Override
            public void onMarkerDrag(Marker mark) {

            }

            @Override
            public void onMarkerDragEnd(Marker mark) {
                LatLng dragPosition     = mark.getPosition();
                double drag_latitude    = dragPosition.latitude;
                double drag_longitude   = dragPosition.longitude;
                List<Address> addresses = null;
                Geocoder geocoder       = new Geocoder(getActivity(), Locale.getDefault());
                int distance            = distance(location, dragPosition);

                if (distance > 100) {
                    utils.showErrorDialog("Alert!!", "Jarak Marker Lebih Dari 100 Meter, Silahkan pindahkan posisi marker.\n");
                    marker.remove();
                    marker = null;
                    return;
                }

                try {
                    addresses = geocoder.getFromLocation(drag_latitude, drag_longitude, 1);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (addresses != null && addresses.size() != 0) {
                    addressLine = addresses.get(0).getAddressLine(0);
                } else {
                    addressLine = "";
                }
            }
        });

        gMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getActivity().getLayoutInflater().inflate(R.layout.info_windows, null);
                txtAddressLine = (TextView) v.findViewById(R.id.address_line);
                txtAddressLine.setText(addressLine);

                return v;
            }
        });
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(getActivity(), 1, Manifest.permission.ACCESS_FINE_LOCATION, true);

        } else if (gMap != null) {
            gMap.setMyLocationEnabled(true);
        }
    }

    public static int distance(Location StartP, LatLng EndP) {
        double earthRadius  = 6371000; //meters
        double lat1         = StartP.getLatitude();
        double lng1         = StartP.getLongitude();
        double lat2         = EndP.latitude;
        double lng2         = EndP.longitude;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a    = Math.sin(dLat / 2)
                * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2))
                * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c    = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        int dist    = (int) (earthRadius * c);
        return dist;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != 1) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults, Manifest.permission.ACCESS_FINE_LOCATION)) {
            enableMyLocation();
        } else {
            mPermissionDenied = true;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPermissionDenied) {
            showMissingPermissionError();
            mPermissionDenied = false;
        }

        if (!checkReady()) {
            return;
        }

        gMap.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!checkReady()) {
            return;
        }
        gMap.clear();

        if (marker != null) {
            marker.remove();
            marker = null;
        }
    }

    private boolean checkReady() {
        if (gMap == null) {
            return false;
        }
        return true;
    }

    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog.newInstance(true).show(getChildFragmentManager(), "dialog");
    }

    private void setLatLong() {
        utils = new Utils(getActivity());
        utils.getLatLong();
        double _latitude    = utils.latitude;
        double _longitude   = utils.longitude;

        if (_latitude == 0 || _longitude == 0) {
            latitude.setText("0");
            longitude.setText("0");

            send.setEnabled(false);
            send.setClickable(false);
            send.setBackgroundResource(R.color.grey_6);
            Toast.makeText(getActivity(), "Latitude dan longitude kosong", Toast.LENGTH_SHORT).show();

        } else {
            latitude.setText(String.valueOf(_latitude));
            longitude.setText(String.valueOf(_longitude));

            send.setEnabled(true);
            send.setClickable(true);
            send.setBackgroundResource(R.drawable.button_blue);
        }

        btnkoordinat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                utils = new Utils(getActivity());
                utils.getLatLong();
                double _latitude    = utils.latitude;
                double _longitude   = utils.longitude;

                if (_latitude == 0 || _longitude == 0) {
                    latitude.setText("0");
                    longitude.setText("0");

                    send.setEnabled(false);
                    send.setClickable(false);
                    send.setBackgroundResource(R.color.grey_6);

                    Toast.makeText(getActivity(), "Latitude dan longitude kosong", Toast.LENGTH_SHORT).show();

                } else {
                    latitude.setText(String.valueOf(_latitude));
                    longitude.setText(String.valueOf(_longitude));

                    send.setEnabled(true);
                    send.setClickable(true);
                    send.setBackgroundResource(R.drawable.button_blue);

                    Toast.makeText(getActivity(), "Latitude : "+String.valueOf(_latitude)+", Longitude : "+String.valueOf(_longitude), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private CompoundButton.OnCheckedChangeListener success_listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                subscribe.setClickable(false);
                reschedule.setClickable(false);
                result_code     = "FT-S01";
                result_note     = "Berhasil";
                Log.d("code", result_code);

            } else {
                subscribe.setClickable(true);
                reschedule.setClickable(true);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener subscribe_listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                success.setClickable(false);
                reschedule.setClickable(false);

                result_code     = "FT-S02";
                result_note     = "Berlangganan";
                Log.d("result_code", result_code);

            } else {
                success.setClickable(true);
                reschedule.setClickable(true);
            }
        }
    };

    private CompoundButton.OnCheckedChangeListener reschedule_listener = new CompoundButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (isChecked) {
                success.setClickable(false);
                subscribe.setClickable(false);

                reasonlayout.setVisibility(View.VISIBLE);

            } else {
                success.setClickable(true);
                subscribe.setClickable(true);

                reasonlayout.setVisibility(View.GONE);
                date.setText("");
                time.setText("");
                reason.setSelection(0);
            }
        }
    };

    private View.OnClickListener date_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setDateField(date);
        }
    };

    private View.OnClickListener time_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setTimeField(time);
        }
    };

    private View.OnClickListener btnupload_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TakeChooseImage();
        }
    };

    private void TakeChooseImage() {
        ArrayAdapter<String> mAdapterFilter;
        String[] upload = {"Take picture", "Choose from Gallery"};
        mAdapterFilter  = new ArrayAdapter<String> (getActivity(), R.layout.spinner_textview, upload) {
            public View getView(int position, View convertView, android.view.ViewGroup parent) {
                TextView txtSpinner = (TextView) super.getView(position, convertView, parent);
                txtSpinner.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
                txtSpinner.setTextColor(Color.BLACK);
                txtSpinner.setTextSize(20);
                txtSpinner.setPadding(20, 15, 20, 15);
                return txtSpinner;
            }
        };

        AlertDialog dialog = new AlertDialog.Builder(getActivity())
                .setAdapter(mAdapterFilter, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                captureImage(CAMERA_TAKE_REQUEST_CODE);
                                dialog.dismiss();
                                break;
                            case 1:
                                selectImageFromGallery(GALLERY_CAPTURE_IMAGE_REQUEST_CODE);
                                dialog.dismiss();
                                break;
                        }
                    }
                })
                .create();
        dialog.show();
    }

    private void captureImage(int code) {
        Intent intent   = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri         = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
        startActivityForResult(intent, code);
    }

    public Uri getOutputMediaFileUri(int type) {
        return Uri.fromFile(getOutputMediaFile(type));
    }

    private static File getOutputMediaFile(int type) {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory
                (Environment.DIRECTORY_PICTURES), Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Log.d("cobacoba", "Oops! Failed create " + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        File mediaFile = null;
        File fileAlbum = getAlbumDir();

        if (type == MEDIA_TYPE_IMAGE) {
            try {
                mediaFile = File.createTempFile("P_" + prospect_nbr + "_" + id_retrieval, ".jpg", fileAlbum);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return null;
        }

        return mediaFile;
    }

    private static File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("TS-APPS");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.d("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.v("UploadImage", "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_TAKE_REQUEST_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                getPictImage(requestCode);

            } else if (resultCode == getActivity().RESULT_CANCELED) {
                Toast.makeText(getActivity(), "User cancelled image capture", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
            }

        } else if (requestCode == GALLERY_CAPTURE_IMAGE_REQUEST_CODE) {
            if (resultCode == getActivity().RESULT_OK) {
                getImageGallery(requestCode, data);

            } else if (resultCode == getActivity().RESULT_CANCELED) {
                Toast.makeText(getActivity(), "User cancelled image capture", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Sorry! Failed to capture image", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private void getPictImage(final int code) {
        image_path = fileUri.getPath();

        renameImage();
        decodeFile(code);
    }

    private void getImageGallery(final int code, Intent data) {
        if (data != null) {
            Uri selectedImage       = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            image_path      = cursor.getString(columnIndex);
            cursor.close();

            renameImage();
            decodeFile(code);
        }
    }

    private void selectImageFromGallery(int code) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), code);
    }

    public void renameImage() {
        StringBuilder fileName  = new StringBuilder();
        SimpleDateFormat sdf    = new SimpleDateFormat("yyyy-MM-dd");

        Date now        = new Date();
        String strDate  = sdf.format(now);
        String yy       = strDate.substring(2, 4);
        String mm       = strDate.substring(5, 7);
        String dd       = strDate.substring(8, 10);

        fileName.append(yy);
        fileName.append(mm);
        fileName.append(dd);

        String _filename    = fileName.toString();
        String  rename      = "P_" + prospect_nbr + "_" + id_retrieval + "_" + _filename;

        myFile          = new File(image_path);
        File directory  = new File(String.valueOf(myFile.getParentFile()));
        File from       = new File(directory, myFile.getName().trim());
        File to         = new File(directory, rename + ".jpg");
        from.renameTo(to);
        image_path = to.getAbsolutePath();
    }

    public void decodeFile(final int code) {
        BitmapFactory.Options options   = new BitmapFactory.Options();
        options.inJustDecodeBounds      = true;
        BitmapFactory.decodeFile(image_path, options);
        options.inJustDecodeBounds      = false;

        final int REQUIRED_SIZE     = 1024;
        int width_tmp   = options.outWidth, height_tmp = options.outHeight;
        final int finalWidth_tmp    = width_tmp;
        final int finalHeight_tmp   = height_tmp;
        int scale       = 1;

        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp   /= 2;
            height_tmp  /= 2;
            scale       *= 2;
        }

        options.inSampleSize = scale;

        // width = 2048; height = 1151;
        if (finalWidth_tmp > Config.width && finalHeight_tmp > Config.height) {
            preview.setImageResource(0);

            if (code == CAMERA_TAKE_REQUEST_CODE) {
                Toast.makeText(getActivity(), "Silahkan atur ulang resolusi kamera anda (2M pixel)!.", Toast.LENGTH_SHORT).show();
            } else if (code == GALLERY_CAPTURE_IMAGE_REQUEST_CODE) {
                Toast.makeText(getActivity(), "Image harus 2 MP", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getActivity(), "Silahkan atur ulang resolusi kamera anda (2M pixel)!.", Toast.LENGTH_SHORT).show();
            }

        } else {
            Bitmap bitmap = BitmapFactory.decodeFile(image_path, options);
            preview.setImageBitmap(bitmap);
        }
    }

    private View.OnClickListener send_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TableFormRejoinAdapter dbAdapterFormRejoin;
            List<TableFormRejoin> listFormRejoin;
            dbAdapterFormRejoin = new TableFormRejoinAdapter(getActivity());
            listFormRejoin      = dbAdapterFormRejoin.getDatabyCondition(getActivity(), TableFormRejoin.fFORMID, form_id);

            if (listFormRejoin.size() == 0) {
                Toast.makeText(getActivity(), "Silahkan isi data BAP terlebih dahulu!!", Toast.LENGTH_SHORT).show();
            } else {
                String _form_id = "", _id_retrieval = "", _vc = "", _dsd = "", _lnb = "", _odu = "",
                        _signature_path = "", _latitude = "", _longitude = "", _result_code = "",
                        _result_datetime = "", _result_note = "", _image_path = "";

                for (int i = 0; i < listFormRejoin.size(); i++) {
                    TableFormRejoin item = listFormRejoin.get(0);
                    _form_id            = item.getfFORMID();
                    _id_retrieval       = item.getfID_RETRIEVAL();
                    _vc                 = item.getfVC();
                    _dsd                = item.getfDSD();
                    _lnb                = item.getfLNB();
                    _odu                = item.getfODU();
                    _signature_path     = item.getfSIGNATURE_PATH();
                    _latitude           = item.getfLATITUDE();
                    _longitude          = item.getfLONGITUDE();
                    _result_code        = item.getfRESULT_CODE();
                    _result_datetime    = item.getfRESULT_DATETIME();
                    _result_note        = item.getfRESULT_NOTE();
                    _image_path         = item.getfIMAGE_PATH();
                }

                if (_vc == null || _vc.equals("")) {
                    Toast.makeText(getActivity(), "No VC kosong, Silahkan isi!", Toast.LENGTH_SHORT).show();

                } else if (_dsd == null || _dsd.equals("")) {
                    Toast.makeText(getActivity(), "No DSD kosong, Silahkan isi!", Toast.LENGTH_SHORT).show();

                } else if (_lnb == null || _lnb.equals("")) {
                    Toast.makeText(getActivity(), "No LNB kosong, Silahkan isi!", Toast.LENGTH_SHORT).show();

                } else if (_odu == null || _odu.equals("")) {
                    Toast.makeText(getActivity(), "No ODU kosong, Silahkan isi!", Toast.LENGTH_SHORT).show();

                } else if (_signature_path == null || _signature_path.equals("")) {
                    Toast.makeText(getActivity(), "Silahkan lakukan tanda tangan!", Toast.LENGTH_SHORT).show();

                } else if (!success.isChecked() && !subscribe.isChecked() && !reschedule.isChecked()) {
                    Toast.makeText(getActivity(), "Silahkan Pilih Result!", Toast.LENGTH_SHORT).show();

                } else if (reasonlayout.getVisibility() != View.GONE && date.getText().equals("")) {
                    Toast.makeText(getActivity(), "Tanggal Reschedule harus diisi", Toast.LENGTH_SHORT).show();

                } else if (reasonlayout.getVisibility() != View.GONE && time.getText().equals("")) {
                    Toast.makeText(getActivity(), "Time Reschedule harus diisi", Toast.LENGTH_SHORT).show();

                } else if (image_path == null) {
                    Toast.makeText(getActivity(), "Image kosong, ulangi ambil gambar!", Toast.LENGTH_SHORT).show();

                } else {
                    dbAdapterFormRejoin.updateAllData(getActivity(), _form_id, _id_retrieval, _vc,
                            _dsd, _lnb, _odu, _signature_path, latitude.getText().toString(),
                            longitude.getText().toString(), result_code, date.getText().toString() + " " + time.getText().toString(),
                            String.valueOf(result_note), image_path, TableFormRejoin.fFORMID, form_id);

                    new SendResult().execute();
                }
            }
        }
    };


    /*
    |---------------------------------------------------------------------------------------------------
    | Get Data Task List From Server
    |---------------------------------------------------------------------------------------------------
    */
    private class SendResult extends AsyncTask<String, String, String> {
        private String _status, _message;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            utils   = new Utils(getActivity());
            _signal = utils.checkSignal();
            _imei   = utils.getIMEI();

            utils.showDialogLoading("Sending . . .");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(getActivity());
                listLogLogin        = dbAdapterLogLogin.getAllData(getActivity());

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_SEND_RESULT;
                    _token              = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl    = _url + "/" + prospect_nbr;
                    String value        = PopulateJson().toString();
                    Log.d("value", PopulateJson().toString());

                    String _response    = ConnectionManager.sendResult(_paramurl, _imei, _token, value, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status         = jsonObject.getString("status");
                        _message        = jsonObject.getString("data");
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (utils.progressdialog.isShowing())
                    utils.progressdialog.dismiss();

                if (_status != null && !_status.equalsIgnoreCase("200")) {
                    uploadImage(image_path, getFileName(image_path));
                    utils.showErrorDialog("Alert!!", _message);

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
    }

    private String getFileName(String path) {
        String filename = path.substring(path.lastIndexOf("/") + 1);
        return filename;
    }

    private void uploadImage(final String image_path, final String image_name) {
        try {
            String url = ConnectionManager.CM_URL_UPLOAD_IMAGE + "/" + prospect_nbr;
            System.out.println(url);

            final File myFile       = new File(image_path);
            RequestParams params    = new RequestParams();
            params.put("image", myFile);

            AsyncHttpClient client  = new AsyncHttpClient();
            client.setTimeout(300000);
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);

                    notifManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
                    notifBuilder = new NotificationCompat.Builder(getActivity());
                    notifBuilder.setContentTitle("Result Upload")
                            .setContentText("Upload in progress")
                            .setSmallIcon(R.drawable.stat_sys_upload);

                    int writen              = (int) bytesWritten;
                    float proportionCorrect = ((float) writen) / ((float) myFile.length());
                    float percentFl         = proportionCorrect * 100;
                    String percent          = String.format("%.0f", percentFl);

                    Log.i("uploadimage", "percent "+percent);
                    if (Integer.valueOf(percent) > Integer.valueOf(percentage.getText().toString().replace(" %", ""))) {
                        Log.i("uploadimage", "if "+percent);

                        percentage.setText(percent + " %");
                        progressbar.setProgress(Integer.valueOf(percent));
                    }

                    if (Integer.valueOf(percent) > 100) {
                        Log.i("uploadimage", "if 100 "+percent);
                        percentage.setText("100 %");
                        progressbar.setProgress(100);
                    }

                    notifBuilder.setProgress(100, Integer.parseInt(percent), false);
                    notifManager.notify(1, notifBuilder.build());
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] bytes) {
                    notifBuilder.setContentText("Upload complete")
                            .setProgress(0, 0, false)
                            .setSmallIcon(R.mipmap.stat_sys_upload_anim25);
                    notifManager.notify(1, notifBuilder.build());

                    Toast.makeText(getContext(), "Success upload " + image_name, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(getActivity(), Rejoin.class);
                    startActivity(intent);
                    getActivity().finish();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] bytes, Throwable throwable) {
                    Log.i("uploadimage", "onFailure "+String.valueOf(myFile));
                    percentage.setText(image_name + " 0 %");
                    progressbar.setProgress(0);

                    String responseString = "Error occurred! Http Status Code: " + statusCode + " " + image_name;
                    Toast.makeText(getContext(), responseString, Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String PopulateJson() {
        TableFormRejoinAdapter dbAdapterFormRejoin;
        List<TableFormRejoin> listFormRejoin;
        dbAdapterFormRejoin = new TableFormRejoinAdapter(getActivity());
        listFormRejoin      = dbAdapterFormRejoin.getDatabyCondition(getActivity(), TableFormRejoin.fFORMID, form_id);

        String _form_id = "", _id_retrieval = "", _vc = "", _dsd = "", _lnb = "", _odu = "",
                _signature_path = "", _latitude = "", _longitude = "", _result_code = "",
                _result_datetime = "", _result_note = "", _image_path = "";

        for (int i = 0; i < listFormRejoin.size(); i++) {
            TableFormRejoin item = listFormRejoin.get(0);
            _form_id            = item.getfFORMID();
            _id_retrieval       = item.getfID_RETRIEVAL();
            _vc                 = item.getfVC();
            _dsd                = item.getfDSD();
            _lnb                = item.getfLNB();
            _odu                = item.getfODU();
            _signature_path     = item.getfSIGNATURE_PATH();
            _latitude           = item.getfLATITUDE();
            _longitude          = item.getfLONGITUDE();
            _result_code        = item.getfRESULT_CODE();
            _result_datetime    = item.getfRESULT_DATETIME();
            _result_note        = item.getfRESULT_NOTE();
            _image_path         = item.getfIMAGE_PATH();
        }

        _imei = ""; _token = ""; _signal = 0;

        dbAdapterLogLogin   = new TableLogLoginAdapter(getActivity());
        listLogLogin        = dbAdapterLogLogin.getAllData(getActivity());
        if (listLogLogin.size() > 0) {
            _token = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
        }

        utils   = new Utils(getActivity());
        _imei   = utils.getIMEI();
        _signal = utils.checkSignal();
        myFile  = new File(image_path);

        JSONObject object       = new JSONObject();
        JSONObject objData      = new JSONObject();
        JSONObject objResult    = new JSONObject();

        try {
            object.put("result", objResult);
            object.put("imei", _imei);
            object.put("token", _token);
            object.put("version", Config.version);
            object.put("signal", String.valueOf(_signal));
            objData.put("data", object);

            objResult.put("customer_nbr", customer_nbr);
            objResult.put("ticket_nbr", ticket_nbr);
            objResult.put("result_code", _result_code);
            objResult.put("result_notes", _result_note);
            objResult.put("no_aplikasi", prospect_nbr);
            objResult.put("latitude", _latitude);
            objResult.put("longitude", _longitude);
            objResult.put("imagekunjungan", myFile.getName());
            objResult.put("resdate", _result_datetime);
            objResult.put("imagesignature", _signature_path);
            objResult.put("vc", _vc);
            objResult.put("dsd", _dsd);
            objResult.put("lnb", _lnb);
            objResult.put("disc", _odu);
            objResult.put("id_retrieval", _id_retrieval);
            objResult.put("username", username);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return objData.toString();
    }
}
