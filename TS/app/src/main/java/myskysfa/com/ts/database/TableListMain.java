package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/17/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "listmain")
public class TableListMain {
    public static final String TABLE_NAME       = "listmain";

    public static final String fNEW_ID          = "new_id";
    public static final String fTICKET_NUMBER   = "ticket_number";
    public static final String fCUSTOMER_ID     = "customer_id";
    public static final String fCUSTOMER_NBR    = "customer_nbr";
    public static final String fCUSTOMER_NAME   = "customer_name";
    public static final String fASSIGN_TO       = "assign_to";
    public static final String fNATURE_CODE     = "nature_code";
    public static final String fNATURE_DESCR    = "nature_descr";
    public static final String fNATURE_CATEGORY = "nature_category";
    public static final String fBRAND           = "brand";
    public static final String fNOTES           = "notes";
    public static final String fSCHEDULE_DATE   = "schedule_date";
    public static final String fSTATUS_TICKET   = "status_ticket";

    @DatabaseField(generatedId = true)
    private int new_id;

    @DatabaseField
    private String ticket_number, customer_id, customer_nbr, customer_name, assign_to, nature_code,
            nature_descr, nature_category, brand, notes, schedule_date, status_ticket;

    public void setfNEW_ID(int new_id) {
        this.new_id = new_id;
    }
    public int getfNEW_ID() {
        return new_id;
    }

    public void setfTICKET_NUMBER(String ticket_number) {
        this.ticket_number = ticket_number;
    }
    public String getfTICKET_NUMBER() {
        return ticket_number;
    }

    public void setfCUSTOMER_ID(String customer_id) {
        this.customer_id = customer_id;
    }
    public String getfCUSTOMER_ID() {
        return customer_id;
    }

    public void setfCUSTOMER_NBR(String customer_nbr) {
        this.customer_nbr = customer_nbr;
    }
    public String getfCUSTOMER_NBR() {
        return customer_nbr;
    }

    public void setfCUSTOMER_NAME(String customer_name) {
        this.customer_name = customer_name;
    }
    public String getfCUSTOMER_NAME() {
        return customer_name;
    }

    public void setfASSIGN_TO(String assign_to) {
        this.assign_to = assign_to;
    }
    public String getfASSIGN_TO() {
        return assign_to;
    }

    public void setfNATURE_CODE(String nature_code) {
        this.nature_code = nature_code;
    }
    public String getfNATURE_CODE() {
        return nature_code;
    }

    public void setfNATURE_DESCR(String nature_descr) {
        this.nature_descr = nature_descr;
    }
    public String getfNATURE_DESCR() {
        return nature_descr;
    }

    public void setfNATURE_CATEGORY(String nature_category) {
        this.nature_category = nature_category;
    }
    public String getfNATURE_CATEGORY() {
        return nature_category;
    }

    public void setfBRAND(String brand) {
        this.brand = brand;
    }
    public String getfBRAND() {
        return brand;
    }

    public void setfNOTES(String notes) {
        this.notes = notes;
    }
    public String getfNOTES() {
        return notes;
    }

    public void setfSCHEDULE_DATE(String schedule_date) {
        this.schedule_date = schedule_date;
    }
    public String getfSCHEDULE_DATE() {
        return schedule_date;
    }

    public void setfSTATUS_TICKET(String status_ticket) {
        this.status_ticket = status_ticket;
    }
    public String getfSTATUS_TICKET() {
        return status_ticket;
    }
}