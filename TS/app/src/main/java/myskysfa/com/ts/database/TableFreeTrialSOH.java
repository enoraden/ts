package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/4/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "free_trial_soh")
public class TableFreeTrialSOH {
    public static final String TABLE_NAME       = "free_trial_soh";

    public static final String fITEM_ID         = "item_id";
    public static final String fITEM_CODE       = "item_code";
    public static final String fITEM_DESCR      = "item_descr";
    public static final String fSERIAL_NO       = "serial_no";
    public static final String fEMPLOYEE_ID     = "employee_id";
    public static final String fQTY             = "qty";

    @DatabaseField(id = true)
    private String serial_no;

    @DatabaseField
    private String item_id, item_code, item_descr, employee_id, qty;

    public void setfITEM_ID(String item_id) {
        this.item_id = item_id;
    }
    public String getfITEM_ID() {
        return item_id;
    }

    public void setfITEM_CODE(String item_code) {
        this.serial_no = item_code;
    }
    public String getfITEM_CODE() {
        return item_code;
    }

    public void setfITEM_DESCR(String item_descr) {
        this.item_descr = item_descr;
    }
    public String getfITEM_DESCR() {
        return item_descr;
    }

    public void setfSERIAL_NO(String serial_no) {
        this.serial_no = serial_no;
    }
    public String getfSERIAL_NO() {
        return serial_no;
    }

    public void setfEMPLOYEE_ID(String employee_id) {
        this.employee_id = employee_id;
    }
    public String getfEMPLOYEE_ID() {
        return employee_id;
    }

    public void setfQTY(String qty) {
        this.qty = qty;
    }
    public String getfQTY() {
        return qty;
    }
}