package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "log_login")
public class TableLogLogin {
    public static final String TABLE_NAME       = "log_login";

    public static final String fNEW_ID          = "new_id";
    public static final String fUSER_ID         = "user_id";
    public static final String fUSER_NAME       = "user_name";
    public static final String fUSER_TYPE       = "user_type";
    public static final String fEMPLOYEE_ID     = "employee_id";
    public static final String fFULL_NAME       = "full_name";
    public static final String fROLE_CODE       = "role_code";
    public static final String fROLE_NAME       = "role_name";
    public static final String fSFL_CODE        = "sfl_code";
    public static final String fBRAND           = "brand";
    public static final String fBRANCH_ID       = "branch_id";
    public static final String fBRANCH          = "branch";
    public static final String fREGION_CODE     = "region_code";
    public static final String fREGION_NAME     = "region_name";
    public static final String fIS_SUPERVISOR   = "is_supervisor";
    public static final String fACCESS_TOKEN    = "access_token";

    @DatabaseField(generatedId = true)
    private int new_id;

    @DatabaseField
    private String user_id, user_name, user_type, employee_id, full_name, role_code, role_name, sfl_code,
            brand, branch_id, branch, region_code, region_name, is_supervisor, access_token;

    public void setfNEW_ID(int new_id) {
        this.new_id = new_id;
    }
    public int getfNEW_ID() {
        return new_id;
    }

    public void setfUSER_ID(String user_id) {
        this.user_id = user_id;
    }
    public String getfUSER_ID() {
        return user_id;
    }

    public void setfUSER_NAME(String user_name) {
        this.user_name = user_name;
    }
    public String getfUSER_NAME() {
        return user_name;
    }

    public void setfUSER_TYPE(String user_type) {
        this.user_type = user_type;
    }
    public String getfUSER_TYPE() {
        return user_type;
    }

    public void setfEMPLOYEE_ID(String employee_id) {
        this.employee_id = employee_id;
    }
    public String getfEMPLOYEE_ID() {
        return employee_id;
    }

    public void setfFULL_NAME(String full_name) {
        this.full_name = full_name;
    }
    public String getfFULL_NAME() {
        return full_name;
    }

    public void setfROLE_CODE(String role_code) {
        this.role_code = role_code;
    }
    public String getfROLE_CODE() {
        return role_code;
    }

    public void setfROLE_NAME(String role_name) {
        this.role_name = role_name;
    }
    public String getfROLE_NAME() {
        return role_name;
    }

    public void setfSFL_CODE(String sfl_code) {
        this.sfl_code = sfl_code;
    }
    public String getfSFL_CODE() {
        return sfl_code;
    }

    public void setfBRAND(String brand) {
        this.brand = brand;
    }
    public String getfBRAND() {
        return brand;
    }

    public void setfBRANCH_ID(String branch_id) {
        this.branch_id = branch_id;
    }
    public String getfBRANCH_ID() {
        return branch_id;
    }

    public void setfBRANCH(String branch) {
        this.branch = branch;
    }
    public String getfBRANCH() {
        return branch;
    }

    public void setfREGION_CODE(String region_code) {
        this.region_code = region_code;
    }
    public String getfREGION_CODE() {
        return region_code;
    }

    public void setfREGION_NAME(String region_name) {
        this.region_name = region_name;
    }
    public String getfREGION_NAME() {
        return region_name;
    }

    public void setfIS_SUPERVISOR(String is_supervisor) {
        this.is_supervisor = is_supervisor;
    }
    public String getfIS_SUPERVISOR() {
        return is_supervisor;
    }

    public void setfACCESS_TOKEN(String access_token) {
        this.access_token = access_token;
    }
    public String getfACCESS_TOKEN() {
        return access_token;
    }
}