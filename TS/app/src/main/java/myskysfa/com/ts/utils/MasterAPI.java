package myskysfa.com.ts.utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterAPI {
    public String inst_troub(){
        String api =
                "{\n" +
                "  \"status\": \"true\",\n" +
                "  \"data\": [\n" +
                "    {\n" +
                "      \"type_data\": \"INSTALASI\",\n" +
                "      \"no_ticket\": \"F777771601131700\",\n" +
                "      \"schedule_date\": \"20-10-2016\",\n" +
                "      \"cust_name\": \"Sambudi Raharjo\",\n" +
                "      \"identitas_id\": \"0000012345678906\",\n" +
                "      \"area\": \"Menteng Atas, Setia Budi\",\n" +
                "      \"zip_code\": \"12960\",\n" +
                "      \"nature\": \"01\",\n" +
                "      \"paket\": \"Indovision\",\n" +
                "      \"status\": \"Resolve\",\n" +
                "      \"target_date\": \"11-09-2016\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"type_data\": \"INSTALASI\",\n" +
                "      \"no_ticket\": \"F602921601131700\",\n" +
                "      \"schedule_date\": \"20-10-2016\",\n" +
                "      \"cust_name\": \"Diana Raharjo\",\n" +
                "      \"identitas_id\": \"0000012345678906\",\n" +
                "      \"area\": \"Bukit Duri, Jakarta\",\n" +
                "      \"zip_code\": \"12110\",\n" +
                "      \"nature\": \"02\",\n" +
                "      \"paket\": \"Top TV\",\n" +
                "      \"status\": \"New\",\n" +
                "      \"target_date\": \"20-10-2016\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"type_data\": \"INSTALASI\",\n" +
                "      \"no_ticket\": \"F602921601131700\",\n" +
                "      \"schedule_date\": \"20-10-2016\",\n" +
                "      \"cust_name\": \"Nisa\",\n" +
                "      \"identitas_id\": \"22342111890678906\",\n" +
                "      \"area\": \"Bukit Duri, Jakarta\",\n" +
                "      \"zip_code\": \"12110\",\n" +
                "      \"nature\": \"02\",\n" +
                "      \"paket\": \"Top TV\",\n" +
                "      \"status\": \"PA\",\n" +
                "      \"target_date\": \"20-10-2016\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"type_data\": \"TROUBLESHOOT\",\n" +
                "      \"no_ticket\": \"F123451601131700\",\n" +
                "      \"schedule_date\": \"20-10-2016\",\n" +
                "      \"cust_name\": \"Sambudi Raharjo\",\n" +
                "      \"identitas_id\": \"0000012345678906\",\n" +
                "      \"area\": \"Menteng Atas, Setia Budi\",\n" +
                "      \"zip_code\": \"12960\",\n" +
                "      \"nature\": \"01\",\n" +
                "      \"paket\": \"Indovision\",\n" +
                "      \"status\": \"Resolve\",\n" +
                "      \"target_date\": \"20-10-2016\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"type_data\": \"TROUBLESHOOT\",\n" +
                "      \"no_ticket\": \"F567357601131700\",\n" +
                "      \"schedule_date\": \"20-10-2016\",\n" +
                "      \"cust_name\": \"Sari Roti\",\n" +
                "      \"identitas_id\": \"0000012345678906\",\n" +
                "      \"area\": \"Kedoya, Jakarta Barat\",\n" +
                "      \"zip_code\": \"12960\",\n" +
                "      \"nature\": \"01\",\n" +
                "      \"paket\": \"Indovision\",\n" +
                "      \"status\": \"New\",\n" +
                "      \"target_date\": \"20-10-2016\"\n" +
                "    },\n" +
                "    {\n" +
                "      \"type_data\": \"TROUBLESHOOT\",\n" +
                "      \"no_ticket\": \"F602921601131700\",\n" +
                "      \"schedule_date\": \"20-10-2016\",\n" +
                "      \"cust_name\": \"Aladin Sukaroti\",\n" +
                "      \"identitas_id\": \"0000012345678906\",\n" +
                "      \"area\": \"Menteng Atas, Setia Budi\",\n" +
                "      \"zip_code\": \"12960\",\n" +
                "      \"nature\": \"01\",\n" +
                "      \"paket\": \"Indovision\",\n" +
                "      \"status\": \"PA\",\n" +
                "      \"target_date\": \"20-10-2016\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";

        return api;
    }

    public String detail_data_profile() {
        String api =
                "{\n" +
                "  \"status\": \"true\",\n" +
                "  \"data\": {\n" +
                "    \"profile_customer\": {\n" +
                "      \"no_form\": \"F777771601131700\",\n" +
                "      \"tgl_daftar\": \"20-10-2016\",\n" +
                "      \"kartu_identitas\": \"KTP\",\n" +
                "      \"no_identitas\": \"0000012345678906\",\n" +
                "      \"nama_depan\": \"Sambudi\",\n" +
                "      \"nama_tengah\": \"Raharjo\",\n" +
                "      \"nama_belakang\": \"-\",\n" +
                "      \"jenis_kelamin\": \"Laki-Laki\",\n" +
                "      \"agama\": \"Islam\",\n" +
                "      \"tempat_lahir\": \"Jakarta\",\n" +
                "      \"tgl_lahir\": \"10-12-1998\"\n" +
                "    },\n" +
                "    \"alamat_kartu_idt\": {\n" +
                "      \"idt_alamat\": \"Jl. Bangau\",\n" +
                "      \"idt_normh\": \"12\",\n" +
                "      \"idt_RT\": \"01\",\n" +
                "      \"idt_RW\": \"14\",\n" +
                "      \"idt_provinsi\": \"Jakarta\",\n" +
                "      \"idt_kota\": \"DKI Jakarta\",\n" +
                "      \"idt_kecamatan\": \"Kedoya Barat\",\n" +
                "      \"idt_kelurahan\": \"Kedoya\",\n" +
                "      \"idt_kodepos\": \"15211\",\n" +
                "      \"idt_masaberlaku\": \"15-10-2020\",\n" +
                "      \"idt_tglpasang\": \"12-02-2016\",\n" +
                "      \"idt_jampasang\": \"16:00\",\n" +
                "      \"idt_tglkonfirm_start\": \"13-02-2016\",\n" +
                "      \"idt_tglkonfirm_end\": \"22-05-2016\",\n" +
                "      \"idt_jamkonfirm_start\": \"17:00\",\n" +
                "      \"idt_jamkonfirm_end\": \"19:00\"\n" +
                "    },\n" +
                "    \"alamat_pemasangan\": {\n" +
                "      \"inst_checkalamatidt\": \"N\",\n" +
                "      \"inst_alamat\": \"Jl. Jambu Mangga\",\n" +
                "      \"inst_patokan\": \"Kebon Jeruk\",\n" +
                "      \"inst_provinsi\": \"Jakarta\",\n" +
                "      \"inst_kota\": \"Pepaya\",\n" +
                "      \"inst_kecamatan\": \"Apel\",\n" +
                "      \"inst_kelurahan\": \"Anggur\",\n" +
                "      \"inst_kodepos\": \"14223\"\n" +
                "    },\n" +
                "    \"alamat_penagihan\": {\n" +
                "      \"bill_checkalamatidt\": \"N\",\n" +
                "      \"bill_checkalamatinst\": \"N\",\n" +
                "      \"bill_alamat\": \"Jl. Jambu Apel\",\n" +
                "      \"bill_provinsi\": \"Jawa\",\n" +
                "      \"bill_kota\": \"Melon\",\n" +
                "      \"bill_kecamatan\": \"Lemon\",\n" +
                "      \"bill_kelurahan\": \"Semangka\",\n" +
                "      \"bill_kodepos\": \"44532\",\n" +
                "      \"bill_tlprmh\": \"0217654832\",\n" +
                "      \"bill_nohp\": \"0856123456789\",\n" +
                "      \"bill_email\": \"jambu@indovision.tv\",\n" +
                "      \"bill_jenisrmh\": \"-\",\n" +
                "      \"bill_statusrmh\": \"Sewa\",\n" +
                "      \"bill_pekerjaan\": \"-\",\n" +
                "      \"bill_penghasilan\": \"1000000\"\n" +
                "    }\n" +
                "  }\n" +
                "}";

        return api;
    }

    public String detail_data_emergency() {
        String api =
                "{\n" +
                        "  \"status\": \"true\",\n" +
                        "  \"data\": {\n" +
                        "    \"no_form\": \"F777771601131700\",\n" +
                        "    \"nama_depan\": \"Fajar\",\n" +
                        "    \"nama_tengah\": \"Anto\",\n" +
                        "    \"nama_belakang\": \"Hasbun\",\n" +
                        "    \"hubungan\": \"Saudara Kandung\",\n" +
                        "    \"notlp\": \"08124567890\",\n" +
                        "    \"alamat\": \"Jl. Bangau Gajah Duduk\"\n" +
                        "  }\n" +
                        "}";

        return api;
    }

    public String detail_data_payment() {
        String api =
                "{\n" +
                "  \"status\": \"true\",\n" +
                "  \"data\": {\n" +
                "    \"no_form\": \"F777771601131700\",\n" +
                "    \"jumlahbayar\": \"1000000\",\n" +
                "    \"carabayar\": \"Cash\",\n" +
                "    \"norekening\": \"091233123402\",\n" +
                "    \"nama\": \"Sambudi\",\n" +
                "    \"namabank\": \"MNC Bank\",\n" +
                "    \"masaberlaku\": \"02-05-2017\",\n" +
                "    \"periodebayar\": \"20-11-2017\",\n" +
                "    \"remark\": \"-\"\n" +
                "  }\n" +
                "}";

        return api;
    }

    public String detail_data_package() {
        String api =
                "{\n" +
                "  \"status\": \"true\",\n" +
                "  \"data\": {\n" +
                "    \"no_form\": \"F777771601131700\",\n" +
                "    \"brand\": \"INDOVISION\",\n" +
                "    \"type\": \"Single\",\n" +
                "    \"promo\": \"IDVGIANT0115. PROMO GIANT. BELI TV 32. MENDAPATKAN PROMO REGULER DAN FREE 2 ALACARTE SPORTS DAN ICONCERT SELAMA 1 BULAN DIBULAN KE-2.\",\n" +
                "    \"data_package\": [\n" +
                "      {\n" +
                "        \"name_basic\": \"SUPER GALAXY 2014\",\n" +
                "        \"data_alacarte\": [\n" +
                "          {\n" +
                "            \"id_alacarte\": \"1\",\n" +
                "            \"name_alacarte\": \"SUPER GALAXY 2014\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id_alacarte\": \"3\",\n" +
                "            \"name_alacarte\": \"VENUS 2014\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id_alacarte\": \"4\",\n" +
                "            \"name_alacarte\": \"MARS 2014\"\n" +
                "          }\n" +
                "        ]\n" +
                "      },\n" +
                "      {\n" +
                "        \"name_basic\": \"VENUS 2014\",\n" +
                "        \"data_alacarte\": []\n" +
                "      },\n" +
                "      {\n" +
                "        \"name_basic\": \"CINEMA1\",\n" +
                "        \"data_alacarte\": [\n" +
                "          {\n" +
                "            \"id_alacarte\": \"7\",\n" +
                "            \"name_alacarte\": \"CINEMA3\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id_alacarte\": \"8\",\n" +
                "            \"name_alacarte\": \"SPORT\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id_alacarte\": \"12\",\n" +
                "            \"name_alacarte\": \"TOPTV 2014\"\n" +
                "          },\n" +
                "          {\n" +
                "            \"id_alacarte\": \"17\",\n" +
                "            \"name_alacarte\": \"STUDIO1\"\n" +
                "          }\n" +
                "        ]\n" +
                "      }\n" +
                "    ]\n" +
                "  }\n" +
                "}";

        return api;
    }

    //API Rejoin
    public  String rejoin = "{\"status\":\"true\",\"data\":[{\"id_retrieval\":\"1111\",\"schedule_penarikan\":\"2016-02-14\",\"no_aplikasi\":\"P4563567840041537\",\"status_penarikan\":\"0\",\"created_date\":\"2016-02-14\",\"username\":\"myusuf\",\"customer_nbr\":\"cobacobacoba\",\"ticket_nbr\":\"122344\",\"job_type\":\"123\"},{\"id_retrieval\":\"2222\",\"schedule_penarikan\":\"2016-02-14\",\"no_aplikasi\":\"P6029216110055674\",\"status_penarikan\":\"1\",\"created_date\":\"2016-02-14\",\"username\":\"mainurr\",\"customer_nbr\":\"cobacobacoba\",\"ticket_nbr\":\"122344\",\"job_type\":\"123\"},{\"id_retrieval\":\"3333\",\"schedule_penarikan\":\"2016-02-14\",\"no_aplikasi\":\"P6029216110022332\",\"status_penarikan\":\"2\",\"created_date\":\"2016-02-14\",\"username\":\"akono\",\"customer_nbr\":\"cobacobacoba\",\"ticket_nbr\":\"122344\",\"job_type\":\"123\"},{\"id_retrieval\":\"4444\",\"schedule_penarikan\":\"2016-02-14\",\"no_aplikasi\":\"P1122311110041537\",\"status_penarikan\":\"3\",\"created_date\":\"2016-02-14\",\"username\":\"myusuf\",\"customer_nbr\":\"cobacobacoba\",\"ticket_nbr\":\"122344\",\"job_type\":\"123\"}]}";


}
