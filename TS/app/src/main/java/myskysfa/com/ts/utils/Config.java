package myskysfa.com.ts.utils;

import com.google.android.gms.maps.model.LatLng;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Config {
    public static final String version = "2.2";

    public static final String URL_HTTP_STRING = "http://";

    /*
   |-----------------------------------------------------------------------------------------------
   | Url Google Map Direction
   |-----------------------------------------------------------------------------------------------
   */
    public static String API_KEY = "AIzaSyB2angf8XDqYm1bPk41-w4X1pl98f6MHoU";

    /*
   |-----------------------------------------------------------------------------------------------
   | KEY
   |-----------------------------------------------------------------------------------------------
   */
    public static final String IMAGE_DIRECTORY_NAME = "Android File Upload";

    /*
    |-----------------------------------------------------------------------------------------------
    | URL server local for this application to transfer data.
    |-----------------------------------------------------------------------------------------------
    */
    //public static final String url_Server_local = "192.168.177.89";
    public static final String url_Server_local = "192.168.177.221";

    public static String makeUrlLocalString(String uri) {
        StringBuilder url = new StringBuilder(URL_HTTP_STRING);
        url.append(url_Server_local);
        url.append("/");
        url.append(uri);
        return url.toString();
    }

    public static String urlMapDirection(LatLng start, LatLng end, String mode) {
        String url = "http://maps.googleapis.com/maps/api/directions/xml?"
                + "origin=" + start.latitude + "," + start.longitude
                + "&destination=" + end.latitude + "," + end.longitude
                + "&sensor=true"
                + "&units=metric"
                + "&mode=" + mode
                + "key=" + Config.API_KEY;

        return url;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Set Camera Resolution
    |-----------------------------------------------------------------------------------------------
    */
    public static final int width = 2048;
    public static final int height = 1151;
}