package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import myskysfa.com.ts.database.TableFreeTrial;
import myskysfa.com.ts.utils.DatabaseManager;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/7/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableFreeTrialAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableFreeTrialAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableFreeTrial> getDatabyCondition(Context context, String condition, Object param) {
        List<TableFreeTrial> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrial.class);

            QueryBuilder<TableFreeTrial, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFreeTrial, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableFreeTrial> getAllData(Context context) {
        List<TableFreeTrial> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrial.class);

            QueryBuilder<TableFreeTrial, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableFreeTrial table, String prospect_nbr, String sfl_code, String register_date,
                           String is_multi, String prospect_name, String mobile_phone, String brand_code,
                           String created_date, String status, String user_name, String plan_id,
                           String alamat, String product_name, String product_id, String value) {
        try {
            table.setfPROSPECT_NBR(prospect_nbr);
            table.setfSFL_CODE(sfl_code);
            table.setfREGISTER_DATE(register_date);
            table.setfIS_MULTI(is_multi);
            table.setfPROSPECT_NAME(prospect_name);
            table.setfMOBILE_PHONE(mobile_phone);
            table.setfBRAND_CODE(brand_code);
            table.setfCREATED_DATE(created_date);
            table.setfSTATUS(status);
            table.setfUSER_NAME(user_name);
            table.setfPLAN_ID(plan_id);
            table.setfALAMAT(alamat);
            table.setfPRODUCT_NAME(product_name);
            table.setfPRODUCT_ID(product_id);
            table.setfVALUE(value);
            getHelper().getTableFreeTrialDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrial.class);

            UpdateBuilder<TableFreeTrial, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String prospect_nbr, String sfl_code, String register_date,
                              String is_multi, String prospect_name, String mobile_phone, String brand_code,
                              String created_date, String status, String user_name, String plan_id,
                              String alamat, String product_name, String product_id, String value,
                              String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrial.class);

            UpdateBuilder<TableFreeTrial, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFreeTrial.fPROSPECT_NBR, prospect_nbr);
            updateBuilder.updateColumnValue(TableFreeTrial.fSFL_CODE, sfl_code);
            updateBuilder.updateColumnValue(TableFreeTrial.fREGISTER_DATE, register_date);
            updateBuilder.updateColumnValue(TableFreeTrial.fIS_MULTI, is_multi);
            updateBuilder.updateColumnValue(TableFreeTrial.fPROSPECT_NAME, prospect_name);
            updateBuilder.updateColumnValue(TableFreeTrial.fMOBILE_PHONE, mobile_phone);
            updateBuilder.updateColumnValue(TableFreeTrial.fBRAND_CODE, brand_code);
            updateBuilder.updateColumnValue(TableFreeTrial.fCREATED_DATE, created_date);
            updateBuilder.updateColumnValue(TableFreeTrial.fSTATUS, status);
            updateBuilder.updateColumnValue(TableFreeTrial.fUSER_NAME, user_name);
            updateBuilder.updateColumnValue(TableFreeTrial.fPLAN_ID, plan_id);
            updateBuilder.updateColumnValue(TableFreeTrial.fALAMAT, alamat);
            updateBuilder.updateColumnValue(TableFreeTrial.fPRODUCT_NAME, product_name);
            updateBuilder.updateColumnValue(TableFreeTrial.fPRODUCT_ID, product_id);
            updateBuilder.updateColumnValue(TableFreeTrial.fVALUE, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFreeTrial.class);

            DeleteBuilder<TableFreeTrial, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableFreeTrial> listTable = null;
        try {
            listTable = getHelper().getTableFreeTrialDAO().queryForAll();
            getHelper().getTableFreeTrialDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}