package myskysfa.com.ts.main;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import myskysfa.com.ts.R;
import myskysfa.com.ts.utils.DatabaseManager;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
public class SplashActivity extends AppCompatActivity {
    private Utils utils;
    private int _signal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);
        new DatabaseManager(this);

        loadResource();
        displaySplashScreen();
    }

    private void loadResource() {
        utils   = new Utils(SplashActivity.this);
        _signal = utils.checkSignal();

        Log.i("TSapps", "signal = "+_signal);
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Method for display Splash screen
    |-----------------------------------------------------------------------------------------------
    */
    private void displaySplashScreen() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(i);
                finish();

                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
            }
        }, 3000);
    }
}
