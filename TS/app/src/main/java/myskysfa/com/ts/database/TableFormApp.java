package myskysfa.com.ts.database;

/**
 * Created by Administrator on 11/22/2016.
 */

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "form_app")
public class TableFormApp {

    public static final String TABLE_NAME = "form_app";

    public static final String fNEW_FORMID    = "new_formid";
    public static final String fNO_TICKET = "no_ticket";
//    public static final String fUSER_ID         = "user_id";
//    public static final String fUSER_NAME       = "user_name";
//    public static final String fUSER_TYPE       = "user_type";
//    public static final String fEMPLOYEE_ID     = "employee_id";
    public static final String fVALUES_PACKAGE = "VALUES_PACKAGE";
    public static final String fVALUES_PAYMENT = "VALUES_PAYMENT";
    public static final String fVALUES_LKT = "VALUES_LKT";
    public static final String fVALUES_CHECKLIST = "VALUES_CHECKLIST";
    public static final String fVALUES_SIGNATURE = "VALUES_SIGNATURE";
    public static final String fFORM_STATUS = "form_status";


    @DatabaseField(generatedId = true)
    private int new_formid;

    @DatabaseField
    private String  NO_TICKET,VALUES_PACKAGE, VALUES_PAYMENT, VALUES_LKT , VALUES_CHECKLIST,
            VALUES_SIGNATURE, FORM_STATUS;


    public void setNEW_FORMID(int new_formid) {
        this.new_formid = new_formid;
    }

    public int getNEW_FORMID() {
        return new_formid;
    }

    public void setNO_TICKET(String NO_TICKET) {
        this.NO_TICKET = NO_TICKET;
    }

    public String getNO_TICKET() {
        return NO_TICKET;
    }

    public void setFORM_STATUS(String FORM_STATUS) {
        this.FORM_STATUS = FORM_STATUS;
    }

    public String getFORM_STATUS() {
        return FORM_STATUS;
    }

    public void setVALUES_PACKAGE(String VALUES_PACKAGE) {
        this.VALUES_PACKAGE = VALUES_PACKAGE;
    }

    public String getVALUES_PACKAGE() {
        return VALUES_PACKAGE;
    }

    public void setVALUES_PAYMENT(String VALUES_PAYMENT) {
        this.VALUES_PAYMENT = VALUES_PAYMENT;
    }

    public String getVALUES_PAYMENT() {
        return VALUES_PAYMENT;
    }

    public void setVALUES_LKT(String VALUES_LKT) {
        this.VALUES_LKT = VALUES_LKT;
    }

    public String getVALUES_LKT() {
        return VALUES_LKT;
    }

    public void setVALUES_CHECKLIST(String VALUES_CHECKLIST) {
        this.VALUES_CHECKLIST = VALUES_CHECKLIST;
    }

    public String getVALUES_CHECKLIST() {
        return VALUES_CHECKLIST;
    }


    public void setVALUES_SIGNATURE(String VALUES_SIGNATURE) {
        this.VALUES_SIGNATURE = VALUES_SIGNATURE;
    }

    public String getVALUES_SIGNATURE() {
        return VALUES_SIGNATURE;
    }


}
