package myskysfa.com.ts.utils;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.SQLException;

import myskysfa.com.ts.database.TableFormApp;
import myskysfa.com.ts.database.TableFormChildView;
import myskysfa.com.ts.database.TableFormRejoin;
import myskysfa.com.ts.database.TableFreeTrial;
import myskysfa.com.ts.database.TableFreeTrialSOH;
import myskysfa.com.ts.database.TableListMain;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.TableMainRejoin;
import myskysfa.com.ts.database.TableMasterMaterial;
import myskysfa.com.ts.database.TableMasterPackage;
import myskysfa.com.ts.database.TableMasterPromo;
import myskysfa.com.ts.database.TablePlan;
import myskysfa.com.ts.database.TableProfileAddress;
import myskysfa.com.ts.database.TableTaskList;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
public class DatabaseManager  extends OrmLiteSqliteOpenHelper {

    private static final String DATABASE_NAME   = "indovision_ts";
    private static final String DATABASE_PATH   = "/data/data/myskysfa.com.ts/databases/";
    public static final int DATABASE_VERSION    = 1;

    /*
    |-----------------------------------------------------------------------------------------------
    | The DAO object we use to access the SimpleData table
    |-----------------------------------------------------------------------------------------------
    */
    private Dao<TableLogLogin, Integer> TableLogLoginDAO                = null;
    private Dao<TableListMain, Integer> TableInstalasiDAO               = null;
    private Dao<TableFreeTrialSOH, Integer> TableFreeTrialSOHDAO        = null;
    private Dao<TableMasterPackage, Integer> TableMasterPackageDAO      = null;
    private Dao<TableMasterPromo, Integer> TableMasterPromoDAO          = null;
    private Dao<TableMasterMaterial, Integer> TableMasterMaterialDAO    = null;
    private Dao<TablePlan, Integer> TablePlanDAO                        = null;
    private Dao<TableFreeTrial, Integer> TableFreeTrialDAO              = null;
    private Dao<TableFormApp, Integer> TableFormAppsDAO                 = null;
    private Dao<TableFormRejoin, Integer> TableFormRejoinDAO            = null;
    private Dao<TableFormChildView, Integer> TableFormChildViewDAO      = null;

    private Dao<TableMainRejoin, Integer> TableMainRejoinDAO            = null;
    private Dao<TableTaskList, Integer> TableTaskListDAO                = null;
    private Dao<TableProfileAddress, Integer> TableProfileAddressDAO    = null;

    public DatabaseManager(Context context) {
        super(context, DATABASE_PATH + DATABASE_NAME, null, DATABASE_VERSION);

        boolean dbexist = checkdatabase();
        if (!dbexist) {

            // If database did not exist, try copying existing database from assets folder.
            try {
                File dir = new File(DATABASE_PATH);
                dir.mkdirs();
                InputStream myinput = context.getAssets().open(DATABASE_NAME);
                String outfilename  = DATABASE_PATH + DATABASE_NAME;
                Log.i(DatabaseManager.class.getName(), "DB Path : " + outfilename);

                OutputStream myoutput = new FileOutputStream(outfilename);
                byte[] buffer = new byte[1024];
                int length;

                while ((length = myinput.read(buffer)) > 0) {
                    myoutput.write(buffer, 0, length);
                }

                myoutput.flush();
                myoutput.close();
                myinput.close();

                DatabaseManager db = new DatabaseManager(context);
                db.setUpgrade();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void setUpgrade() {
        SQLiteDatabase db   = getWritableDatabase();
        ConnectionSource cs = getConnectionSource();

        try {
            onUpgrade(db, cs, db.getVersion(), DATABASE_VERSION + 1);
        } catch (SQLiteException e) {
            System.out.println("Database doesn't exist");
            e.printStackTrace();
        }
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, TableLogLogin.class);
            TableUtils.createTable(connectionSource, TableListMain.class);
            TableUtils.createTable(connectionSource, TableFreeTrialSOH.class);
            TableUtils.createTable(connectionSource, TableMasterPackage.class);
            TableUtils.createTable(connectionSource, TableMasterPromo.class);
            TableUtils.createTable(connectionSource, TableMasterMaterial.class);
            TableUtils.createTable(connectionSource, TablePlan.class);
            TableUtils.createTable(connectionSource, TableFreeTrial.class);
            TableUtils.createTable(connectionSource, TableFormApp.class);
            TableUtils.createTable(connectionSource, TableFormRejoin.class);
            TableUtils.createTable(connectionSource, TableFormChildView.class);
            TableUtils.createTable(connectionSource, TableMainRejoin.class);
            TableUtils.createTable(connectionSource, TableTaskList.class);
            TableUtils.createTable(connectionSource, TableProfileAddress.class);
        } catch (SQLException e) {
            Log.e("this", "Can't create database", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int oldVersion, int newVersion) {
        try {
            TableUtils.dropTable(connectionSource, TableLogLogin.class, true);
            TableUtils.dropTable(connectionSource, TableListMain.class, true);
            TableUtils.dropTable(connectionSource, TableFreeTrialSOH.class, true);
            TableUtils.dropTable(connectionSource, TableMasterPackage.class, true);
            TableUtils.dropTable(connectionSource, TableMasterPromo.class, true);
            TableUtils.dropTable(connectionSource, TableMasterMaterial.class, true);
            TableUtils.dropTable(connectionSource, TablePlan.class, true);
            TableUtils.dropTable(connectionSource, TableFreeTrial.class, true);
            TableUtils.dropTable(connectionSource, TableFormApp.class, true);
            TableUtils.dropTable(connectionSource, TableFormRejoin.class, true);
            TableUtils.dropTable(connectionSource, TableFormChildView.class, true);
            TableUtils.dropTable(connectionSource, TableMainRejoin.class, true);
            TableUtils.dropTable(connectionSource, TableTaskList.class, true);
            TableUtils.dropTable(connectionSource, TableProfileAddress.class, true);
            onCreate(sqLiteDatabase, connectionSource);
        } catch (SQLException e) {
            Log.e("this", "Can't create database", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Check whether or not database exist
    |-----------------------------------------------------------------------------------------------
    */
    private boolean checkdatabase() {
        boolean checkdb = false;

        String myPath = DATABASE_PATH + DATABASE_NAME;
        File dbfile = new File(myPath);
        checkdb = dbfile.exists();

        Log.i(DatabaseManager.class.getName(), "DB Exist : " + checkdb);

        return checkdb;
    }

    public Dao<TableLogLogin, Integer> getTableLogLoginDAO() {
        if (null == TableLogLoginDAO) {
            try {
                TableLogLoginDAO = getDao(TableLogLogin.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableLogLoginDAO;
    }

    public Dao<TableListMain, Integer> getTableInstalasiDAO() {
        if (null == TableInstalasiDAO) {
            try {
                TableInstalasiDAO = getDao(TableListMain.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableInstalasiDAO;
    }

    public Dao<TableFreeTrialSOH, Integer> getTableFreeTrialSOHDAO() {
        if (null == TableFreeTrialSOHDAO) {
            try {
                TableFreeTrialSOHDAO = getDao(TableFreeTrialSOH.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableFreeTrialSOHDAO;
    }

    public Dao<TableMasterPackage, Integer> getTableMasterPackageDAO() {
        if (null == TableMasterPackageDAO) {
            try {
                TableMasterPackageDAO = getDao(TableMasterPackage.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableMasterPackageDAO;
    }

    public Dao<TableMasterPromo, Integer> getTableMasterPromoDAO() {
        if (null == TableMasterPromoDAO) {
            try {
                TableMasterPromoDAO = getDao(TableMasterPromo.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableMasterPromoDAO;
    }

    public Dao<TableMasterMaterial, Integer> getTableMasterMaterialDAO() {
        if (null == TableMasterMaterialDAO) {
            try {
                TableMasterMaterialDAO = getDao(TableMasterMaterial.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableMasterMaterialDAO;
    }

    public Dao<TablePlan, Integer> getTablePlanDAO() {
        if (null == TablePlanDAO) {
            try {
                TablePlanDAO = getDao(TablePlan.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TablePlanDAO;
    }

    public Dao<TableFreeTrial, Integer> getTableFreeTrialDAO() {
        if (null == TableFreeTrialDAO) {
            try {
                TableFreeTrialDAO = getDao(TableFreeTrial.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableFreeTrialDAO;
    }

    public Dao<TableFormApp, Integer> getTableFormAppsDAO() {
        if (null == TableFormAppsDAO) {
            try {
                TableFormAppsDAO = getDao(TableFormApp.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableFormAppsDAO;
    }

    public Dao<TableFormRejoin, Integer> getTableFormRejoinDAO() {
        if (null == TableFormRejoinDAO) {
            try {
                TableFormRejoinDAO = getDao(TableFormRejoin.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableFormRejoinDAO;
    }

    public Dao<TableFormChildView, Integer> getTableFormChildViewDAO() {
        if (null == TableFormChildViewDAO) {
            try {
                TableFormChildViewDAO = getDao(TableFormChildView.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableFormChildViewDAO;
    }

    public Dao<TableMainRejoin, Integer> getTableMainRejoinDAO() {
        if (null == TableMainRejoinDAO) {
            try {
                TableMainRejoinDAO = getDao(TableMainRejoin.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableMainRejoinDAO;
    }

    public Dao<TableTaskList, Integer> getTableTaskListDAO() {
        if (null == TableTaskListDAO) {
            try {
                TableTaskListDAO = getDao(TableTaskList.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableTaskListDAO;
    }

    public Dao<TableProfileAddress, Integer> getTableProfileAddressDAO() {
        if (null == TableProfileAddressDAO) {
            try {
                TableProfileAddressDAO = getDao(TableProfileAddress.class);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return TableProfileAddressDAO;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Close the database connections and clear any cached tableDAO.
    |-----------------------------------------------------------------------------------------------
    */
    @Override
    public void close() {
        super.close();

        TableLogLoginDAO        = null;
        TableInstalasiDAO       = null;
        TableFreeTrialSOHDAO    = null;
        TableMasterPackageDAO   = null;
        TableMasterPromoDAO     = null;
        TableMasterMaterialDAO  = null;
        TablePlanDAO            = null;
        TableFreeTrialDAO       = null;
        TableFormAppsDAO        = null;
        TableFormRejoinDAO      = null;
        TableFormChildViewDAO   = null;
        TableMainRejoinDAO      = null;
        TableTaskListDAO        = null;
        TableProfileAddressDAO  = null;
    }
}