package myskysfa.com.ts.main.sliding;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.ChecklistAdapter;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.utils.CheckList;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Checklist extends Fragment {
    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private SwipeRefreshLayout chk_swipecontainer;
    private RecyclerView chk_recyclerview;
    private TextView chk_tittle;
    private Button dtl_chk_save;

    private Utils utils;
    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;
    private int signal;
    private String imei;

    private ChecklistAdapter adapter;
    private ArrayList<CheckList> checkList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.add_checklist, container, false);
        initView(viewGroup);

        chk_swipecontainer.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        chk_swipecontainer.setEnabled(true);
        chk_swipecontainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        refreshItems();
        loadListener();

        return viewGroup;
    }

    private void initView(ViewGroup root) {
        utils = new Utils(getActivity());

        chk_swipecontainer  = (SwipeRefreshLayout) root.findViewById(R.id.add_chk_swipecontainer);
        chk_tittle          = (TextView) root.findViewById(R.id.add_chk_tittle);
        chk_recyclerview    = (RecyclerView) root.findViewById(R.id.add_chk_recyclerview);
        dtl_chk_save        = (Button)root.findViewById(R.id.adddtl_chk_save);

        chk_recyclerview.setNestedScrollingEnabled(false);
        dtl_chk_save.setVisibility(View.GONE);

        chk_tittle.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
        dtl_chk_save.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
    }

    private void refreshItems() {
        connDetect          = new ConnectionDetector(getActivity());
        isInternetPresent   = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            new getDataChecklist().execute();
        } else {
            if (chk_swipecontainer.isShown()) {
                chk_swipecontainer.setRefreshing(false);
            }

            utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
        }
    }

    /*
    |---------------------------------------------------------------------------------------------------
    | Get Data Checklist From Server
    |---------------------------------------------------------------------------------------------------
    */
    private class getDataChecklist extends AsyncTask<String, String, String> {
        private Boolean status;
        private JSONArray dataArray;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            chk_swipecontainer.setRefreshing(true);

            utils   = new Utils(getActivity());
            signal  = utils.checkSignal();
            imei    = utils.getIMEI();
            status  = false;

            checkList = new ArrayList<CheckList>();
            checkList.clear();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(getActivity());
                listLogLogin        = dbAdapterLogLogin.getAllData(getActivity());

                if (listLogLogin.size() > 0) {
                    String url      = ConnectionManager.CM_URL_MASTER_PROMOTION;
                    String token    = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    //TDT String response = ConnectionManager.requestDataMaster(url, imei, token, Config.version, String.valueOf(signal));
                    String response = "{\"status\": \"true\", " +
                            "\"data\":[\n" +
                            "      {\n" +
                            "        \"id_checklist\": \"1\",\n" +
                            "        \"label_checklist\": \"KTP\",\n" +
                            "        \"edit_checklist\": \"123456789\",\n" +
                            "        \"status_checklist\": \"1\"\n" +
                            "     \n" +
                            "      },{\n" +
                            "        \"id_checklist\": \"2\",\n" +
                            "        \"label_checklist\": \"Card\",\n" +
                            "        \"edit_checklist\": \"\",\n" +
                            "        \"status_checklist\": \"0\"\n" +
                            "     \n" +
                            "      },{\n" +
                            "        \"id_checklist\": \"3\",\n" +
                            "        \"label_checklist\": \"No Surat\",\n" +
                            "        \"edit_checklist\": \"\",\n" +
                            "        \"status_checklist\": \"0\"\n" +
                            "     \n" +
                            "      },{\n" +
                            "        \"id_checklist\": \"4\",\n" +
                            "        \"label_checklist\": \"Dana\",\n" +
                            "        \"edit_checklist\": \"30000\",\n" +
                            "        \"status_checklist\": \"1\"\n" +
                            "     \n" +
                            "      },{\n" +
                            "        \"id_checklist\": \"5\",\n" +
                            "        \"label_checklist\": \"ID Sales\",\n" +
                            "        \"edit_checklist\": \"\",\n" +
                            "        \"status_checklist\": \"0\"\n" +
                            "     \n" +
                            "      }\n" +
                            "    ]\n" +
                            "}";

                    Log.d("TSApp", "response= " + response);

                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        status      = jsonObject.getBoolean("status");
                        dataArray   = jsonObject.getJSONArray("data");

                        if (status) {
                            //TDT dbAdapterMasterPromo = new TableMasterPromoAdapter(getActivity());
                            //TDT dbAdapterMasterPromo.deleteAllData();

                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject objData      = dataArray.getJSONObject(i);

                                String id_checklist     = objData.getString("id_checklist");
                                String label_checklist  = objData.getString("label_checklist");
                                String edit_checklist   = objData.getString("edit_checklist");
                                String status_checklist = objData.getString("status_checklist");

                                //TDT dbAdapterMasterPromo = new TableMasterPromoAdapter(getActivity());
                                //TDT dbAdapterMasterPromo.insertData(new TableMasterPromo(), promotion_id,
                                        //TDT promotion_code, promotion_desc.toUpperCase(), start_date, end_date, "A", "1");

                                CheckList item  = new CheckList(getActivity(), id_checklist, label_checklist, edit_checklist, status_checklist);
                                checkList.add(item);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (chk_swipecontainer.isShown()) {
                    chk_swipecontainer.setRefreshing(false);
                }

                if (status) {
                    showList();

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "MasterPromo");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void showList() {
        LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
        layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
        chk_recyclerview.setLayoutManager(layoutParams);

        //TDT dbAdapterMasterPromo    = new TableMasterPromoAdapter(getActivity());
        //TDT listMasterPromo         = dbAdapterMasterPromo.getAllData(getActivity());
        adapter                 = new ChecklistAdapter(getActivity(), checkList);
        chk_recyclerview.setAdapter(adapter);
        dtl_chk_save.setVisibility(View.VISIBLE);
    }

    private void loadListener() {
        dtl_chk_save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //mFormAppAdapter.updatePartial(getActivity(), nama kolom, isinya / isi jsonnya, dimana tabelyang akan di update, idnya );
                JSONObject jResult  = new JSONObject();// main object
                JSONArray jArray    = new JSONArray();// /ItemDetail jsonArray

                for (int i = 0; i < checkList.size(); i++) {
                    JSONObject jGroup = new JSONObject();// /sub Object

                    try {
                        if (checkList.get(i).getCHECKLIST_STATUS() == "1") {
                            jGroup.put("CHECKLIST_ID", checkList.get(i).getID_CHECKLIST());
                            jGroup.put("CHECKLIST_LABEL", checkList.get(i).getCHECKLIST_LABEL());
                            jGroup.put("CHECKLIST_EDIT_LABEL", checkList.get(i).getCHECKLIST_LABEL_EDIT());
                            jGroup.put("CHECKLIST_STATUS", checkList.get(i).getCHECKLIST_STATUS());

                            jArray.put(jGroup);

                            // /itemDetail Name is JsonArray Name
                            jResult.put("data", jArray);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

                Toast.makeText(getActivity(), jResult.toString(), Toast.LENGTH_SHORT).show();
            }
        });
    }
}