package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/4/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "m_package")
public class TableMasterPackage {
    public static final String TABLE_NAME       = "m_package";

    public static final String fPRODUCT_ID      = "product_id";
    public static final String fPRODUCT_TYPE    = "product_type";
    public static final String fPRODUCT_NAME    = "product_name";
    public static final String fPRICE           = "price";
    public static final String fALACARTE        = "alacarte";
    public static final String fFLAG_PRODUCT    = "flag_product";
    public static final String fBRAND           = "brand";

    @DatabaseField(id = true)
    private String product_id;

    @DatabaseField
    private String product_type, product_name;

    @DatabaseField
    private int price, alacarte, flag_product;

    @DatabaseField
    private String brand;

    public void setfPRODUCT_ID(String product_id) {
        this.product_id = product_id;
    }
    public String getfPRODUCT_ID() {
        return product_id;
    }

    public void setfPRODUCT_TYPE(String product_type) {
        this.product_type = product_type;
    }
    public String getfPRODUCT_TYPE() {
        return product_type;
    }

    public void setfPRODUCT_NAME(String product_name) {
        this.product_name = product_name;
    }
    public String getfPRODUCT_NAME() {
        return product_name;
    }

    public void setfPRICE(int price) {
        this.price = price;
    }
    public int getfPRICE() {
        return price;
    }

    public void setfALACARTE(int alacarte) {
        this.alacarte = alacarte;
    }
    public int getfALACARTE() {
        return alacarte;
    }

    public void setfFLAG_PRODUCT(int flag_product) {
        this.flag_product = flag_product;
    }
    public int getfFLAG_PRODUCT() {
        return flag_product;
    }

    public void setfBRAND(String brand) {
        this.brand = brand;
    }
    public String getfBRAND() {
        return brand;
    }
}
