package myskysfa.com.ts.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.itemObject.AlacarteStore;
import myskysfa.com.ts.database.TableMasterPackage;
import myskysfa.com.ts.utils.Utils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/7/2016.
|---------------------------------------------------------------------------------------------------
*/
public class AlacarteListAdapter extends RecyclerView.Adapter<AlacarteListAdapter.ViewHolder> {
    private Context context;
    public static List<TableMasterPackage> itemData = new ArrayList<>();
    private View itemLayoutView;
    public AlacarteStore itemAlacarte = new AlacarteStore();

    public AlacarteListAdapter(Context _context, List<TableMasterPackage> list) {
        this.context    = _context;
        this.itemData   = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView          = LayoutInflater.from(parent.getContext()).inflate(R.layout.pkg_alacarte_item, parent, false);
        ViewHolder viewHolder   = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableMasterPackage itemMasterPackage = itemData.get(position);

        if (itemMasterPackage != null) {
            holder.ala_item_icon.setText(itemMasterPackage.getfPRODUCT_NAME().substring(0, 1).toUpperCase());
            holder.ala_item_id.setText(itemMasterPackage.getfPRODUCT_NAME());
            holder.ala_item_checkbox.setTag(itemMasterPackage.getfPRODUCT_ID() + "#" + itemMasterPackage.getfPRODUCT_NAME());
        }

        Set<String> hs = new HashSet<>();
        hs.addAll(itemAlacarte.getList());
        itemAlacarte.getList().clear();
        itemAlacarte.getList().addAll(hs);
        holder.ala_item_checkbox.setOnCheckedChangeListener(checkAlacarte);
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private Utils utils;
        private TextView ala_item_icon, ala_item_id;
        private CheckBox ala_item_checkbox;

        public ViewHolder(View itemView) {
            super(itemView);
            utils = new Utils(context);

            ala_item_icon       = (TextView) itemLayoutView.findViewById(R.id.ala_item_icon);
            ala_item_id         = (TextView) itemLayoutView.findViewById(R.id.ala_item_id);
            ala_item_checkbox   = (CheckBox) itemLayoutView.findViewById(R.id.ala_item_checkbox);

            ala_item_icon.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
            ala_item_id.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        }
    }


    private CheckBox.OnCheckedChangeListener checkAlacarte = new CheckBox.OnCheckedChangeListener(){
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if(buttonView.isChecked()){
                itemAlacarte.setList(buttonView.getTag().toString());
                Log.i("Cobaitemala=SET", String.valueOf(buttonView.getTag().toString()));
                Log.i("Cobaitemala=get", String.valueOf(itemAlacarte.getList()));
            }else{
                itemAlacarte.removeAlacarte(buttonView.getTag().toString());
                Log.i("Cobaitemala=SET", String.valueOf(buttonView.getTag().toString()));
                Log.i("Cobaitemala=get", String.valueOf(itemAlacarte.getList()));
            }
        }
    };
}