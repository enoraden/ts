package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import myskysfa.com.ts.database.TableListMain;
import myskysfa.com.ts.utils.DatabaseManager;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableListMainAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableListMainAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private synchronized DatabaseManager getHelper() {
        return helper;
    }


    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableListMain> getDatabyCondition(Context context, String condition, Object param) {
        List<TableListMain> listTable = null;

        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableListMain.class);

            QueryBuilder<TableListMain, Integer> queryBuilder = dao.queryBuilder();
            Where<TableListMain, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableListMain> getAllData(Context context) {
        List<TableListMain> listTable = null;
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableListMain.class);
            QueryBuilder<TableListMain, Integer> queryBuilder = dao.queryBuilder();
            listTable = queryBuilder.query();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableListMain table, String ticket_number, String customer_id, String customer_nbr,
                           String customer_name, String assign_to, String nature_code, String nature_descr,
                           String nature_category, String brand, String notes, String schedule_date,
                           String status_ticket) {
        try {
            table.setfTICKET_NUMBER(ticket_number);
            table.setfCUSTOMER_ID(customer_id);
            table.setfCUSTOMER_NBR(customer_nbr);
            table.setfCUSTOMER_NAME(customer_name);
            table.setfASSIGN_TO(assign_to);
            table.setfNATURE_CODE(nature_code);
            table.setfNATURE_DESCR(nature_descr);
            table.setfNATURE_CATEGORY(nature_category);
            table.setfBRAND(brand);
            table.setfNOTES(notes);
            table.setfSCHEDULE_DATE(schedule_date);
            table.setfSTATUS_TICKET(status_ticket);
            getHelper().getTableInstalasiDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableListMain.class);

            UpdateBuilder<TableListMain, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String ticket_number, String customer_id, String customer_nbr,
                              String customer_name, String assign_to, String nature_code, String nature_descr,
                              String nature_category, String brand, String notes, String schedule_date,
                              String status_ticket, String condition,
                              Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableListMain.class);

            UpdateBuilder<TableListMain, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableListMain.fTICKET_NUMBER, ticket_number);
            updateBuilder.updateColumnValue(TableListMain.fCUSTOMER_ID, customer_id);
            updateBuilder.updateColumnValue(TableListMain.fCUSTOMER_NBR, customer_nbr);
            updateBuilder.updateColumnValue(TableListMain.fCUSTOMER_NAME, customer_name);
            updateBuilder.updateColumnValue(TableListMain.fASSIGN_TO, assign_to);
            updateBuilder.updateColumnValue(TableListMain.fNATURE_CODE, nature_code);
            updateBuilder.updateColumnValue(TableListMain.fNATURE_DESCR, nature_descr);
            updateBuilder.updateColumnValue(TableListMain.fNATURE_CATEGORY, nature_category);
            updateBuilder.updateColumnValue(TableListMain.fBRAND, brand);
            updateBuilder.updateColumnValue(TableListMain.fNOTES, notes);
            updateBuilder.updateColumnValue(TableListMain.fSCHEDULE_DATE, schedule_date);
            updateBuilder.updateColumnValue(TableListMain.fSTATUS_TICKET, status_ticket);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableListMain.class);

            DeleteBuilder<TableListMain, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableListMain> listTable = null;
        try {
            listTable = getHelper().getTableInstalasiDAO().queryForAll();
            getHelper().getTableInstalasiDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void deleteAll(Context context) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableListMain.class);
            DeleteBuilder<TableListMain, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }
}