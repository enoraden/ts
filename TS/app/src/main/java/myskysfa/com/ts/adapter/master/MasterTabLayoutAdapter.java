package myskysfa.com.ts.adapter.master;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import myskysfa.com.ts.main.master.MasterFreeTrialSOH;
import myskysfa.com.ts.main.master.MasterMaterial;
import myskysfa.com.ts.main.master.MasterPackage;
import myskysfa.com.ts.main.master.MasterPromo;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/27/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterTabLayoutAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public MasterTabLayoutAdapter(FragmentManager manager, int NumOfTabs) {
        super(manager);
        this.mNumOfTabs = NumOfTabs;
    }
    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                MasterFreeTrialSOH mStockOnHand = new MasterFreeTrialSOH();
                return mStockOnHand;

            case 1:
                MasterPackage mPackage = new MasterPackage();
                return mPackage;

            case 2:
                MasterPromo mPromo = new MasterPromo();
                return mPromo;

            case 3:
                MasterMaterial mMaterial = new MasterMaterial();
                return mMaterial;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}
