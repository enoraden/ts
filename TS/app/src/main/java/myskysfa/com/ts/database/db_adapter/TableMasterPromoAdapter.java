package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import myskysfa.com.ts.database.TableMasterPromo;
import myskysfa.com.ts.utils.DatabaseManager;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/4/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableMasterPromoAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableMasterPromoAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableMasterPromo> getDatabyCondition(Context context, String condition, Object param) {
        List<TableMasterPromo> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPromo.class);

            QueryBuilder<TableMasterPromo, Integer> queryBuilder = dao.queryBuilder();
            Where<TableMasterPromo, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableMasterPromo> getAllData(Context context) {
        List<TableMasterPromo> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPromo.class);

            QueryBuilder<TableMasterPromo, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder
                    .orderBy(TableMasterPromo.fPROMOTION_ID, false)
                    .query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableMasterPromo table, String promotion_id, String promotion_code,
                           String promotion_desc, String start_date, String end_date, String status,
                           String flag_promo) {
        try {
            table.setfPROMOTION_ID(promotion_id);
            table.setfPROMOTION_CODE(promotion_code);
            table.setfPROMOTION_DESC(promotion_desc);
            table.setfSTART_DATE(start_date);
            table.setfEND_DATE(end_date);
            table.setfSTATUS(status);
            table.setfFLAG_PROMO(flag_promo);
            getHelper().getTableMasterPromoDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPromo.class);

            UpdateBuilder<TableMasterPromo, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String promotion_id, String promotion_code,
                              String promotion_desc, String start_date, String end_date, String status,
                              String flag_promo, String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPromo.class);

            UpdateBuilder<TableMasterPromo, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableMasterPromo.fPROMOTION_ID, promotion_id);
            updateBuilder.updateColumnValue(TableMasterPromo.fPROMOTION_CODE, promotion_code);
            updateBuilder.updateColumnValue(TableMasterPromo.fPROMOTION_DESC, promotion_desc);
            updateBuilder.updateColumnValue(TableMasterPromo.fSTART_DATE, start_date);
            updateBuilder.updateColumnValue(TableMasterPromo.fEND_DATE, end_date);
            updateBuilder.updateColumnValue(TableMasterPromo.fSTATUS, status);
            updateBuilder.updateColumnValue(TableMasterPromo.fFLAG_PROMO, flag_promo);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableMasterPromo.class);

            DeleteBuilder<TableMasterPromo, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableMasterPromo> listTable = null;
        try {
            listTable = getHelper().getTableMasterPromoDAO().queryForAll();
            getHelper().getTableMasterPromoDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
