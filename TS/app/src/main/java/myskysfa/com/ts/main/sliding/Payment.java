package myskysfa.com.ts.main.sliding;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.SpinnerArrayAdapter;
import myskysfa.com.ts.database.TableFormApp;
import myskysfa.com.ts.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/27/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Payment extends Fragment {
    private Utils utils;
    private TextView jmlbayar, carabayar, databayar, norek, nama, namabank, masaberlaku, periodebayar,card_type,card_no,tanggalbayar,
           rp,remark,dtl_pyt_carabayar,dtl_pyt_jmlbayar,dtl_pyt_cardtype , dtl_pyt_masaberlaku ,dtl_pyt_periodebayar,dtl_pyt_tanggalbayar;
    private ViewGroup viewGroup;
    private List<TableFormApp> list;
    private TableFormAppAdapter mFormAppAdapter;
    private String name_cardType;
    private EditText  dtl_pyt_norek, dtl_pyt_nama, dtl_pyt_namabank,
           dtl_pyt_remark,dtl_pyt_cardno,dtl_pyt_amount;
    private Button dtl_pyt_save ;
    private String uId, no_ticket, value, idMethodBayar,idCardType;
    private SpinnerArrayAdapter spinnerCarabayar;

    private LinearLayout lyt_pyt_cardtype, lyt_pyt_cardno, lyt_pyt_norek, lyt_pyt_namabank, lyt_pyt_masaberlaku,lyt_pyt_tanggalbayar;
    private ArrayAdapter<String> arrayAdapter;
    private List<String> assignValue;
    private Boolean isCC = false, isAtm = false, isExpired = true;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
          viewGroup = (ViewGroup) inflater.inflate(R.layout.add_payment, container, false);

        loadResources(viewGroup);
        loadListener();

        return viewGroup;
    }

    public void loadResources(ViewGroup root) {
        utils = new Utils(getActivity());
        mFormAppAdapter = new TableFormAppAdapter(getActivity());
        jmlbayar        = (TextView) root.findViewById(R.id.add_pyt_jmlbayar);
        databayar       = (TextView) root.findViewById(R.id.add_pyt_databayar);
        carabayar       = (TextView) root.findViewById(R.id.add_pyt_carabayar);
        norek           = (TextView) root.findViewById(R.id.add_pyt_norek);
        nama            = (TextView) root.findViewById(R.id.add_pyt_nama);
        namabank        = (TextView) root.findViewById(R.id.add_pyt_namabank);
        masaberlaku     = (TextView) root.findViewById(R.id.add_pyt_masaberlaku);
        periodebayar    = (TextView) root.findViewById(R.id.add_pyt_periodebayar);
        remark          = (TextView) root.findViewById(R.id.add_pyt_remark);
        card_type       = (TextView) root.findViewById(R.id.add_pyt_cardtype);
        card_no       = (TextView) root.findViewById(R.id.add_pyt_cardno);
        tanggalbayar       = (TextView) root.findViewById(R.id.add_pyt_tanggalbayar);
        rp       = (TextView) root.findViewById(R.id.add_pyt_rp);

        dtl_pyt_jmlbayar        = (TextView) root.findViewById(R.id.adddtl_pyt_jmlbayar);
        dtl_pyt_carabayar       = (TextView) root.findViewById(R.id.adddtl_pyt_carabayar);
        dtl_pyt_cardtype        = (TextView) root.findViewById(R.id.adddtl_pyt_cardtype);
        dtl_pyt_cardno          = (EditText) root.findViewById(R.id.adddtl_pyt_cardno);
        dtl_pyt_norek           = (EditText) root.findViewById(R.id.adddtl_pyt_norek);
        dtl_pyt_nama            = (EditText) root.findViewById(R.id.adddtl_pyt_nama);
        dtl_pyt_namabank        = (EditText) root.findViewById(R.id.adddtl_pyt_namabank);
        dtl_pyt_tanggalbayar     = (TextView) root.findViewById(R.id.adddtl_pyt_tanggalbayar);
        dtl_pyt_masaberlaku     = (TextView) root.findViewById(R.id.adddtl_pyt_masaberlaku);
        dtl_pyt_periodebayar    = (TextView) root.findViewById(R.id.adddtl_pyt_periodebayar);
        dtl_pyt_remark          = (EditText) root.findViewById(R.id.adddtl_pyt_remark);
        dtl_pyt_amount         = (EditText) root.findViewById(R.id.adddtl_pyt_rp);
        dtl_pyt_save            = (Button) root.findViewById(R.id.adddtl_pyt_save);


        lyt_pyt_cardtype = (LinearLayout) root.findViewById(R.id.lyt_pyt_cardtype);
        lyt_pyt_cardno = (LinearLayout) root.findViewById(R.id.lyt_pyt_cardno);
        lyt_pyt_norek = (LinearLayout) root.findViewById(R.id.lyt_pyt_norek);
        lyt_pyt_namabank = (LinearLayout) root.findViewById(R.id.lyt_pyt_namabank);
        lyt_pyt_masaberlaku = (LinearLayout) root.findViewById(R.id.lyt_pyt_masaberlaku);
        lyt_pyt_tanggalbayar = (LinearLayout) root.findViewById(R.id.lyt_pyt_tanggalbayar);


        setFontType();
        dtl_pyt_save.setOnClickListener(_dtl_pyt_save);
        dtl_pyt_masaberlaku.setOnClickListener(_dtl_pyt_masaberlaku);
        dtl_pyt_periodebayar.setOnClickListener(_dtl_pyt_periodebayar);
        dtl_pyt_carabayar.setOnClickListener(_dtl_pyt_carabayar);
        dtl_pyt_cardtype.setOnClickListener(_dtl_pyt_cardtype);
        dtl_pyt_tanggalbayar.setOnClickListener(_dtl_pyt_tanggalbayar);

    }

    private void setFontType() {
        jmlbayar.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
        databayar.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
        carabayar.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        norek.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        nama.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        namabank.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        masaberlaku.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        periodebayar.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        remark.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        card_type.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        card_no.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        tanggalbayar.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        rp.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));

        dtl_pyt_carabayar.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_tanggalbayar.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_cardno.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_amount.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_cardtype.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_jmlbayar.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
        dtl_pyt_norek.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_nama.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_namabank.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_masaberlaku.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_periodebayar.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_remark.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_pyt_save.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
    }

    private void loadListener() {
        dtl_pyt_jmlbayar.setText("Rp.  "+"0"+",-");
    }

    private View.OnClickListener _dtl_pyt_masaberlaku = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setDateField(dtl_pyt_masaberlaku);
        }
    };

    private View.OnClickListener _dtl_pyt_periodebayar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setDateField(dtl_pyt_periodebayar);
        }
    };

    private View.OnClickListener _dtl_pyt_save = new View.OnClickListener() {
        @Override
        public void onClick(View v) {try {
            checkMandatoryForm();
        } catch (Exception e) {
            e.printStackTrace();
        }
        }
    };
    private View.OnClickListener _dtl_pyt_cardtype = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            assignValue = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.card_type)));
            arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item
                    , assignValue);
            new AlertDialog.Builder(getActivity())
                    .setTitle("Card Type")
                    .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dtl_pyt_cardtype.setText(assignValue.get(which));
                            idCardType = assignValue.get(which).substring(0, 1);

                            if (idCardType.equalsIgnoreCase("1")) {
                             name_cardType =  assignValue.get(which).substring(2);
                            } else if (idCardType.equalsIgnoreCase("2")) {
                                name_cardType =  assignValue.get(which).substring(2);
                            } else if (idCardType.equalsIgnoreCase("3")) {
                                name_cardType =  assignValue.get(which).substring(2);
                            } else if (idCardType.equalsIgnoreCase("4")) {
                                name_cardType =  assignValue.get(which).substring(2);
                            } else if (idCardType.equalsIgnoreCase("5")) {
                                name_cardType =  assignValue.get(which).substring(2);
                            } else if (idCardType.equalsIgnoreCase("6")) {
                                name_cardType =  assignValue.get(which).substring(2);
                            } else {
                                name_cardType =  assignValue.get(which).substring(2);
                            }
                        }
                    }).create().show();
        }
    };

    private View.OnClickListener _dtl_pyt_tanggalbayar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setDateField(dtl_pyt_tanggalbayar);
        }
    };

    private View.OnClickListener _dtl_pyt_carabayar = new View.OnClickListener() {
        @Override
        public void onClick(final View v) {
            /*showList = new ShowListDropDown(getActivity(), v, "Cara Bayar",
                    txtByr, getResources().getStringArray(R.array.payment_method));
            showList.Show();*/
            assignValue = new ArrayList<>(Arrays.asList(getResources().getStringArray(R.array.payment_method2)));
            arrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.support_simple_spinner_dropdown_item
                    , assignValue);
            new AlertDialog.Builder(getActivity())
                    .setTitle("Cara Bayar")
                    .setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dtl_pyt_carabayar.setText(assignValue.get(which));
                            idMethodBayar = assignValue.get(which).substring(0, 1);

                            if (idMethodBayar.equalsIgnoreCase("1")) {
                                lyt_pyt_masaberlaku.setVisibility(View.VISIBLE);
                                lyt_pyt_cardtype.setVisibility(View.VISIBLE);
                                lyt_pyt_cardno.setVisibility(View.VISIBLE);
                                isCC = true;
                                isExpired = true;
                                isAtm = false;
                            } else if (idMethodBayar.equalsIgnoreCase("2")) {
                                lyt_pyt_masaberlaku.setVisibility(View.VISIBLE);
                                lyt_pyt_cardtype.setVisibility(View.GONE);
                                lyt_pyt_cardno.setVisibility(View.GONE);
                                dtl_pyt_cardtype.setText("");
                                dtl_pyt_cardno.setText("");
                                isCC = false;
                                isAtm = false;
                                isExpired = true;
                            } else if (idMethodBayar.equalsIgnoreCase("3")) {
                                lyt_pyt_masaberlaku.setVisibility(View.VISIBLE);
                                lyt_pyt_cardtype.setVisibility(View.GONE);
                                lyt_pyt_cardno.setVisibility(View.GONE);
                                lyt_pyt_norek.setVisibility(View.VISIBLE);
                                lyt_pyt_namabank.setVisibility(View.VISIBLE);
                                dtl_pyt_cardtype.setText("");
                                dtl_pyt_cardno.setText("");
                                isCC = false;
                                isAtm = true;
                                isExpired = true;
                            } else {
                                lyt_pyt_masaberlaku.setVisibility(View.GONE);
                                lyt_pyt_cardtype.setVisibility(View.GONE);
                                lyt_pyt_cardno.setVisibility(View.GONE);
                                dtl_pyt_cardtype.setText("");
                                dtl_pyt_cardno.setText("");
                                isCC = false;
                                isAtm = false;
                                isExpired = false;
                            }
                        }
                    }).create().show();
        }
    };

    private void checkMandatoryForm() {
        if (dtl_pyt_carabayar.getText().toString() == null
                || dtl_pyt_carabayar.getText().toString().equalsIgnoreCase("")
                || dtl_pyt_carabayar.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali cara pembayaran", Toast.LENGTH_SHORT).show();
            return;
        }

        if (isAtm) {
            if (dtl_pyt_norek.getText().toString() == null
                    || dtl_pyt_norek.getText().toString().equalsIgnoreCase("")
                    || dtl_pyt_norek.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek kembali no Rekening", Toast.LENGTH_SHORT).show();
                return;
            } else if (dtl_pyt_namabank.getText().toString() == null
                    || dtl_pyt_namabank.getText().toString().equalsIgnoreCase("")
                    || dtl_pyt_namabank.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek kembali nama bank", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if (isExpired) {
            if (dtl_pyt_masaberlaku.getText().toString() == null
                    || dtl_pyt_masaberlaku.getText().toString().equalsIgnoreCase("")
                    || dtl_pyt_masaberlaku.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek kembali masa berlaku", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (isCC) {
            if (dtl_pyt_cardtype.getText().toString() == null
                    || dtl_pyt_cardtype.getText().toString().equalsIgnoreCase("")
                    || dtl_pyt_cardtype.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek kembali Type Card", Toast.LENGTH_SHORT).show();
                return;
            } else if (dtl_pyt_cardno.getText().toString() == null
                    || dtl_pyt_cardno.getText().toString().equalsIgnoreCase("")
                    || dtl_pyt_cardno.getText().toString().length() < 1) {
                Toast.makeText(getActivity(), "Cek No Card", Toast.LENGTH_SHORT).show();
                return;
            }
        }

        if (dtl_pyt_nama.getText().toString() == null
                || dtl_pyt_nama.getText().toString().equalsIgnoreCase("")
                || dtl_pyt_nama.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali nama", Toast.LENGTH_SHORT).show();
            return;
        } if (dtl_pyt_periodebayar.getText().toString() == null
                || dtl_pyt_periodebayar.getText().toString().equalsIgnoreCase("")
                || dtl_pyt_periodebayar.getText().toString().length() < 1) {
            Toast.makeText(getActivity(), "Cek kembali periode bayar", Toast.LENGTH_SHORT).show();
            return;
        } else if (dtl_pyt_amount.getText().toString() == null
                || dtl_pyt_amount.getText().toString().equalsIgnoreCase("")
                || dtl_pyt_amount.getText().toString().trim().length() < 1) {
            Toast.makeText(getActivity(), "Masukkan Jumlah Pembayaran!", Toast.LENGTH_SHORT).show();
            return;
        } else if (dtl_pyt_tanggalbayar.getText().toString() == null
                || dtl_pyt_tanggalbayar.getText().toString().equalsIgnoreCase("")
                || dtl_pyt_tanggalbayar.getText().toString().trim().length() < 1) {
            Toast.makeText(getActivity(), "Masukkan Tanggal Pembayaran", Toast.LENGTH_SHORT).show();
            return;
        } else {
            try {
                value = jsonValueCreator();
                mFormAppAdapter.updatePartial(getActivity(), TableFormApp.fVALUES_PAYMENT,
                        value, TableFormApp.fNO_TICKET, no_ticket);
                Toast.makeText(getActivity(), "Payment Saved", Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getActivity(), "Payment Saved Failed", Toast.LENGTH_SHORT).show();
            }
        }

    }


    private String jsonValueCreator() throws Exception {
        JSONObject obj = new JSONObject();
        //File newFile = new File(imagePath);

        try {
            obj.put("no_ticket", no_ticket);
            obj.put("total", dtl_pyt_amount.getText().toString());
            obj.put("caraBayar", dtl_pyt_carabayar.getText().toString());
            obj.put("name", dtl_pyt_nama.getText().toString());
            if (dtl_pyt_cardno.getText().toString().trim().length() > 0) {
                obj.put("cc_no", dtl_pyt_cardno.getText().toString());
            }
            if (dtl_pyt_cardtype.getText().toString().trim().length() > 0) {
                obj.put("cc_type", dtl_pyt_cardtype.getText().toString());
            }
            if (dtl_pyt_namabank.getText().toString().trim().length() > 0) {
                obj.put("bank", dtl_pyt_namabank.getText().toString());
            }
            if (dtl_pyt_norek.getText().toString().trim().length() > 0) {
                obj.put("noRek", dtl_pyt_norek.getText().toString());
            }
            obj.put("payment_date", dtl_pyt_tanggalbayar.getText().toString());

            if (dtl_pyt_masaberlaku.getText().toString().trim().length() > 0) {
                obj.put("masaBerlaku", dtl_pyt_masaberlaku.getText().toString());
            }
            obj.put("periode", dtl_pyt_periodebayar.getText().toString());
            obj.put("remark", dtl_pyt_remark.getText().toString());
            Log.d("jsonku", obj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj.toString();
    }

    public void setEstimate() {
        try {
            mFormAppAdapter = new TableFormAppAdapter(getActivity());
            list = new ArrayList<>();
            list = mFormAppAdapter.getDatabyCondition(TableFormApp.fNO_TICKET, no_ticket);
            if (list != null && list.size() > 0) {
                try {
                    JSONObject jsonObject = new JSONObject(list.get(0).getVALUES_PACKAGE());
                    if (jsonObject != null) {
                        int hasil = jsonObject.getInt("amount");
                        NumberFormat defaultF = NumberFormat.getInstance();
                        String amount = defaultF.format(hasil)
                                .replace(",", ".");
                        dtl_pyt_jmlbayar.setText(amount);
                    } else {
                        dtl_pyt_jmlbayar.setText("");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
        }

    }

    public void setHideKeyBoard() {
        utils.setHideKeyboard(getActivity(),viewGroup );
    }

    public void setNoTicket() {
        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.no_ticket),
                Context.MODE_PRIVATE);
        no_ticket = sm.getString("no_ticket", null);
    }
}