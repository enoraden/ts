package myskysfa.com.ts.utils;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableFreeTrial;
import myskysfa.com.ts.database.TableLogLogin;

import myskysfa.com.ts.database.TablePlan;
import myskysfa.com.ts.database.db_adapter.TableFreeTrialAdapter;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.database.db_adapter.TablePlanAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/26/2016.
|---------------------------------------------------------------------------------------------------
*/
public class InitData {
    private static Context context;
    private Utils utils;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    public InitData(Context _context) {
        this.context = _context;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data Plan From Server
    |-----------------------------------------------------------------------------------------------
    */
    private class getDataPlan extends AsyncTask<String, String, String> {
        private Boolean _status;
        private JSONArray _dataarray;
        private int _signal;
        private String _imei;
        private TablePlanAdapter dbAdapterPlan;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            utils   = new Utils(context);
            _signal = utils.checkSignal();
            _imei   = utils.getIMEI();
            _status = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(context);
                listLogLogin        = dbAdapterLogLogin.getAllData(context);

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_FTDTD_PLAN_ALL;
                    String _sflcode     = listLogLogin.get(listLogLogin.size() - 1).getfSFL_CODE();
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl    = _url + "/" + _sflcode;

                    String _response = ConnectionManager.requestDataMaster(_paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status     = jsonObject.getBoolean("status");
                        _dataarray  = jsonObject.getJSONArray("data");

                        if (_status) {
                            dbAdapterPlan = new TablePlanAdapter(context);
                            dbAdapterPlan.deleteAllData();

                            for (int i = 0; i < _dataarray.length(); i++) {
                                JSONObject objData      = _dataarray.getJSONObject(i);

                                String plan_id          = objData.getString("plan_id");
                                String plan_date        = objData.getString("plan_date");
                                String zip_code         = objData.getString("zip_code");
                                String area             = objData.getString("area");
                                String ts_code          = objData.getString("ts_code");
                                String ts_name          = objData.getString("ts_name");
                                String target           = objData.getString("target");
                                String sfl_code         = objData.getString("sfl_code");

                                dbAdapterPlan = new TablePlanAdapter(context);
                                dbAdapterPlan.insertData(new TablePlan(), plan_id, plan_date,
                                        zip_code, area, ts_code, ts_name, target, sfl_code);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_status) {

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "MasterPackage");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
    |---------------------------------------------------------------------------------------------------
    | Get Data Free Trial From Server
    |---------------------------------------------------------------------------------------------------
    */
    private class getDataFreeTrial extends AsyncTask<String, String, String> {
        private Boolean _status;
        private JSONArray _dataarray;
        private int _signal;
        private String _imei;
        private TableFreeTrialAdapter dbAdapterFreeTrial;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            utils   = new Utils(context);
            _signal = utils.checkSignal();
            _imei   = utils.getIMEI();
            _status = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(context);
                listLogLogin        = dbAdapterLogLogin.getAllData(context);

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_FTDTD_LIST;
                    String _sflcode     = listLogLogin.get(listLogLogin.size() - 1).getfSFL_CODE();
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl    = _url + "/" + _sflcode;

                    String response = ConnectionManager.requestDataMaster(_paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + response);

                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        _status     = jsonObject.getBoolean("status");
                        _dataarray  = jsonObject.getJSONArray("data");

                        if (_status) {
                            dbAdapterFreeTrial = new TableFreeTrialAdapter(context);
                            dbAdapterFreeTrial.deleteAllData();

                            for (int i = 0; i < _dataarray.length(); i++) {
                                JSONObject objData      = _dataarray.getJSONObject(i);

                                String prospect_nbr     = objData.getString("prospect_nbr");
                                String sfl_code         = objData.getString("sfl_code");
                                String register_date    = objData.getString("register_date");
                                String is_multi         = objData.getString("is_multi");
                                String prospect_name    = objData.getString("prospect_name");
                                String mobile_phone     = objData.getString("mobile_phone");
                                String brand_code       = objData.getString("brand_code");
                                String created_date     = objData.getString("created_date");
                                String status           = objData.getString("status");
                                String user_name        = objData.getString("user_name");
                                String plan_id          = objData.getString("plan_id");
                                String alamat           = objData.getString("alamat");
                                String product_name     = objData.getString("product_name");
                                String product_id       = objData.getString("product_id");
                                String value            = objData.toString();

                                dbAdapterFreeTrial = new TableFreeTrialAdapter(context);
                                dbAdapterFreeTrial.insertData(new TableFreeTrial(), prospect_nbr, sfl_code,
                                        register_date, is_multi, prospect_name, mobile_phone, brand_code,
                                        created_date, status, user_name, plan_id, alamat, product_name,
                                        product_id, value);
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_status) {

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "MasterFreeTrial");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | ActivationTask From Server
    |-----------------------------------------------------------------------------------------------
    */
    private class ActivationTask extends AsyncTask<String, String, String> {
        private String _status, _message;
        private String ticket_number;
        private int _signal;
        private String _imei;

        private ActivationTask(String ticket_number) {
            this.ticket_number = ticket_number;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils       = new Utils(context);
            _signal     = utils.checkSignal();
            _imei       = utils.getIMEI();
            _status     = "";
            _message    = "";

            utils.showDialogLoading("Loading . . .");
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(context);
                listLogLogin        = dbAdapterLogLogin.getAllData(context);

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_FTDTD_ACTIVATION;
                    String _sflcode     = listLogLogin.get(listLogLogin.size() - 1).getfSFL_CODE();
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl    = _url + "/" + ticket_number;

                    String response = ConnectionManager.requestDataMaster(_paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + response);

                    JSONObject jsonObject = new JSONObject(response.toString());
                    if (jsonObject != null) {
                        _status     = jsonObject.getString("status");
                        _message    = jsonObject.getString("message");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return _message;
        }
        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (utils.progressdialog.isShowing())
                    utils.progressdialog.dismiss();

                if (_status.equalsIgnoreCase("200")) {
                    Toast.makeText(context, _message, Toast.LENGTH_SHORT).show();
                    //refresh data list
                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "ActivationTask");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void activationTaskDialog(final String ticket_number) {
        utils = new Utils(context);

        utils.loadAlertDialog(false, "Aktivasi", "Apakah anda ingin melakukan aktivasi untuk no prospect "
                + ticket_number + "?", "", "CANCEL", "AKTIVASI");

        utils._twobtn_left.setBackgroundResource(R.drawable.button_green);
        utils._twobtn_right.setBackgroundResource(R.drawable.button_red);
        utils._twobtn_left.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View paramAnonymousView) {
                utils.dialog.dismiss();
            }
        });
        utils._twobtn_right.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View paramAnonymousView) {
                Toast.makeText(context, "Please Unmark activationTaskDialog", Toast.LENGTH_LONG).show();
                //TDT utils.dialog.dismiss();
                //TDT new ActivationTask(ticket_number).execute();
            }
        });

        utils.dialog.setCancelable(false);
        utils.dialog.show();
    }
}