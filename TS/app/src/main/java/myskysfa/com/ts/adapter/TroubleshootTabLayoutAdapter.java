package myskysfa.com.ts.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import myskysfa.com.ts.main.sliding.Checklist;
import myskysfa.com.ts.main.sliding.LKT;
import myskysfa.com.ts.main.sliding.Package;
import myskysfa.com.ts.main.sliding.Payment;
import myskysfa.com.ts.main.sliding.Signature;
import myskysfa.com.ts.main.sliding.Status;

/*
|---------------------------------------------------------------------------------------------------
| Created by Taufik Hidayat on 1/18/2017.
|---------------------------------------------------------------------------------------------------
*/
public class TroubleshootTabLayoutAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public TroubleshootTabLayoutAdapter(FragmentManager manager, int NumOfTabs) {
        super(manager);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                Package packagee = new Package();
                return packagee;

            case 1:
                Payment payment = new Payment();
                return payment;

            case 2:
                LKT lkt = new LKT();
                return lkt;

            case 3:
                Checklist checklist = new Checklist();
                return checklist;

            case 4:
                Signature signature = new Signature();
                return signature;

            case 5:
                Status status = new Status();
                return status;

            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}