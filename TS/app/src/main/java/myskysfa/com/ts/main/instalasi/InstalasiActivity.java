package myskysfa.com.ts.main.instalasi;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import myskysfa.com.ts.R;
import myskysfa.com.ts.main.TutupTugas;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/28/2016.
|---------------------------------------------------------------------------------------------------
*/
public class InstalasiActivity extends AppCompatActivity {
    private String case_item, ticket_number, customer_id, customer_nbr, customer_name, assign_to,
            nature_code, nature_descr, nature_category, brand, notes, schedule_date, status_ticket;

    private FragmentManager fragmentManager;

    private LinearLayout btnlayout;
    private TextView icon, ticketnumber, status, tooldetail_dateschedule, detail_one, detail_two, detail_three;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_m_activity);

        saveTaskList();
        fragTSList(case_item);
        loadToolbarDatail();
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Shared Preferences
    |-----------------------------------------------------------------------------------------------
    */
    private void saveTaskList (){
        Bundle bundle       = getIntent().getExtras();

        case_item           = bundle.getString("case_item");
        ticket_number       = bundle.getString("ticket_number");
        customer_id         = bundle.getString("customer_id");
        customer_nbr        = bundle.getString("customer_nbr");
        customer_name       = bundle.getString("customer_name");
        assign_to           = bundle.getString("assign_to");
        nature_code         = bundle.getString("nature_code");
        nature_descr        = bundle.getString("nature_descr");
        nature_category     = bundle.getString("nature_category");
        brand               = bundle.getString("brand");
        notes               = bundle.getString("notes");
        schedule_date       = bundle.getString("schedule_date");
        status_ticket       = bundle.getString("status_ticket");

        //ticket_number untuk childview
        SharedPreferences preferences           = getSharedPreferences(getString(R.string.detail_id), Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor    = preferences.edit();
        shareEditor.putString("ticket_number", ticket_number);
        shareEditor.commit();
    }

    private void deleteTaskList() {
        SharedPreferences preferences           = getSharedPreferences(getString(R.string.detail_id), Context.MODE_PRIVATE);
        SharedPreferences.Editor shareEditor    = preferences.edit();
        shareEditor.clear();
        shareEditor.commit();

        finish();
    }

    private void fragTSList(String case_item) {
        try {
            Fragment detail;
            switch (case_item) {
                case "instalasi":
                    detail = new InstalasiDetail();
                    loadResourceFrag(detail);
                    break;

                case "tutup_tugas":
                    detail = new TutupTugas();
                    loadResourceFrag(detail);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadResourceFrag(Fragment fragment) {
        fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.listmact_contentframemain, fragment).commit();
    }


    private void loadToolbarDatail() {
        btnlayout       = (LinearLayout) findViewById(R.id.tooldetail_btnlayout);
        icon            = (TextView) findViewById(R.id.tooldetail_icon);
        ticketnumber    = (TextView) findViewById(R.id.tooldetail_ticketnumber);
        status          = (TextView) findViewById(R.id.tooldetail_status);
        detail_one      = (TextView) findViewById(R.id.tooldetail_detail_one);
        detail_two      = (TextView) findViewById(R.id.tooldetail_detail_two);
        detail_three    = (TextView) findViewById(R.id.tooldetail_detail_three);

        icon.setText(nature_category.toString().toUpperCase().substring(0, 3));
        ticketnumber.setText(ticket_number);

        if (status_ticket.equals("0")){
            status.setText("NEW");
        }else if (status_ticket.equals("1")){
            status.setText("RESOLVE");
        }else{
            status.setText("ACTIVE");
        }

        detail_one.setText(customer_name+" - "+customer_id);
        detail_two.setText(nature_code+" - "+nature_descr);
        detail_three.setText(brand+" - "+"Schedule Date : "+schedule_date);

        btnlayout.setOnClickListener(btnlayout_listener);
    }

    private View.OnClickListener btnlayout_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (case_item.toString().equalsIgnoreCase("tutup_tugas")){
                exitFormDialog();
            } else {
                deleteTaskList();
            }
        }
    };


    @Override
    public void onBackPressed() {
        if (case_item.toString().equalsIgnoreCase("tutup_tugas")){
            exitFormDialog();
        } else {
            deleteTaskList();
        }
    }

    private void exitFormDialog() {
        final Utils utils = new Utils(InstalasiActivity.this);
        utils.loadAlertDialog(false, "Keluar Form!", "Apakah anda yakin ingin keluar dari form ?\n", "", "TIDAK", "YA");

        utils._twobtn_left.setBackgroundResource(R.drawable.button_green);
        utils._twobtn_right.setBackgroundResource(R.drawable.button_red);
        utils._twobtn_left.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
            }
        });
        utils._twobtn_right.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
                deleteTaskList();
            }
        });

        utils.dialog.setCancelable(false);
        utils.dialog.show();
    }
}