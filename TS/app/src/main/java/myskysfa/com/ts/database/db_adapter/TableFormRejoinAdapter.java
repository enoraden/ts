package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;

import java.util.List;

import myskysfa.com.ts.database.TableFormRejoin;
import myskysfa.com.ts.utils.DatabaseManager;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/7/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableFormRejoinAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableFormRejoinAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableFormRejoin> getDatabyCondition(Context context, String condition, Object param) {
        List<TableFormRejoin> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormRejoin.class);

            QueryBuilder<TableFormRejoin, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFormRejoin, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableFormRejoin> getAllData(Context context) {
        List<TableFormRejoin> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormRejoin.class);

            QueryBuilder<TableFormRejoin, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableFormRejoin table, String formid, String id_retrieval, String vc, String dsd, String lnb,
                           String odu, String signature_path, String latitude, String longitude, String result_code,
                           String result_datetime, String result_note, String image_path) {
        try {
            table.setfFORMID(formid);
            table.setfID_RETRIEVAL(id_retrieval);
            table.setfVC(vc);
            table.setfDSD(dsd);
            table.setfLNB(lnb);
            table.setfODU(odu);
            table.setfSIGNATURE_PATH(signature_path);
            table.setfLATITUDE(latitude);
            table.setfLONGITUDE(longitude);
            table.setfRESULT_CODE(result_code);
            table.setfRESULT_DATETIME(result_datetime);
            table.setfIMAGE_PATH(image_path);
            table.setfRESULT_NOTE(result_note);
            getHelper().getTableFormRejoinDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormRejoin.class);

            UpdateBuilder<TableFormRejoin, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String formid, String id_retrieval, String vc, String dsd, String lnb,
                              String odu, String signature_path, String latitude, String longitude, String result_code,
                              String result_datetime, String result_note, String image_path, String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormRejoin.class);

            UpdateBuilder<TableFormRejoin, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFormRejoin.fFORMID, formid);
            updateBuilder.updateColumnValue(TableFormRejoin.fID_RETRIEVAL, id_retrieval);
            updateBuilder.updateColumnValue(TableFormRejoin.fVC, vc);
            updateBuilder.updateColumnValue(TableFormRejoin.fDSD, dsd);
            updateBuilder.updateColumnValue(TableFormRejoin.fLNB, lnb);
            updateBuilder.updateColumnValue(TableFormRejoin.fODU, odu);
            updateBuilder.updateColumnValue(TableFormRejoin.fSIGNATURE_PATH, signature_path);
            updateBuilder.updateColumnValue(TableFormRejoin.fLATITUDE, latitude);
            updateBuilder.updateColumnValue(TableFormRejoin.fLONGITUDE, longitude);
            updateBuilder.updateColumnValue(TableFormRejoin.fRESULT_CODE, result_code);
            updateBuilder.updateColumnValue(TableFormRejoin.fRESULT_DATETIME, result_datetime);
            updateBuilder.updateColumnValue(TableFormRejoin.fRESULT_NOTE, result_note);
            updateBuilder.updateColumnValue(TableFormRejoin.fIMAGE_PATH, image_path);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableFormRejoin.class);

            DeleteBuilder<TableFormRejoin, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableFormRejoin> listTable = null;
        try {
            listTable = getHelper().getTableFormRejoinDAO().queryForAll();
            getHelper().getTableFormRejoinDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}