package myskysfa.com.ts.main.childview;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.TableProfileAddress;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.database.db_adapter.TableProfileAddressAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.MasterAPI;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/14/2016.
|---------------------------------------------------------------------------------------------------
*/
public class ChildProfile {
    private static Context context;
    private Utils utils;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private TextView statusdata;

    private TextView no_ticket, tgl_daftar, kartu_identitas, no_identitas, nama_depan, nama_tengah,
            nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir;
    private TextView idt_alamat, idt_normh, idt_RT, idt_RW, idt_provinsi, idt_kota, idt_kecamatan,
            idt_kelurahan, idt_kodepos, idt_masaberlaku, idt_tglpasang, idt_jampasang, idt_tglkonfirm_start,
            idt_tglkonfirm_end, idt_jamkonfirm_start, idt_jamkonfirm_end;
    private CheckBox inst_checkalamatidt;
    private TextView inst_alamat, inst_patokan, inst_provinsi, inst_kota, inst_kecamatan, inst_kelurahan,
            inst_kodepos;
    private CheckBox bill_checkalamatidt, bill_checkalamatinst;
    private TextView bill_alamat, bill_provinsi, bill_kota, bill_kecamatan, bill_kelurahan, bill_kodepos,
            bill_tlprmh, bill_nohp, bill_email, bill_jenisrmh, bill_statusrmh, bill_pekerjaan, bill_penghasilan;

    public ChildProfile(Context _context) {
        this.context = _context;
    }

    public void profileView(View root) {
        loadResource(root);
        loadListener();
        new getChildProfile().execute();
    }

    private void loadResource(View root) {
        statusdata      = (TextView) root.findViewById(R.id.chdpro_statusdata);
        //------------------------------------------------------------------------------------------
        no_ticket       = (TextView) root.findViewById(R.id.chdpro_no_ticket);
        tgl_daftar      = (TextView) root.findViewById(R.id.chdpro_tgl_daftar);
        kartu_identitas = (TextView) root.findViewById(R.id.chdpro_kartu_identitas);
        no_identitas    = (TextView) root.findViewById(R.id.chdpro_no_identitas);
        nama_depan      = (TextView) root.findViewById(R.id.chdpro_nama_depan);
        nama_tengah     = (TextView) root.findViewById(R.id.chdpro_nama_tengah);
        nama_belakang   = (TextView) root.findViewById(R.id.chdpro_nama_belakang);
        jenis_kelamin   = (TextView) root.findViewById(R.id.chdpro_jenis_kelamin);
        agama           = (TextView) root.findViewById(R.id.chdpro_agama);
        tempat_lahir    = (TextView) root.findViewById(R.id.chdpro_tempat_lahir);
        tgl_lahir       = (TextView) root.findViewById(R.id.chdpro_tgl_lahir);
        //------------------------------------------------------------------------------------------
        idt_alamat      = (TextView) root.findViewById(R.id.chdpro_idt_alamat);
        idt_normh       = (TextView) root.findViewById(R.id.chdpro_idt_normh);
        idt_RT          = (TextView) root.findViewById(R.id.chdpro_idt_RT);
        idt_RW          = (TextView) root.findViewById(R.id.chdpro_idt_RW);
        idt_provinsi    = (TextView) root.findViewById(R.id.chdpro_idt_provinsi);
        idt_kota        = (TextView) root.findViewById(R.id.chdpro_idt_kota);
        idt_kecamatan   = (TextView) root.findViewById(R.id.chdpro_idt_kecamatan);
        idt_kelurahan   = (TextView) root.findViewById(R.id.chdpro_idt_kelurahan);
        idt_kodepos     = (TextView) root.findViewById(R.id.chdpro_idt_kodepos);
        idt_masaberlaku = (TextView) root.findViewById(R.id.chdpro_idt_masaberlaku);
        idt_tglpasang   = (TextView) root.findViewById(R.id.chdpro_idt_tglpasang);
        idt_jampasang   = (TextView) root.findViewById(R.id.chdpro_idt_jampasang);
        idt_tglkonfirm_start    = (TextView) root.findViewById(R.id.chdpro_idt_tglkonfirm_start);
        idt_tglkonfirm_end      = (TextView) root.findViewById(R.id.chdpro_idt_tglkonfirm_end);
        idt_jamkonfirm_start    = (TextView) root.findViewById(R.id.chdpro_idt_jamkonfirm_start);
        idt_jamkonfirm_end      = (TextView) root.findViewById(R.id.chdpro_idt_jamkonfirm_end);
        //------------------------------------------------------------------------------------------
        inst_checkalamatidt     = (CheckBox) root.findViewById(R.id.chdpro_inst_checkalamatidt);
        inst_alamat             = (TextView) root.findViewById(R.id.chdpro_inst_alamat);
        inst_patokan            = (TextView) root.findViewById(R.id.chdpro_inst_patokan);
        inst_provinsi           = (TextView) root.findViewById(R.id.chdpro_inst_provinsi);
        inst_kota               = (TextView) root.findViewById(R.id.chdpro_inst_kota);
        inst_kecamatan          = (TextView) root.findViewById(R.id.chdpro_inst_kecamatan);
        inst_kelurahan          = (TextView) root.findViewById(R.id.chdpro_inst_kelurahan);
        inst_kodepos            = (TextView) root.findViewById(R.id.chdpro_inst_kodepos);
        //------------------------------------------------------------------------------------------
        bill_checkalamatidt     = (CheckBox) root.findViewById(R.id.chdpro_bill_checkalamatidt);
        bill_checkalamatinst    = (CheckBox) root.findViewById(R.id.chdpro_bill_checkalamatinst);
        bill_alamat             = (TextView) root.findViewById(R.id.chdpro_bill_alamat);
        bill_provinsi           = (TextView) root.findViewById(R.id.chdpro_bill_provinsi);
        bill_kota               = (TextView) root.findViewById(R.id.chdpro_bill_kota);
        bill_kecamatan          = (TextView) root.findViewById(R.id.chdpro_bill_kecamatan);
        bill_kelurahan          = (TextView) root.findViewById(R.id.chdpro_bill_kelurahan);
        bill_kodepos            = (TextView) root.findViewById(R.id.chdpro_bill_kodepos);
        bill_tlprmh             = (TextView) root.findViewById(R.id.chdpro_bill_tlprmh);
        bill_nohp               = (TextView) root.findViewById(R.id.chdpro_bill_nohp);
        bill_email              = (TextView) root.findViewById(R.id.chdpro_bill_email);
        bill_jenisrmh           = (TextView) root.findViewById(R.id.chdpro_bill_jenisrmh);
        bill_statusrmh          = (TextView) root.findViewById(R.id.chdpro_bill_statusrmh);
        bill_pekerjaan          = (TextView) root.findViewById(R.id.chdpro_bill_pekerjaan);
        bill_penghasilan        = (TextView) root.findViewById(R.id.chdpro_bill_penghasilan);
        //------------------------------------------------------------------------------------------
        inst_checkalamatidt.setClickable(false);
        bill_checkalamatidt.setClickable(false);
        bill_checkalamatinst.setClickable(false);
    }

    private void loadListener() {
        no_ticket.setText("-");
        tgl_daftar.setText("-");
        kartu_identitas.setText("-");
        no_identitas.setText("-");
        nama_depan.setText("-");
        nama_tengah.setText("-");
        nama_belakang.setText("-");
        jenis_kelamin.setText("-");
        agama.setText("-");
        tempat_lahir.setText("-");
        tgl_lahir.setText("-");
        //------------------------------------------------------------------------------------------
        idt_alamat.setText("-");
        idt_normh.setText("-");
        idt_RT.setText("-");
        idt_RW.setText("-");
        idt_provinsi.setText("-");
        idt_kota.setText("-");
        idt_kecamatan.setText("-");
        idt_kelurahan.setText("-");
        idt_kodepos.setText("-");
        idt_masaberlaku.setText("-");
        idt_tglpasang.setText("-");
        idt_jampasang.setText("-");
        idt_tglkonfirm_start.setText("-");
        idt_tglkonfirm_end.setText("-");
        idt_jamkonfirm_start.setText("-");
        idt_jamkonfirm_end.setText("-");
        //------------------------------------------------------------------------------------------
        inst_checkalamatidt.setChecked(false);
        inst_alamat.setText("-");
        inst_patokan.setText("-");
        inst_provinsi.setText("-");
        inst_kota.setText("-");
        inst_kecamatan.setText("-");
        inst_kelurahan.setText("-");
        inst_kodepos.setText("-");
        //------------------------------------------------------------------------------------------
        bill_checkalamatidt.setChecked(false);
        bill_checkalamatinst.setChecked(false);
        bill_alamat.setText("-");
        bill_provinsi.setText("-");
        bill_kota.setText("-");
        bill_kecamatan.setText("-");
        bill_kelurahan.setText("-");
        bill_kodepos.setText("-");
        bill_tlprmh.setText("-");
        bill_nohp.setText("-");
        bill_email.setText("-");
        bill_jenisrmh.setText("-");
        bill_statusrmh.setText("-");
        bill_pekerjaan.setText("-");
        bill_penghasilan.setText("0"+",-");
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data Child Profile Server
    |-----------------------------------------------------------------------------------------------
    */
    private class getChildProfile extends AsyncTask<String, String, String> {
        private TableProfileAddressAdapter dbAdapterProfileAddress;
        private List<TableProfileAddress> listAddrIdentitas, listAddrPemasangan, listAddrPenagihan;

        private Boolean _status;
        private String _imei;
        private int _signal;

        private SharedPreferences preferences;
        private String _ticket_number;

        private String js_home_phone, js_mobile_phone, js_work_phone, js_email, js_religion,
                js_work, js_income, js_gender;
        private String js_prospect_nbr, js_sfl_code, js_pos_code, js_imei, js_user_name, js_created_by,
                js_brand_code, js_register_date, js_occupation;
        private String js_first_name, js_middle_name, js_last_name, js_tgl_lahir, js_tempat_lahir;
        private String js_identity_type, js_identity_nbr, js_identity_period;
        private String js_emergency_first_name, js_emergency_middle_name, js_emergency_last_name,
                js_emergency_phone, js_emergency_address, js_relation;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils       = new Utils(context);
            preferences = context.getSharedPreferences(context.getString(R.string.detail_id), Context.MODE_PRIVATE);
            _signal     = utils.checkSignal();
            _imei       = utils.getIMEI();
            _status     = false;

            statusdata.setText("Loading . . .");
            statusdata.setVisibility(View.VISIBLE);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(context);
                listLogLogin        = dbAdapterLogLogin.getAllData(context);

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_DETAIL_PROFILE;
                    _ticket_number      = preferences.getString("ticket_number", null);
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl    = _url + "/" + _ticket_number;

                    String _response = ConnectionManager.requestDataMaster(_paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status = jsonObject.getBoolean("status");

                        if (_status) {
                            dbAdapterProfileAddress = new TableProfileAddressAdapter(context);
                            dbAdapterProfileAddress.deleteAllData();

                            String _data        = jsonObject.getString("data");
                            JSONObject objData  = new JSONObject(_data.toString());

                            js_prospect_nbr     = objData.getString("prospect_nbr");
                            js_sfl_code         = objData.getString("sfl_code");
                            js_pos_code         = objData.getString("pos_code");
                            js_imei             = objData.getString("imei");
                            js_user_name        = objData.getString("user_name");
                            js_created_by       = objData.getString("created_by");
                            js_brand_code       = objData.getString("brand_code");
                            js_register_date    = objData.getString("register_date");
                            js_occupation       = objData.getString("occupation");
                            //----------------------------------------------------------------------
                            String _profile         = jsonObject.getString("profile");
                            JSONObject objProfile   = new JSONObject(_profile.toString());

                            js_first_name   = objProfile.getString("first_name");
                            js_middle_name  = objProfile.getString("middle_name");
                            js_last_name    = objProfile.getString("last_name");
                            js_tgl_lahir    = objProfile.getString("tgl_lahir");
                            js_tempat_lahir = objProfile.getString("tempat_lahir");
                            //----------------------------------------------------------------------
                            js_home_phone   = jsonObject.getString("home_phone");
                            js_mobile_phone = jsonObject.getString("mobile_phone");
                            js_work_phone   = jsonObject.getString("work_phone");
                            js_email        = jsonObject.getString("email");
                            js_religion     = jsonObject.getString("religion");
                            js_work         = jsonObject.getString("work");
                            js_income       = jsonObject.getString("income");
                            js_gender       = jsonObject.getString("gender");
                            //----------------------------------------------------------------------
                            String _identity        = jsonObject.getString("identity");
                            JSONObject objIdentity  = new JSONObject(_identity.toString());

                            js_identity_type    = objIdentity.getString("identity_type");
                            js_identity_nbr     = objIdentity.getString("identity_nbr");
                            js_identity_period  = objIdentity.getString("identity_period");
                            //----------------------------------------------------------------------
                            String _address         = jsonObject.getString("address");
                            JSONObject objAddress   = new JSONObject(_address.toString());
                            JSONArray _dataarray    = objAddress.getJSONArray("data");

                            for (int i = 0; i < _dataarray.length(); i++) {
                                JSONObject objDataArray = _dataarray.getJSONObject(i);

                                String address_type         = objDataArray.getString("address_type");
                                String prospect_nbr         = objDataArray.getString("prospect_nbr");
                                String building_status_id   = objDataArray.getString("building_status_id");
                                String building_type        = objDataArray.getString("building_type");
                                String street               = objDataArray.getString("street");
                                String rt                   = objDataArray.getString("rt");
                                String rw                   = objDataArray.getString("rw");
                                String no_rumah             = objDataArray.getString("no_rumah");
                                String kelurahan            = objDataArray.getString("kelurahan");
                                String kecamatan            = objDataArray.getString("kecamatan");
                                String kota                 = objDataArray.getString("kota");
                                String province             = objDataArray.getString("province");
                                String zipcode              = objDataArray.getString("zipcode");
                                String created_by           = objDataArray.getString("created_by");
                                String created_date         = objDataArray.getString("created_date");
                                String last_upd_by          = objDataArray.getString("last_upd_by");
                                String last_upd_date        = objDataArray.getString("last_upd_date");

                                dbAdapterProfileAddress = new TableProfileAddressAdapter(context);
                                dbAdapterProfileAddress.insertData(new TableProfileAddress(), address_type,
                                        prospect_nbr, building_status_id, building_type, street, rt, rw,
                                        no_rumah, kelurahan, kecamatan, kota, province, zipcode, created_by,
                                        created_date, last_upd_by, last_upd_date);
                            }
                            //----------------------------------------------------------------------
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_status) {
                    statusdata.setText("Finish");
                    statusdata.setVisibility(View.GONE);
                    //------------------------------------------------------------------------------
                    no_ticket.setText(_ticket_number);
                    tgl_daftar.setText(js_register_date);
                    kartu_identitas.setText(js_identity_type);
                    no_identitas.setText(js_identity_nbr);
                    nama_depan.setText(js_first_name);
                    nama_tengah.setText(js_middle_name);
                    nama_belakang.setText(js_last_name);
                    jenis_kelamin.setText(js_gender);

                    if (js_religion.toString().equalsIgnoreCase("1")){
                        agama.setText("Islam");

                    } else if (js_religion.toString().equalsIgnoreCase("2")){
                        agama.setText("Kristen Protestan");

                    } else if (js_religion.toString().equalsIgnoreCase("3")){
                        agama.setText("Katolik");

                    } else if (js_religion.toString().equalsIgnoreCase("4")){
                        agama.setText("Hindu");

                    } else if (js_religion.toString().equalsIgnoreCase("5")){
                        agama.setText("Buddha");

                    } else if (js_religion.toString().equalsIgnoreCase("6")) {
                        agama.setText("Konghucu");
                    }else {
                        agama.setText("null");
                    }

                    tempat_lahir.setText(js_tempat_lahir);
                    tgl_lahir.setText(js_tgl_lahir);
                    //------------------------------------------------------------------------------
                    String _idt_address_type = "-", _idt_prospect_nbr = "-", _idt_building_status_id = "-",
                            _idt_building_type = "-", _idt_street = "-", _idt_rt = "-", _idt_rw = "-",
                            _idt_no_rumah = "-", _idt_kelurahan = "-", _idt_kecamatan = "-", _idt_kota = "-",
                            _idt_province = "-", _idt_zipcode = "-", _idt_created_by = "-", _idt_created_date = "-",
                            _idt_last_upd_by = "-", _idt_last_upd_date = "-";

                    dbAdapterProfileAddress = new TableProfileAddressAdapter(context);
                    listAddrIdentitas       = dbAdapterProfileAddress.getDatabyCondition(context,
                            TableProfileAddress.fADDRESS_TYPE, "PRI");

                    for (int i = 0; i < listAddrIdentitas.size(); i++) {
                        TableProfileAddress item = listAddrIdentitas.get(0);
                        _idt_address_type       = item.getfADDRESS_TYPE();
                        _idt_prospect_nbr       = item.getfPROSPECT_NBR();
                        _idt_building_status_id = item.getfBUILDING_STATUS_ID();
                        _idt_building_type      = item.getfBUILDING_TYPE();
                        _idt_street             = item.getfSTREET();
                        _idt_rt                 = item.getfRT();
                        _idt_rw                 = item.getfRW();
                        _idt_no_rumah           = item.getfNO_RUMAH();
                        _idt_kelurahan          = item.getfKELURAHAN();
                        _idt_kecamatan          = item.getfKECAMATAN();
                        _idt_kota               = item.getfKOTA();
                        _idt_province           = item.getfPROVINCE();
                        _idt_zipcode            = item.getfZIPCODE();
                        _idt_created_by         = item.getfCREATED_BY();
                        _idt_created_date       = item.getfCREATED_DATE();
                        _idt_last_upd_by        = item.getfLAST_UPD_BY();
                        _idt_last_upd_date      = item.getfLAST_UPD_DATE();
                    }

                    idt_alamat.setText(_idt_street);
                    idt_normh.setText(_idt_no_rumah);
                    idt_RT.setText(_idt_rt);
                    idt_RW.setText(_idt_rw);
                    idt_provinsi.setText(_idt_province);
                    idt_kota.setText(_idt_kota);
                    idt_kecamatan.setText(_idt_kecamatan);
                    idt_kelurahan.setText(_idt_kelurahan);
                    idt_kodepos.setText(_idt_zipcode);
                    idt_masaberlaku.setText("-");
                    idt_tglpasang.setText(_idt_created_date);
                    idt_jampasang.setText(js_identity_period);
                    idt_tglkonfirm_start.setText("-");
                    idt_tglkonfirm_end.setText("-");
                    idt_jamkonfirm_start.setText("-");
                    idt_jamkonfirm_end.setText("-");
                    //------------------------------------------------------------------------------
                    String _inst_address_type = "-", _inst_prospect_nbr = "-", _inst_building_status_id = "-",
                            _inst_building_type = "-", _inst_street = "-", _inst_rt = "-", _inst_rw = "-",
                            _inst_no_rumah = "-", _inst_kelurahan = "-", _inst_kecamatan = "-", _inst_kota = "-",
                            _inst_province = "-", _inst_zipcode = "-", _inst_created_by = "-", _inst_created_date = "-",
                            _inst_last_upd_by = "-", _inst_last_upd_date = "-";

                    dbAdapterProfileAddress = new TableProfileAddressAdapter(context);
                    listAddrPemasangan      = dbAdapterProfileAddress.getDatabyCondition(context,
                            TableProfileAddress.fADDRESS_TYPE, "INS");

                    for (int i = 0; i < listAddrPemasangan.size(); i++) {
                        TableProfileAddress item = listAddrPemasangan.get(0);
                        _inst_address_type       = item.getfADDRESS_TYPE();
                        _inst_prospect_nbr       = item.getfPROSPECT_NBR();
                        _inst_building_status_id = item.getfBUILDING_STATUS_ID();
                        _inst_building_type      = item.getfBUILDING_TYPE();
                        _inst_street             = item.getfSTREET();
                        _inst_rt                 = item.getfRT();
                        _inst_rw                 = item.getfRW();
                        _inst_no_rumah           = item.getfNO_RUMAH();
                        _inst_kelurahan          = item.getfKELURAHAN();
                        _inst_kecamatan          = item.getfKECAMATAN();
                        _inst_kota               = item.getfKOTA();
                        _inst_province           = item.getfPROVINCE();
                        _inst_zipcode            = item.getfZIPCODE();
                        _inst_created_by         = item.getfCREATED_BY();
                        _inst_created_date       = item.getfCREATED_DATE();
                        _inst_last_upd_by        = item.getfLAST_UPD_BY();
                        _inst_last_upd_date      = item.getfLAST_UPD_DATE();
                    }
                    inst_alamat.setText(_inst_street);
                    inst_patokan.setText("-");
                    inst_provinsi.setText(_inst_province);
                    inst_kota.setText(_inst_kota);
                    inst_kecamatan.setText(_inst_kecamatan);
                    inst_kelurahan.setText(_inst_kelurahan);
                    inst_kodepos.setText(_inst_zipcode);
                    //------------------------------------------------------------------------------
                    String _bill_address_type = "-", _bill_prospect_nbr = "-", _bill_building_status_id = "-",
                            _bill_building_type = "-", _bill_street = "-", _bill_rt = "-", _bill_rw = "-",
                            _bill_no_rumah = "-", _bill_kelurahan = "-", _bill_kecamatan = "-", _bill_kota = "-",
                            _bill_province = "-", _bill_zipcode = "-", _bill_created_by = "-", _bill_created_date = "-",
                            _bill_last_upd_by = "-", _bill_last_upd_date = "-";

                    dbAdapterProfileAddress = new TableProfileAddressAdapter(context);
                    listAddrPenagihan       = dbAdapterProfileAddress.getDatabyCondition(context,
                            TableProfileAddress.fADDRESS_TYPE, "BIL");

                    for (int i = 0; i < listAddrPenagihan.size(); i++) {
                        TableProfileAddress item = listAddrPenagihan.get(0);
                        _bill_address_type       = item.getfADDRESS_TYPE();
                        _bill_prospect_nbr       = item.getfPROSPECT_NBR();
                        _bill_building_status_id = item.getfBUILDING_STATUS_ID();
                        _bill_building_type      = item.getfBUILDING_TYPE();
                        _bill_street             = item.getfSTREET();
                        _bill_rt                 = item.getfRT();
                        _bill_rw                 = item.getfRW();
                        _bill_no_rumah           = item.getfNO_RUMAH();
                        _bill_kelurahan          = item.getfKELURAHAN();
                        _bill_kecamatan          = item.getfKECAMATAN();
                        _bill_kota               = item.getfKOTA();
                        _bill_province           = item.getfPROVINCE();
                        _bill_zipcode            = item.getfZIPCODE();
                        _bill_created_by         = item.getfCREATED_BY();
                        _bill_created_date       = item.getfCREATED_DATE();
                        _bill_last_upd_by        = item.getfLAST_UPD_BY();
                        _bill_last_upd_date      = item.getfLAST_UPD_DATE();
                    }
                    bill_alamat.setText(_bill_street);
                    bill_provinsi.setText(_bill_province);
                    bill_kota.setText(_bill_kota);
                    bill_kecamatan.setText(_bill_kecamatan);
                    bill_kelurahan.setText(_bill_kelurahan);
                    bill_kodepos.setText(_bill_zipcode);
                    bill_tlprmh.setText(js_home_phone);
                    bill_nohp.setText(js_mobile_phone);
                    bill_email.setText(js_email);
                    bill_jenisrmh.setText(_bill_building_type);
                    bill_statusrmh.setText(_bill_building_status_id);
                    bill_pekerjaan.setText(js_work);
                    bill_penghasilan.setText(js_income+",-");
                } else {
                    statusdata.setText("Gagal terhubung ke server, silahkan dicoba kembali.");
                    statusdata.setVisibility(View.VISIBLE);
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "detail_inst_troub");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
