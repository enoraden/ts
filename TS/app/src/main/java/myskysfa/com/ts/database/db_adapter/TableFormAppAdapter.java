package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import myskysfa.com.ts.database.TableFormApp;
import myskysfa.com.ts.utils.DatabaseManager;

import java.util.List;


/**
 * Created by admin on 6/20/2016.
 */
public class TableFormAppAdapter {

    static private TableFormAppAdapter instance;
    private Dao dao;

    static public void init(Context ctx) {
        if (null == instance) {
            instance = new TableFormAppAdapter(ctx);
        }
    }

    static public TableFormAppAdapter getInstance() {
        return instance;
    }

    private DatabaseManager helper;

    public TableFormAppAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /**
     * Get All Data
     *
     * @return
     */
    public List<TableFormApp> getAllData() {
        List<TableFormApp> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFormAppsDAO().queryBuilder().query();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableFormApp> getDatabyCondition(String condition, Object param) {
        List<TableFormApp> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableFormApp.class);
            QueryBuilder<TableFormApp, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFormApp, Integer> where = queryBuilder.where();
            where.eq(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Get Data By ID
     *
     * @return
     */
    public List<TableFormApp> getDatalikeCondition(String condition, Object param) {
        List<TableFormApp> tblsatu = null;
        int count = 0;
        try {
            dao = helper.getDao(TableFormApp.class);
            QueryBuilder<TableFormApp, Integer> queryBuilder = dao.queryBuilder();
            Where<TableFormApp, Integer> where = queryBuilder.where();
            where.like(condition, param);
            count++;

            if (count > 0) {
                tblsatu = queryBuilder.query();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tblsatu;
    }

    /**
     * Method for count data by condition
     */
    public Long getDataCount(String condition, String param) {
        Long usuarios = 0L;
        try {
            dao = helper.getDao(TableFormApp.class);
            QueryBuilder queryBuilder = dao.queryBuilder();
            queryBuilder.setCountOf(true);
            queryBuilder.setWhere(queryBuilder.where().eq(condition, param));
            usuarios = dao.countOf(queryBuilder.prepare());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return usuarios;
    }

    /**
     * Insert Data
     */
    public void insertData(TableFormApp tbl,  String no_ticket,
                           String values_package, String values_payment, String values_lkt,String values_checklist,
                            String values_signature,  String form_status
                           ) {
        try {
            tbl.setNO_TICKET(no_ticket);
            tbl.setVALUES_PAYMENT(values_payment);
            tbl.setVALUES_PACKAGE(values_package);
            tbl.setVALUES_SIGNATURE(values_signature);
            tbl.setVALUES_LKT(values_lkt);
            tbl.setVALUES_CHECKLIST(values_checklist);
            tbl.setFORM_STATUS(form_status);
            getHelper().getTableFormAppsDAO().create(tbl);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    * Update by Condition
    *  column = nama coloumn
    *  object value =
    *  string condition =
    *  object param =
    */
    public void updatePartial(Context context, String column, Object value, String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFormApp.class);
            UpdateBuilder<TableFormApp, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Update All
     */
    public void updateAll(Context context, String no_ticket, String values_package, String values_payment,
                            String values_lkt,String values_checklist,String values_signature, String form_status,
                          String condition, Object param) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFormApp.class);
            UpdateBuilder<TableFormApp, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableFormApp.fNO_TICKET, no_ticket);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_PACKAGE, values_package);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_PAYMENT, values_payment);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_LKT, values_lkt);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_SIGNATURE, values_signature);
            updateBuilder.updateColumnValue(TableFormApp.fVALUES_CHECKLIST, values_checklist);
            updateBuilder.updateColumnValue(TableFormApp.fFORM_STATUS, form_status);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Delete By Conditition
     */
    public void deleteBy(Context context, String condition, Object value) {
        try {
            helper = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao = helper.getDao(TableFormApp.class);
            DeleteBuilder<TableFormApp, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableFormApp> tblsatu = null;
        try {
            tblsatu = getHelper().getTableFormAppsDAO().queryForAll();
            getHelper().getTableFormAppsDAO().delete(tblsatu);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
