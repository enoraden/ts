package myskysfa.com.ts.adapter.master;

import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableMasterPackage;

import java.util.ArrayList;
import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterPackageAdapter extends RecyclerView.Adapter<MasterPackageAdapter.ViewHolder> {
    public static List<TableMasterPackage> itemData = new ArrayList<>();
    private View itemLayoutView;

    public MasterPackageAdapter(List<TableMasterPackage> list) {
        this.itemData   = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView          = LayoutInflater.from(parent.getContext()).inflate(R.layout.master_item, parent, false);
        ViewHolder viewHolder   = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableMasterPackage item = itemData.get(position);

        if (item != null) {
            holder.icon.setText(item.getfPRODUCT_NAME().substring(0, 1));
            holder.id.setText(item.getfPRODUCT_NAME());
            holder.detail.setText("Rp. "+String.valueOf(item.getfPRICE()));
        }
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView icon, id, detail, date;

        public ViewHolder(View itemView) {
            super(itemView);

            icon        = (TextView) itemLayoutView.findViewById(R.id.masteritem_icon);
            id          = (TextView) itemLayoutView.findViewById(R.id.masteritem_id);
            detail      = (TextView) itemLayoutView.findViewById(R.id.masteritem_detail);
            date        = (TextView) itemLayoutView.findViewById(R.id.masteritem_date);

            date.setVisibility(View.GONE);
            detail.setTextColor(Color.parseColor("#14386C"));
        }
    }
}
