package myskysfa.com.ts.main.rejoin;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableFormRejoin;
import myskysfa.com.ts.database.TableTaskList;
import myskysfa.com.ts.database.db_adapter.TableFormRejoinAdapter;
import myskysfa.com.ts.database.db_adapter.TableTaskListAdapter;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/05/2016.
|---------------------------------------------------------------------------------------------------
*/
public class BAP extends Fragment {
    private String form_id, id_retrieval, prospect_nbr, customer_nbr, ticket_nbr, job_type,
            schedule_penarikan, status_penarikan, username;

    private EditText vc, dsd, lnb, odu;
    private TextView ref_vc, ref_dsd, ref_lnb, ref_odu;
    private ImageView btn_vc, btn_dsd, btn_lnb, btn_odu;
    private Button save;

    private final String KEY_HW_VC  = "VC";
    private final String KEY_HW_LNB = "LNB";
    private final String KEY_HW_DSD = "DSD";
    private final String KEY_HW_ODU = "ODU";

    private IntentIntegrator integrator;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.r_bap, container, false);

        loadResources(viewGroup);
        getDataTaskList();

        return viewGroup;
    }

    private void loadResources(ViewGroup root) {
        vc      = (EditText) root.findViewById(R.id.bap_vc);
        ref_vc  = (TextView) root.findViewById(R.id.ref_vc);
        btn_vc  = (ImageView) root.findViewById(R.id.bapbtn_vc);

        dsd     = (EditText) root.findViewById(R.id.bap_dsd);
        ref_dsd = (TextView) root.findViewById(R.id.ref_dsd);
        btn_dsd = (ImageView) root.findViewById(R.id.bapbtn_dsd);

        lnb     = (EditText) root.findViewById(R.id.bap_lnb);
        ref_lnb = (TextView) root.findViewById(R.id.ref_lnb);
        btn_lnb = (ImageView) root.findViewById(R.id.bapbtn_lnb);

        odu     = (EditText) root.findViewById(R.id.bap_odu);
        ref_odu = (TextView) root.findViewById(R.id.ref_odu);
        btn_odu = (ImageView) root.findViewById(R.id.bapbtn_odu);

        save    = (Button) root.findViewById(R.id.bap_save);

        ref_vc.setVisibility(View.GONE);
        ref_dsd.setVisibility(View.GONE);
        ref_lnb.setVisibility(View.GONE);
        ref_odu.setVisibility(View.GONE);

        btn_vc.setOnClickListener(vc_listener);
        btn_dsd.setOnClickListener(dsd_listener);
        btn_lnb.setOnClickListener(lnb_listener);
        btn_odu.setOnClickListener(odu_listener);
        save.setOnClickListener(save_listener);
    }

    private void getDataTaskList() {
        TableTaskListAdapter dbAdapterTaskList  = new TableTaskListAdapter(getActivity());
        List<TableTaskList> listTaskList        = dbAdapterTaskList.getAllData(getActivity());

        if (listTaskList.size() > 0) {
            form_id             = listTaskList.get(listTaskList.size() - 1).getfFORM_ID();
            id_retrieval        = listTaskList.get(listTaskList.size() - 1).getfID_RETRIEVAL();
            prospect_nbr        = listTaskList.get(listTaskList.size() - 1).getfPROSPECT_NBR();
            customer_nbr        = listTaskList.get(listTaskList.size() - 1).getfCUSTOMER_NBR();
            ticket_nbr          = listTaskList.get(listTaskList.size() - 1).getfTICKET_NBR();
            job_type            = listTaskList.get(listTaskList.size() - 1).getfJOB_TYPE();
            schedule_penarikan  = listTaskList.get(listTaskList.size() - 1).getfSCHEDULE_PENARIKAN();
            status_penarikan    = listTaskList.get(listTaskList.size() - 1).getfSTATUS_PENARIKAN();
            username            = listTaskList.get(listTaskList.size() - 1).getfUSERNAME();
        }
    }

    private View.OnClickListener vc_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            integrator = new IntentIntegrator(getActivity());
            integrator.addExtra(Intent.EXTRA_TEXT, KEY_HW_VC);
            integrator.initiateScan();
        }
    };

    private View.OnClickListener dsd_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            integrator = new IntentIntegrator(getActivity());
            integrator.addExtra(Intent.EXTRA_TEXT, KEY_HW_DSD);
            integrator.initiateScan();
        }
    };

    private View.OnClickListener lnb_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            integrator = new IntentIntegrator(getActivity());
            integrator.addExtra(Intent.EXTRA_TEXT, KEY_HW_LNB);
            integrator.initiateScan();
        }
    };

    private View.OnClickListener odu_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            integrator = new IntentIntegrator(getActivity());
            integrator.addExtra(Intent.EXTRA_TEXT, KEY_HW_ODU);
            integrator.initiateScan();
        }
    };

    private View.OnClickListener save_listener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (vc.getText().toString()== null || vc.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "Silahkan input no VC!", Toast.LENGTH_SHORT).show();

            } else if (dsd.getText().toString()== null || dsd.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "Silahkan input no DSD!", Toast.LENGTH_SHORT).show();

            } else if (lnb.getText().toString()== null || lnb.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "Silahkan input no LNB!", Toast.LENGTH_SHORT).show();

            } else if (odu.getText().toString()== null || odu.getText().toString().equals("")) {
                Toast.makeText(getActivity(), "Silahkan input no DISH!", Toast.LENGTH_SHORT).show();

            } else {
                TableFormRejoinAdapter dbAdapterFormRejoin;
                List<TableFormRejoin> listFormRejoin;
                dbAdapterFormRejoin = new TableFormRejoinAdapter(getActivity());
                listFormRejoin      = dbAdapterFormRejoin.getDatabyCondition(getActivity(), TableFormRejoin.fFORMID, form_id);

                if (listFormRejoin.size() == 0) {
                    dbAdapterFormRejoin.deleteAllData();
                    dbAdapterFormRejoin.insertData(new TableFormRejoin(), form_id, id_retrieval, vc.getText().toString(),
                            dsd.getText().toString(), lnb.getText().toString(), odu.getText().toString(),
                            "", "", "", "", "", "", "");
                    Toast.makeText(getActivity(), "Data tersimpan.", Toast.LENGTH_SHORT).show();

                } else {
                    String _form_id = "", _id_retrieval = "", _vc = "", _dsd = "", _lnb = "", _odu = "",
                            _signature_path = "", _latitude = "", _longitude = "", _result_code = "",
                            _result_datetime = "", _result_note = "", _image_path = "";

                    for (int i = 0; i < listFormRejoin.size(); i++) {
                        TableFormRejoin item = listFormRejoin.get(0);
                        _form_id            = item.getfFORMID();
                        _id_retrieval       = item.getfID_RETRIEVAL();
                        _vc                 = item.getfVC();
                        _dsd                = item.getfDSD();
                        _lnb                = item.getfLNB();
                        _odu                = item.getfODU();
                        _signature_path     = item.getfSIGNATURE_PATH();
                        _latitude           = item.getfLATITUDE();
                        _longitude          = item.getfLONGITUDE();
                        _result_code        = item.getfRESULT_CODE();
                        _result_datetime    = item.getfRESULT_DATETIME();
                        _result_note        = item.getfRESULT_NOTE();
                        _image_path         = item.getfIMAGE_PATH();
                    }

                    dbAdapterFormRejoin.updateAllData(getActivity(), _form_id, _id_retrieval, vc.getText().toString(),
                            dsd.getText().toString(), lnb.getText().toString(), odu.getText().toString(),
                            _signature_path, _latitude, _longitude, _result_code, _result_datetime,
                            _result_note, _image_path, TableFormRejoin.fFORMID, form_id);

                    Toast.makeText(getActivity(), "Data BAP berhasil diupdate.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);

        if (scanResult != null) {
            Map<String, ?> map  = integrator.getMoreExtras();
            Set keys            = map.keySet();
            String value        ="";

            for (Iterator i = keys.iterator(); i.hasNext();) {
                String key  = (String) i.next();
                value       = (String) map.get(key);
            }

            String result = scanResult.getContents();

            if (value.equalsIgnoreCase(KEY_HW_VC)) {
                if (result!=null && result.length()<14) {
                    String a = result.substring(0, 2);
                    String b = result.substring(2, result.length());
                    StringBuilder sb = new StringBuilder();
                    sb.append(a);
                    sb.append("0000");
                    sb.append(b);
                    result = sb.toString();
                }
                vc.setText(result);

            } else if (value.equalsIgnoreCase(KEY_HW_DSD)) {
                dsd.setText(result);

            } else if (value.equalsIgnoreCase(KEY_HW_LNB)) {
                lnb.setText(result);

            } else if (value.equalsIgnoreCase(KEY_HW_ODU)) {
                odu.setText(result);

            } else {
                Toast.makeText(getActivity(), "Silahkan ulangi!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}