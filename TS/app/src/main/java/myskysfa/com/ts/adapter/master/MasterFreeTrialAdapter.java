package myskysfa.com.ts.adapter.master;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableFreeTrial;

import java.util.ArrayList;
import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterFreeTrialAdapter extends RecyclerView.Adapter<MasterFreeTrialAdapter.ViewHolder> {
    public static List<TableFreeTrial> itemData = new ArrayList<>();
    private View itemLayoutView;

    public MasterFreeTrialAdapter(List<TableFreeTrial> list) {
        this.itemData   = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView          = LayoutInflater.from(parent.getContext()).inflate(R.layout.master_item, parent, false);
        ViewHolder viewHolder   = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableFreeTrial item = itemData.get(position);

        if (item != null) {
            holder.id.setText(item.getfPROSPECT_NBR());
            holder.detail.setText(item.getfALAMAT());
            holder.date.setText(item.getfPRODUCT_NAME()+" - "+item.getfCREATED_DATE());
        }
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView icon, id, detail, date;

        public ViewHolder(View itemView) {
            super(itemView);
            icon        = (TextView) itemLayoutView.findViewById(R.id.masteritem_icon);
            id          = (TextView) itemLayoutView.findViewById(R.id.masteritem_id);
            detail      = (TextView) itemLayoutView.findViewById(R.id.masteritem_detail);
            date        = (TextView) itemLayoutView.findViewById(R.id.masteritem_date);

            icon.setText("");
            icon.setBackgroundResource(R.mipmap.icm_ft_dtd);
        }
    }
}