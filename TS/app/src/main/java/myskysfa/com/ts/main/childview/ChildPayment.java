package myskysfa.com.ts.main.childview;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.List;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.MasterAPI;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/16/2016.
|---------------------------------------------------------------------------------------------------
*/
public class ChildPayment {
    private static Context context;
    private Utils utils;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private TextView statusdata;
    private TextView no_ticket, jumlahbayar, carabayar, namabank, norekening, cardtype, cardno, nama,
            masaberlaku, periodebayar, tglbayar, remark, amount;
    private LinearLayout layoutnorekening, layoutnamabank, layoutcardtype, chdpyt_layoutcardno;

    public ChildPayment(Context _context) {
        this.context = _context;
    }

    public void paymentView(View root) {
        loadResorce(root);
        loadListener();
        new getChildPayment().execute();
    }
    private void loadResorce(View root) {
        statusdata      = (TextView) root.findViewById(R.id.chdpyt_statusdata);
        //------------------------------------------------------------------------------------------
        no_ticket       = (TextView) root.findViewById(R.id.chdpyt_no_ticket);
        jumlahbayar     = (TextView) root.findViewById(R.id.chdpyt_jumlahbayar);
        carabayar       = (TextView) root.findViewById(R.id.chdpyt_carabayar);
        norekening      = (TextView) root.findViewById(R.id.chdpyt_norekening);
        namabank        = (TextView) root.findViewById(R.id.chdpyt_namabank);
        cardtype        = (TextView) root.findViewById(R.id.chdpyt_cardtype);
        cardno          = (TextView) root.findViewById(R.id.chdpyt_cardno);
        nama            = (TextView) root.findViewById(R.id.chdpyt_nama);
        masaberlaku     = (TextView) root.findViewById(R.id.chdpyt_masaberlaku);
        periodebayar    = (TextView) root.findViewById(R.id.chdpyt_periodebayar);
        tglbayar        = (TextView) root.findViewById(R.id.chdpyt_tglbayar);
        remark          = (TextView) root.findViewById(R.id.chdpyt_remark);
        amount          = (TextView) root.findViewById(R.id.chdpyt_amount);

        layoutnorekening    = (LinearLayout) root.findViewById(R.id.chdpyt_layoutnorekening);
        layoutnamabank      = (LinearLayout) root.findViewById(R.id.chdpyt_layoutnamabank);
        layoutcardtype      = (LinearLayout) root.findViewById(R.id.chdpyt_layoutcardtype);
        chdpyt_layoutcardno = (LinearLayout) root.findViewById(R.id.chdpyt_layoutcardno);
    }

    private void loadListener() {
        no_ticket.setText("-");
        jumlahbayar.setText("0");
        carabayar.setText("-");
        norekening.setText("-");
        namabank.setText("-");
        cardtype.setText("-");
        cardno.setText("-");
        nama.setText("-");
        masaberlaku.setText("-");
        periodebayar.setText("-");
        tglbayar.setText("-");
        remark.setText("-");
        amount.setText("-");
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data Child Payment Server
    |-----------------------------------------------------------------------------------------------
    */
    private class getChildPayment extends AsyncTask<String, String, String> {
        private Boolean _status;
        private String _imei;
        private int _signal;

        private SharedPreferences preferences;
        private String _ticket_number;

        private String js_prospect_nbr, js_period, js_method, js_amount, js_remark;
        private String js_account_number, js_account_name, js_bank, js_expiry_date;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils       = new Utils(context);
            preferences = context.getSharedPreferences(context.getString(R.string.detail_id), Context.MODE_PRIVATE);
            _signal     = utils.checkSignal();
            _imei       = utils.getIMEI();
            _status     = false;

            statusdata.setText("Loading . . .");
            statusdata.setVisibility(View.VISIBLE);
        }


        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(context);
                listLogLogin        = dbAdapterLogLogin.getAllData(context);

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_DETAIL_PROFILE;
                    _ticket_number      = preferences.getString("ticket_number", null);
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl    = _url + "/" + _ticket_number;

                    String _response = ConnectionManager.requestDataMaster(_paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status = jsonObject.getBoolean("status");

                        if (_status) {
                            String _data        = jsonObject.getString("data");
                            JSONObject objData  = new JSONObject(_data.toString());

                            js_prospect_nbr     = objData.getString("prospect_nbr");
                            //----------------------------------------------------------------------
                            String _payment         = jsonObject.getString("payment");
                            JSONObject objPayment   = new JSONObject(_payment.toString());

                            js_period   = objPayment.getString("period");
                            js_method   = objPayment.getString("method");
                            js_amount   = objPayment.getString("amount");
                            js_remark   = objPayment.getString("remark");

                            String _methode_desc        = objPayment.getString("methode_desc");
                            JSONObject objMethodeDesc   = new JSONObject(_methode_desc.toString());

                            js_account_number   = objMethodeDesc.getString("account_number");
                            js_account_name     = objMethodeDesc.getString("account_name");
                            js_bank             = objMethodeDesc.getString("bank");
                            js_expiry_date      = objMethodeDesc.getString("expiry_date");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }


        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_status) {
                    statusdata.setText("Finish");
                    statusdata.setVisibility(View.GONE);
                    //------------------------------------------------------------------------------
                    no_ticket.setText(_ticket_number);
                    jumlahbayar.setText("0");
                    carabayar.setText(js_method);

                    if (js_method.toString().equalsIgnoreCase("KARTU KREDIT")) {
                        layoutnorekening.setVisibility(View.GONE);
                        layoutnamabank.setVisibility(View.GONE);
                    } else if (js_method.toString().equalsIgnoreCase("AUTODEBET REKENING")) {
                        layoutcardtype.setVisibility(View.GONE);
                        chdpyt_layoutcardno.setVisibility(View.GONE);
                        layoutnorekening.setVisibility(View.GONE);
                        layoutnamabank.setVisibility(View.GONE);
                    } else if (js_method.toString().equalsIgnoreCase("ATM")) {
                        layoutcardtype.setVisibility(View.GONE);
                        chdpyt_layoutcardno.setVisibility(View.GONE);
                    } else if (js_method.toString().equalsIgnoreCase("CASH")) {
                        layoutcardtype.setVisibility(View.GONE);
                        chdpyt_layoutcardno.setVisibility(View.GONE);
                        layoutnorekening.setVisibility(View.GONE);
                        layoutnamabank.setVisibility(View.GONE);
                    } else {
                        layoutcardtype.setVisibility(View.GONE);
                        chdpyt_layoutcardno.setVisibility(View.GONE);
                        layoutnorekening.setVisibility(View.GONE);
                        layoutnamabank.setVisibility(View.GONE);
                    }

                    norekening.setText(js_account_number);
                    namabank.setText(js_bank);
                    cardtype.setText("-");
                    cardno.setText("-");
                    nama.setText("-");
                    masaberlaku.setText(js_expiry_date);
                    periodebayar.setText(js_period);
                    tglbayar.setText("-");
                    remark.setText(js_remark);
                    amount.setText(js_amount+",-");
                } else {
                    statusdata.setText("Gagal terhubung ke server, silahkan dicoba kembali.");
                    statusdata.setVisibility(View.VISIBLE);
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "detail_inst_troub");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
