package myskysfa.com.ts.main.instalasi;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Document;

import java.util.ArrayList;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.ListMainDetailAdapter;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.PermissionUtils;
import myskysfa.com.ts.utils.Utils;
import myskysfa.com.ts.widget.GMapDirection;
import myskysfa.com.ts.widget.WorkaroundMapFragment;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class InstalasiDetail extends Fragment implements OnMapReadyCallback {
    private GMapDirection md;
    private GoogleMap mMap;
    private SupportMapFragment fragment;

    private ScrollView scrollview;
    private LinearLayout layoutmap;
    private ExpandableListView expandlist;
    private FloatingActionButton instdetail_fab;
    private ListMainDetailAdapter adapter;
    private String[] groupsItem         = {"Data Profile", "Data Emergency", "Data Payment"};
    private String[][] childParam       = {{"profile"}, {"emergency"}, {"payment"}};
    private int lastExpandedPosition    = -1;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.list_m_detail, container, false);

        loadResources(viewGroup);
        loadMap();
        loadListener();

        setGroupIndicatorToRight();

        return viewGroup;
    }

    private void loadResources(ViewGroup root) {
        scrollview      = (ScrollView) root.findViewById(R.id.listmdetail_scrollview);
        layoutmap       = (LinearLayout) root.findViewById(R.id.listmdetail_layoutmap);
        expandlist      = (ExpandableListView) root.findViewById(R.id.listmdetail_expandlist);
        instdetail_fab  = (FloatingActionButton) root.findViewById(R.id.listmdetail_fab);
        adapter         = new ListMainDetailAdapter(getActivity(), groupsItem, childParam);

        //layoutmap.setVisibility(View.GONE);
        expandlist.setAdapter(adapter);
        instdetail_fab.setOnClickListener(_instdetail_fab);
    }

    private void loadMap() {
        if (mMap == null) {
            FragmentManager fm = getChildFragmentManager();
            fragment = (WorkaroundMapFragment) fm.findFragmentById(R.id.listmdetail_map);

            if (fragment == null) {
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.listmdetail_map, fragment).commit();
            }

            ((WorkaroundMapFragment) fm.findFragmentById(R.id.listmdetail_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
                @Override
                public void onTouch() {
                    scrollview.requestDisallowInterceptTouchEvent(true);
                }
            });

            fragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap    = googleMap;
        md      = new GMapDirection();
        enableMyLocation();
        new getDirection().execute();
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(getActivity(), 1, Manifest.permission.ACCESS_FINE_LOCATION, true);

        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }
    }

    private class getDirection extends AsyncTask<String, String, String> {
        private Utils utils;
        private LatLng position, direction;
        private double latitude, longitude;
        private Document doc;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            utils = new Utils(getActivity());
            utils.getLatLong();
            latitude    = utils.latitude;
            longitude   = utils.longitude;

            utils.showDialogLoading("Loading . . .");

            Log.i("latlong", String.valueOf(latitude)+","+String.valueOf(longitude));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                if (latitude == 0 || longitude == 0) {
                    doc = null;
                } else {
                    position    = new LatLng(latitude, longitude);
                    direction   = new LatLng(-6.227622, 106.796916);
                    doc         = md.getDocument(position, direction, GMapDirection.MODE_DRIVING);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (utils.progressdialog.isShowing())
                    utils.progressdialog.dismiss();

                if (doc != null) {
                    ArrayList<LatLng> directionPoint    = md.getDirection(doc);
                    PolylineOptions rectLine            = new PolylineOptions().width(3).color(Color.RED);

                    for (int i = 0; i < directionPoint.size(); i++) {
                        rectLine.add(directionPoint.get(i));
                    }

                    mMap.addMarker(new MarkerOptions().position(position).title("Your Position"));
                    mMap.addMarker(new MarkerOptions().position(direction).title("direction"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(position.latitude, position.longitude), 17.0f));
                    //mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
                    mMap.addPolyline(rectLine);
                } else {
                    Utils utils = new Utils(getActivity());
                    utils.showErrorDialog("Koneksi Error", "Latitude dan Longitude kosong.\n");
                    Log.d("ErrorJSON", "GoogleMap");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadListener() {
        expandlist.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                ConnectionDetector connDetect   = new ConnectionDetector(getActivity());
                Boolean isInternetPresent       = connDetect.isConnectingToInternet();

                if (isInternetPresent) {
                    if (lastExpandedPosition != -1 && groupPosition != lastExpandedPosition) {
                        expandlist.collapseGroup(lastExpandedPosition);
                    }

                    lastExpandedPosition = groupPosition;
                } else {
                    expandlist.collapseGroup(groupPosition);
                    Utils utils = new Utils(getActivity());
                    utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, " +
                            "Silahkan cek koneksi internet anda dan dicoba kembali.\n");
                }
            }
        });

        expandlist.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {
            }
        });

        expandlist.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        instdetail_fab.show();
                        break;
                    case MotionEvent.ACTION_UP:
                        instdetail_fab.show();
                        break;
                    case MotionEvent.ACTION_MOVE:
                        instdetail_fab.hide();
                        break;
                }
                return false;
            }
        });
    }

    private View.OnClickListener _instdetail_fab = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent(getActivity(), InstalasiTabLayout.class);
            startActivity(intent);
        }
    };

    private void setGroupIndicatorToRight() {
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;

        if (android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.JELLY_BEAN_MR2) {
            expandlist.setIndicatorBounds(width - GetPixelFromDips(40), width - GetPixelFromDips(0));

        } else {
            expandlist.setIndicatorBoundsRelative(width - GetPixelFromDips(40), width - GetPixelFromDips(0));
        }
    }

    public int GetPixelFromDips(float pixels) {
        final float scale = getResources().getDisplayMetrics().density;
        return (int) (pixels * scale + 0.5f);
    }
}