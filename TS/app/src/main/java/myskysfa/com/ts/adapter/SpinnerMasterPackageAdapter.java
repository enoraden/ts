package myskysfa.com.ts.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import myskysfa.com.ts.database.TableMasterPackage;
import myskysfa.com.ts.utils.Utils;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/8/2016.
|---------------------------------------------------------------------------------------------------
*/
public class SpinnerMasterPackageAdapter extends ArrayAdapter<TableMasterPackage> {
    private Context context;
    private List<TableMasterPackage> itemsStatus;
    private Utils utils;

    public SpinnerMasterPackageAdapter(Context _context, int textViewResourceId, List<TableMasterPackage> _itemsStatus) {
        super(_context, textViewResourceId, _itemsStatus);
        this.context        = _context;
        this.itemsStatus    = _itemsStatus;
        utils               = new Utils(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView txtSpinner = new TextView(context);
        txtSpinner.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        txtSpinner.setTextSize(16);
        txtSpinner.setTextColor(Color.BLACK);
        txtSpinner.setText(itemsStatus.get(position).getfPRODUCT_NAME());

        return txtSpinner;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView txtSpinner = (TextView) super.getView(position, convertView, parent);
        txtSpinner.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        txtSpinner.setTextSize(16);
        txtSpinner.setPadding(10, 5, 10, 5);
        txtSpinner.setTextColor(Color.BLACK);
        txtSpinner.setText(itemsStatus.get(position).getfPRODUCT_NAME());

        return txtSpinner;
    }
}