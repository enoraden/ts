package myskysfa.com.ts.adapter.master;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableMasterPromo;

import java.util.ArrayList;
import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterPromoAdapter extends RecyclerView.Adapter<MasterPromoAdapter.ViewHolder> {
    public static List<TableMasterPromo> itemData = new ArrayList<>();
    private View itemLayoutView;

    public MasterPromoAdapter(List<TableMasterPromo> list) {
        this.itemData   = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        itemLayoutView          = LayoutInflater.from(parent.getContext()).inflate(R.layout.master_item, parent, false);
        ViewHolder viewHolder   = new ViewHolder(itemLayoutView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final TableMasterPromo item = itemData.get(position);

        if (item != null) {
            holder.icon.setText(item.getfPROMOTION_CODE());
            holder.id.setText(item.getfPROMOTION_ID());
            holder.detail.setText(item.getfPROMOTION_DESC());
            holder.date.setText(item.getfEND_DATE());
        }
    }

    @Override
    public int getItemCount() {
        return itemData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView icon, id, detail, date;

        public ViewHolder(View itemView) {
            super(itemView);

            icon        = (TextView) itemLayoutView.findViewById(R.id.masteritem_icon);
            id          = (TextView) itemLayoutView.findViewById(R.id.masteritem_id);
            detail      = (TextView) itemLayoutView.findViewById(R.id.masteritem_detail);
            date        = (TextView) itemLayoutView.findViewById(R.id.masteritem_date);
        }
    }
}