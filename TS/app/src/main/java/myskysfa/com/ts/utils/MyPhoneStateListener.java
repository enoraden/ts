package myskysfa.com.ts.utils;

import android.telephony.PhoneStateListener;
import android.telephony.SignalStrength;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MyPhoneStateListener extends PhoneStateListener {

    private int signalStrengthValue;
    private static int signal;

    @Override
    public void onSignalStrengthsChanged(SignalStrength signalStrength) {
        super.onSignalStrengthsChanged(signalStrength);

        //Range 0 s/d 31, 99 tidak ada signal
        if (signalStrength.isGsm()) {
            if (signalStrength.getGsmSignalStrength() != 99){
                signalStrengthValue = signalStrength.getGsmSignalStrength();
                signalStrengthValue = (2 * signalStrengthValue) - 113; // --> dBm
            }
            else{
                signalStrengthValue = signalStrength.getGsmSignalStrength();
                signalStrengthValue = (2 * signalStrengthValue) - 113; // --> dBm
            }

        } else {
            signalStrengthValue = signalStrength.getCdmaDbm();
        }

        signal = signalStrengthValue;
    }

    public int setSignal() {
        return signal;
    }
}