package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/7/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "form_rejoin")
public class TableFormRejoin {
    public static final String TABLE_NAME       = "form_rejoin";

    public static final String fFORMID          = "formid";
    public static final String fID_RETRIEVAL    = "id_retrieval";
    public static final String fVC              = "vc";
    public static final String fDSD             = "dsd";
    public static final String fLNB             = "lnb";
    public static final String fODU             = "odu";
    public static final String fSIGNATURE_PATH  = "signature_path";
    public static final String fLATITUDE        = "latitude";
    public static final String fLONGITUDE       = "longitude";
    public static final String fRESULT_CODE     = "result_code";
    public static final String fRESULT_DATETIME = "result_datetime";
    public static final String fRESULT_NOTE     = "result_note";
    public static final String fIMAGE_PATH      = "image_path";

    @DatabaseField(id = true)
    private String formid;

    @DatabaseField
    private String id_retrieval, vc, dsd, lnb, odu, signature_path, latitude, longitude, result_code,
            result_datetime, result_note, image_path;

    public void setfFORMID(String formid) {
        this.formid = formid;
    }
    public String getfFORMID() {
        return formid;
    }

    public void setfID_RETRIEVAL(String id_retrieval) {
        this.id_retrieval = id_retrieval;
    }
    public String getfID_RETRIEVAL() {
        return id_retrieval;
    }

    public void setfVC(String vc) {
        this.vc = vc;
    }
    public String getfVC() {
        return vc;
    }

    public void setfDSD(String dsd) {
        this.dsd = dsd;
    }
    public String getfDSD() {
        return dsd;
    }

    public void setfLNB(String lnb) {
        this.lnb = lnb;
    }
    public String getfLNB() {
        return lnb;
    }

    public void setfODU(String odu) {
        this.odu = odu;
    }
    public String getfODU() {
        return odu;
    }

    public void setfSIGNATURE_PATH(String signature_path) {
        this.signature_path = signature_path;
    }
    public String getfSIGNATURE_PATH() {
        return signature_path;
    }

    public void setfLATITUDE(String latitude) {
        this.latitude = latitude;
    }
    public String getfLATITUDE() {
        return latitude;
    }

    public void setfLONGITUDE(String longitude) {
        this.longitude = longitude;
    }
    public String getfLONGITUDE() {
        return longitude;
    }

    public void setfRESULT_CODE(String result_code) {
        this.result_code = result_code;
    }
    public String getfRESULT_CODE() {
        return result_code;
    }

    public void setfRESULT_DATETIME(String result_datetime) {
        this.result_datetime = result_datetime;
    }
    public String getfRESULT_DATETIME() {
        return result_datetime;
    }

    public void setfRESULT_NOTE(String result_note) {
        this.result_note = result_note;
    }
    public String getfRESULT_NOTE() {
        return result_note;
    }

    public void setfIMAGE_PATH(String image_path) {
        this.image_path = image_path;
    }
    public String getfIMAGE_PATH() {
        return image_path;
    }
}
