package myskysfa.com.ts.main.sliding;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableFormApp;
import myskysfa.com.ts.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.ts.utils.PermissionUtils;
import myskysfa.com.ts.utils.Utils;

import org.json.JSONArray;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


/**
 * Created by admin on 6/22/2016.
 */
public class Status extends Fragment implements GoogleMap.OnMyLocationButtonClickListener, OnMapReadyCallback,
        ActivityCompat.OnRequestPermissionsResultCallback, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnMarkerDragListener{

        private static Context _context;
    private LinearLayout  paketLayout, paymentLayout, lktLayout, checklistLayout,signatureLayout;
    private GoogleMap googleMap;
    private ScrollView mScrollView;
    private Utils utils;
    public static Double lat, lon;
    private Location latLng;
    private Marker marker;
    private boolean mPermissionDenied = false;
    // Status TextView
    private TextView paket_status_txt , payment_status_txt,lkt_status_txt,checklist_status_txt,signature_status_txt;

    public static String addressLine;
    private TextView txtAddressLine, _latitude, _longitude, _address, pKet,
            eKet ;
    private ImageView image_paket_flag, image_payment_flag, image_lkt_flag, image_checklist_flag,image_signature_flag;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private Menu menu;
    private MenuItem alert;
    private String paket,payment,lkt,checklist,signature , no_ticketSP;
//    private String numberForm, photo, signature, profile, payment, paket, emergency,
//            imei, token, value, sflCode, hp,billZipcode, direction,
//            installAddress, installProvince, birthPlace,city, billingKelurahan, installDate,
//            billingProvince, identityNo, userId, zipCode, province, billingCity, installZipcode,
//            birthDate, firstName, installCity, homeNo, middleName, ftId, lastName, identityType,
//            installKelurahan, confirmStart, regDate, installKecamatan, kelurahan, billingKecamatan,
//            kecamatan, email, address, confirmEnd, rt, billingAddress, validityPriod, installTime,
//            rw, telephone, eFirstName, eAddress, eFormNo, eTelephone, eLastName, eRelationship,
//            eMiddleName, ODU, VC, BRAND, LNB, DSD, HW_STATUS, ala1, ala2, ala3, bsc1, bsc2, bsc3,
//            religion, job, income, gender, homeType, homeStatus, url, urlImage, response, planId,
//            status, data;
    private TableFormAppAdapter mFormAppAdapter;
    private List<TableFormApp> listFormApp;
    private boolean isTaskRunning = false;
    private ProgressDialog _progressDialog;
    private SharedPreferences sessionPref;
    private Handler handler;
    private JSONArray imageArray;
    private SupportMapFragment fragment;

    public static Fragment newInstance(Context context) {
        _context = context;
        Status dtdStatus = new Status();
        return dtdStatus;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.add_status, container, false);
//        sessionPref         = getContext().getSharedPreferences(Config.KEY_LOGIN_PROFILE, Context.MODE_PRIVATE);

        utils = new Utils(getActivity());


        handler = new Handler();

        _latitude = (TextView) view.findViewById(R.id.latitude);
        _longitude = (TextView) view.findViewById(R.id.longitude);
        paket_status_txt = (TextView) view.findViewById(R.id.txt_paket_status);
        payment_status_txt = (TextView) view.findViewById(R.id.txt_payment_status);
        lkt_status_txt = (TextView) view.findViewById(R.id.txt_lkt_status);
        checklist_status_txt = (TextView) view.findViewById(R.id.txt_checklist_status);
        signature_status_txt = (TextView) view.findViewById(R.id.txt_signature_status);

        image_paket_flag = (ImageView) view.findViewById(R.id.image_paket_flag);
        image_payment_flag = (ImageView) view.findViewById(R.id.image_payment_flag);
        image_lkt_flag = (ImageView) view.findViewById(R.id.image_lkt_flag);
        image_checklist_flag = (ImageView) view.findViewById(R.id.image_checklist_flag);
        image_signature_flag = (ImageView) view.findViewById(R.id.image_signature_flag);

        paketLayout = (LinearLayout) view.findViewById(R.id.paket_layout);
        paymentLayout = (LinearLayout) view.findViewById(R.id.payment_layout);
        lktLayout = (LinearLayout) view.findViewById(R.id.lkt_layout);
        checklistLayout = (LinearLayout) view.findViewById(R.id.checklist_layout);
        signatureLayout = (LinearLayout) view.findViewById(R.id.signature_layout);

        mScrollView = (ScrollView) view.findViewById(R.id.frame);
        setMapMarker();

        /*WorkaroundMapFragment mapFragment =
                (WorkaroundMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mapFragment.setListener(new WorkaroundMapFragment.OnTouchListener() {
            @Override
            public void onTouch() {
                mScrollView.requestDisallowInterceptTouchEvent(true);
            }
        });*/
        return view;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    public static void changeStatus() {
    }

    private void setMapMarker() {
        //latitude    = Double.parseDouble(utils.getLatitude());
        // longitude   = Double.parseDouble(utils.getLongitude());
        //Log.d("MSStatus", "long =" + longitude + "  lat=" + latitude);
        if (googleMap == null) {
            FragmentManager fm = getChildFragmentManager();
            fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
            if (fragment == null) {
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.map, fragment).commit();
            }
            fragment.getMapAsync(this);
            //googleMap = fragment;
            //mMap =  ((SupportMapFragment)  getChildFragmentManager()
            //        .findFragmentById(R.id.map)).getMap();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        FragmentManager fm = getChildFragmentManager();
        fragment = (SupportMapFragment) fm.findFragmentById(R.id.map);
        if (fragment == null) {
            fragment = SupportMapFragment.newInstance();
            fm.beginTransaction().replace(R.id.map, fragment).commit();
        }
        fragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        googleMap = map;

        googleMap.setOnMyLocationButtonClickListener(this);
        enableMyLocation();

        googleMap.getUiSettings().setZoomControlsEnabled(false);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnMarkerDragListener(this);

        googleMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {

            @Override
            public void onMyLocationChange(Location location) {
                lon = location.getLongitude();
                lat = location.getLatitude();

                latLng = location;
                Geocoder geocoder;
                List<Address> addresses = null;
                geocoder = new Geocoder(getActivity(), Locale.getDefault());

                if (marker == null) {
                    marker = googleMap.addMarker(new MarkerOptions().position(new LatLng(location.getLatitude(),
                            location.getLongitude())).icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED))
                            .draggable(true).alpha(0.7f));

                    googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(),
                            location.getLongitude()), 17.0f));

                    try {
                        addresses = geocoder.getFromLocation(lat, lon, 1);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    if (addresses != null && addresses.size() != 0) {
                        addressLine = addresses.get(0).getAddressLine(0);
                    } else {
                        addressLine = "";
                    }

                    _longitude.setText(String.valueOf(lon));
                    _latitude.setText(String.valueOf(lat));
                }
            }
        });

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
            @Override
            public View getInfoWindow(Marker marker) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {
                View v = getActivity().getLayoutInflater().inflate(R.layout.info_windows, null);
                txtAddressLine = (TextView) v.findViewById(R.id.address_line);
                txtAddressLine.setText(addressLine);
                return v;
            }
        });
    }

    /**
     * Enables the My Location layer if the fine location permission has been granted.
     */
    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission to access the location is missing.
            PermissionUtils.requestPermission(getActivity(), LOCATION_PERMISSION_REQUEST_CODE,
                    Manifest.permission.ACCESS_FINE_LOCATION, true);
        } else if (googleMap != null) {
            // Access to the location has been granted to the app.
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    @Override
    public void onMarkerDragStart(Marker marker) {
        mScrollView.requestDisallowInterceptTouchEvent(true);
    }

    @Override
    public void onMarkerDrag(Marker marker) {
        mScrollView.requestDisallowInterceptTouchEvent(true);
    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        LatLng dragPosition = marker.getPosition();
        double dragLat = dragPosition.latitude;
        double dragLong = dragPosition.longitude;
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());

        int distance = distance(latLng, dragPosition);

        if (distance>100) {
            utils.showErrorDialog("Jarak Marker Lebih Dari 100 Meter", "Silahkan pindahkan posisi marker");
            this.marker.remove();
            this.marker = null;
            //setUpMap();
            return;
        }

        try {
            addresses = geocoder.getFromLocation(dragLat, dragLong, 1);
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses != null && addresses.size() != 0) {
            addressLine = addresses.get(0).getAddressLine(0);
        } else {
            addressLine = "";
        }

        _longitude.setText(String.valueOf(dragLong));
        _latitude.setText(String.valueOf(dragLat));
    }

    @Override
    public boolean onMyLocationButtonClick() {
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != LOCATION_PERMISSION_REQUEST_CODE) {
            return;
        }

        if (PermissionUtils.isPermissionGranted(permissions, grantResults,
                Manifest.permission.ACCESS_FINE_LOCATION)) {
            // Enable the my location layer if the permission has been granted.
            enableMyLocation();
        } else {
            // Display the missing permission error dialog when the fragments resume.
            mPermissionDenied = true;
        }
    }




    @Override
    public void onResume() {
        super.onResume();
        getDataFromDB();

        if (mPermissionDenied) {
            // Permission was not granted, display error dialog.
            showMissingPermissionError();
            mPermissionDenied = false;
        }

        if (!checkReady()) {
            return;
        }
        // Clear the map because we don't want duplicates of the markers.
        googleMap.clear();
    }

    @Override
    public void onPause() {
        super.onPause();
        if (!checkReady()) {
            return;
        }
        googleMap.clear();

        if (marker != null) {
            marker.remove();
            marker = null;
        }
    }

    private boolean checkReady() {
        if (googleMap == null) {
            //Toast.makeText(getActivity(), R.string.map_not_ready, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    /**
     * Displays a dialog with error message explaining that the location permission is missing.
     */
    private void showMissingPermissionError() {
        PermissionUtils.PermissionDeniedDialog
                .newInstance(true).show(getChildFragmentManager(), "dialog");
    }

    public static int distance(Location StartP, LatLng EndP) {
        double earthRadius = 6371000; //meters
        double lat1 = StartP.getLatitude();
        double lat2 = EndP.latitude;
        double lng1 = StartP.getLongitude();
        double lng2 = EndP.longitude;

        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLng / 2) * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        int dist = (int) (earthRadius * c);
        return dist;
    }

    /**
     * Method for get refresh longitude and latitude
     */
   /* private void setLocation() {
        String stringLatitude = utils.getLatitude();
        String stringLongitude = utils.getLongitude();
        Geocoder geocoder;
        List<Address> addresses = null;
        geocoder = new Geocoder(getActivity(), Locale.getDefault());
        if (stringLatitude!=null && stringLatitude.length()> 1) {
            lat  = Double.valueOf(stringLatitude);
            lon = Double.valueOf(stringLongitude);
            try {
                addresses = geocoder.getFromLocation(lat, lon, 1);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (addresses != null && addresses.size() != 0) {
                addressLine = addresses.get(0).getAddressLine(0);
            } else {
                addressLine = "";
            }
            Toast.makeText(getActivity(), "Long= "+stringLongitude+" lat= "+stringLatitude, Toast.LENGTH_SHORT).show();

            _longitude.setText(stringLongitude);
            _latitude.setText(stringLatitude);
        } else {
            Toast.makeText(getActivity(), "Longitude dan latitude kosong", Toast.LENGTH_SHORT).show();
        }
    }*/

    private void getDataFromDB() {
        try {
            /*if (getActivity().getIntent().getStringExtra("ftId")!=null
                    ||getActivity().getIntent().getStringExtra("ftId").length()>1) {
                numberForm = getActivity().getIntent().getStringExtra("ftId");
            } else {
                SharedPreferences sm = getActivity().getSharedPreferences(
                        "ftId", Context.MODE_PRIVATE);
                numberForm = sm.getString("ft", null);
            }*/

            SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.detail_id), Context.MODE_PRIVATE);
            no_ticketSP = sm.getString("no_ticket", null);

            mFormAppAdapter      = new TableFormAppAdapter(getActivity());
//            listFormApp          = mFormAppAdapter.getAllData();
            listFormApp          = mFormAppAdapter.getDatabyCondition(TableFormApp.fNO_TICKET,no_ticketSP);
            for (int i=0; i<listFormApp.size(); i++) {
                TableFormApp item        = listFormApp.get(i);
                if (item !=null) {
                    paket       = item.getVALUES_PACKAGE();
                    payment     = item.getVALUES_PAYMENT();
                    lkt         = item.getVALUES_LKT();
                    checklist   = item.getVALUES_CHECKLIST();
                    signature   = item.getVALUES_SIGNATURE();
                }
            }

            // token   = sessionPref.getString(TableLogLogin.C_TOKEN, "");

            if (paket != null) {
                paket_status_txt.setText("Data sudah diambil");
                paketLayout.setBackgroundResource(R.drawable.border_item_master);
                image_paket_flag.setImageResource(R.mipmap.ic_centang);
            } else {
                paket_status_txt.setText("Data belum diambil");
                paketLayout.setBackgroundResource(R.drawable.border_item_red);
                image_paket_flag.setImageResource(R.mipmap.ic_kali);
            }

            if (payment != null) {
                payment_status_txt.setText("Data sudah diambil");
                paymentLayout.setBackgroundResource(R.drawable.border_item_master);
                image_payment_flag.setImageResource(R.mipmap.ic_centang);
            }  else {
                payment_status_txt.setText("Data belum diambil");
                paymentLayout.setBackgroundResource(R.drawable.border_item_red);
                image_payment_flag.setImageResource(R.mipmap.ic_kali);
            }

            if (lkt != null){
                lkt_status_txt.setText("Data sudah diambil");
                lktLayout.setBackgroundResource(R.drawable.border_item_master);
                image_lkt_flag.setImageResource(R.mipmap.ic_centang);
            } else {
                lkt_status_txt.setText("Data belum diambil");
                lktLayout.setBackgroundResource(R.drawable.border_item_red);
                image_lkt_flag.setImageResource(R.mipmap.ic_kali);
            }

            if (checklist != null){
                checklist_status_txt.setText("Data sudah diambil");
                checklistLayout.setBackgroundResource(R.drawable.border_item_master);
                image_checklist_flag.setImageResource(R.mipmap.ic_centang);
            } else {
                checklist_status_txt.setText("Data belum diambil");
                checklistLayout.setBackgroundResource(R.drawable.border_item_red);
                image_checklist_flag.setImageResource(R.mipmap.ic_kali);
            }
            if (signature != null){
                signature_status_txt.setText("Data sudah diambil");
                signatureLayout.setBackgroundResource(R.drawable.border_item_master);
                image_signature_flag.setImageResource(R.mipmap.ic_centang);
            } else {
                signature_status_txt.setText("Data belum diambil");
                signatureLayout.setBackgroundResource(R.drawable.border_item_red);
                image_signature_flag.setImageResource(R.mipmap.ic_kali);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*
    private class SendToServerTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            Log.d("FTStatus", "long=" + lon + " lat=" + lat + " add=" + addressLine);
            if (isTaskRunning) {
                this.cancel(true);
                return;
            }
            else if (paket== null || paket.length()<1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data package", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (payment==null || payment.length()<1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data payment", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (lkt==null || lkt.length()<1){
                Toast.makeText(getActivity(), "Silahkan masukkan data lkt", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (checklist==null || checklist.length()<1) {
                Toast.makeText(getActivity(), "Silahkan masukkan data checklist", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (signature==null || signature.length()<1) {
                Toast.makeText(getActivity(), "Silahkan tanda tangan", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            } else if (lat==null  || String.valueOf(lat).equalsIgnoreCase("0.0")) {
                Toast.makeText(getActivity(), "Latitude kosong", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            }  else if (lon==null  || String.valueOf(lon).equalsIgnoreCase("0.0")) {
                Toast.makeText(getActivity(), "Longitude kosong", Toast.LENGTH_SHORT).show();
                this.cancel(true);
                return;
            }


            if (_progressDialog != null) {
                _progressDialog.dismiss();
            }
            _progressDialog = new ProgressDialog(getActivity());
            _progressDialog.setMessage(getActivity().getResources().getString(R.string.loading));
            _progressDialog.setIndeterminate(false);
            _progressDialog.setCancelable(false);
            _progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                isTaskRunning = true;
//                value = generateJson().toString();
//                TablePlanAdapter mTablePlanAdapter = new TablePlanAdapter(getActivity());
//                token   = sessionPref.getString(TableLogLogin.C_TOKEN, "");
//                sflCode = sessionPref.getString(TableLogLogin.C_SFL_CODE, "");
//                List<TablePlan> listPlan = mTablePlanAdapter.getDatabySfl(getActivity(), TablePlan.KEY_PLAN_DATE,
//                        utils.getCurrentDate());
//                if (listPlan!=null && listPlan.size()>0) {
//                    planId      = listPlan.get(0).getPlan_id();
//                }
//                imei        = utils.getIMEI();
//                response    = ConnectionManager.requestToServer(url, imei, token, value, planId, Config.version, "9", getActivity());
//                JSONObject jsonObject  = new JSONObject(response.toString());
//                if (jsonObject != null) {
//                    status          = jsonObject.getString(Config.KEY_STATUS);
//                    data            = jsonObject.getString(Config.KEY_MESSAGE);
//                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                isTaskRunning = false;
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }

//                if (data != null) {
//                    Toast.makeText(getActivity(), data, Toast.LENGTH_SHORT).show();
//                }
//
//                if (status!=null && status.equalsIgnoreCase("200")) {
//
//                    getActivity().finish();
//                } else {
//                    utils.showErrorDlg(handler, getActivity().getResources().getString(R.string.network_error),
//                            getActivity());
//                }
            } catch (Exception e) {
                if (_progressDialog != null) {
                    _progressDialog.dismiss();
                }
                e.printStackTrace();
            }
        }
    }*/
}
