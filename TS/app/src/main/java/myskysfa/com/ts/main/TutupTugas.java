package myskysfa.com.ts.main;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import android.widget.Spinner;
import android.widget.Toast;

import myskysfa.com.ts.R;

import myskysfa.com.ts.adapter.SpinnerArrayAdapter;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/28/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TutupTugas extends Fragment {
    private Utils utils;

    private EditText catatan;
    private Button tutuptugas;

    private String[] array_resolvecode;
    private Spinner resolvecode;
    private SpinnerArrayAdapter resolvecodeAdapter;

    private String no_ticket;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.tutup_tugas, container, false);

        loadResources(viewGroup);
        loadListener();
        return viewGroup;
    }

    public void loadResources(ViewGroup root) {
        utils = new Utils(getActivity());

        resolvecode = (Spinner) root.findViewById(R.id.resc_resolvecode);
        catatan     = (EditText) root.findViewById(R.id.resc_catatan);
        tutuptugas  = (Button) root.findViewById(R.id.resc_tutuptugas);

        array_resolvecode   = getResources().getStringArray(R.array.array_resolvecode);
        resolvecodeAdapter  = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_resolvecode);
        resolvecodeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        resolvecode.setAdapter(resolvecodeAdapter);

        resolvecode.setSelection(0);
        tutuptugas.setOnClickListener(_btn_tutuptugas);
    }

    private void loadListener() {
        SharedPreferences preferences = getActivity().getSharedPreferences(getString(R.string.detail_id), Context.MODE_PRIVATE);
        no_ticket = preferences.getString("no_ticket", null);
    }

    private View.OnClickListener _btn_tutuptugas = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showTutupTugasDialog();
        }
    };

    /*
    |---------------------------------- -------------------------------------------------------------
    | Tutup Tugas Alert Dialog
    |-----------------------------------------------------------------------------------------------
    */
    public void showTutupTugasDialog() {
        utils.loadAlertDialog(false, "Peringatan!!", "Apakah anda yakin ingin menutup No. Ticket "+no_ticket+" ?\n", "", "TIDAK", "YA");

        utils._twobtn_left.setBackgroundResource(R.drawable.button_green);
        utils._twobtn_right.setBackgroundResource(R.drawable.button_red);
        utils._twobtn_left.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                utils.dialog.dismiss();
            }
        });

        utils._twobtn_right.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View paramAnonymousView)
            {
                Toast.makeText(getActivity(), "Belum dibuat, showTutupTugasDialog", Toast.LENGTH_LONG).show();
                //utils.dialog.dismiss();
            }
        });

        //utils.dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        utils.dialog.show();
        utils.dialog.setCancelable(false);
    }
}