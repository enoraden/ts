package myskysfa.com.ts.main.master;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.master.MasterTabLayoutAdapter;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterTabLayout extends Fragment {
    private Utils utils;

    private LinearLayout tittlelayout;
    private TabLayout tabLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.tab_layout, container, false);

        loadResources(viewGroup);
        return viewGroup;
    }

    private void loadResources(ViewGroup root) {
        utils = new Utils(getActivity());

        tittlelayout = (LinearLayout) root.findViewById(R.id.tablayout_tittlelayout);
        tittlelayout.setVisibility(View.GONE);

        tabLayout = (TabLayout) root.findViewById(R.id.tablayout_tabs);
        tabLayout.addTab(tabLayout.newTab().setText("STOCK ON HAND"));
        tabLayout.addTab(tabLayout.newTab().setText("PACKAGE"));
        tabLayout.addTab(tabLayout.newTab().setText("PROMO"));
        tabLayout.addTab(tabLayout.newTab().setText("MATERIAL"));

        final ViewPager viewpager               = (ViewPager) root.findViewById(R.id.tablayout_viewpager);
        final MasterTabLayoutAdapter adapter    = new MasterTabLayoutAdapter(getActivity().getSupportFragmentManager(), tabLayout.getTabCount());

        viewpager.setOffscreenPageLimit(3);
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewpager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        ViewGroup viewGroup = (ViewGroup) tabLayout.getChildAt(0);
        int tabsCount       = viewGroup.getChildCount();

        for (int j = 0; j < tabsCount; j++) {
            ViewGroup vgTab     = (ViewGroup) viewGroup.getChildAt(j);
            int tabChildsCount  = vgTab.getChildCount();

            for (int i = 0; i < tabChildsCount; i++) {
                View tabViewChild = vgTab.getChildAt(i);

                if (tabViewChild instanceof TextView) {
                    ((TextView) tabViewChild).setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
                }
            }
        }
    }
}