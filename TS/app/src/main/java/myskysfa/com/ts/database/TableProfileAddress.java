package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by Taufik Hidayat on 1/17/2017.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "profile_address")
public class TableProfileAddress {
    public static final String TABLE_NAME       = "profile_address";

    public static final String fNEW_ID          = "new_id";
    public static final String fADDRESS_TYPE    = "address_type";
    public static final String fPROSPECT_NBR    = "prospect_nbr";
    public static final String fBUILDING_STATUS_ID = "building_status_id";
    public static final String fBUILDING_TYPE   = "building_type";
    public static final String fSTREET          = "street";
    public static final String fRT              = "rt";
    public static final String fRW              = "rw";
    public static final String fNO_RUMAH        = "no_rumah";
    public static final String fKELURAHAN       = "kelurahan";
    public static final String fKECAMATAN       = "kecamatan";
    public static final String fKOTA            = "kota";
    public static final String fPROVINCE        = "province";
    public static final String fZIPCODE         = "zipcode";
    public static final String fCREATED_BY      = "created_by";
    public static final String fCREATED_DATE    = "created_date";
    public static final String fLAST_UPD_BY     = "last_upd_by";
    public static final String fLAST_UPD_DATE   = "last_upd_date";

    @DatabaseField(generatedId = true)
    private int new_id;

    @DatabaseField
    private String address_type, prospect_nbr, building_status_id, building_type, street, rt, rw,
            no_rumah, kelurahan, kecamatan, kota, province, zipcode, created_by, created_date,
            last_upd_by, last_upd_date;

    public void setfNEW_ID(int new_id) {
        this.new_id = new_id;
    }
    public int getfNEW_ID() {
        return new_id;
    }

    public void setfADDRESS_TYPE(String address_type) {
        this.address_type = address_type;
    }
    public String getfADDRESS_TYPE() {
        return address_type;
    }

    public void setfPROSPECT_NBR(String prospect_nbr) {
        this.prospect_nbr = prospect_nbr;
    }
    public String getfPROSPECT_NBR() {
        return prospect_nbr;
    }

    public void setfBUILDING_STATUS_ID(String building_status_id) {
        this.building_status_id = building_status_id;
    }
    public String getfBUILDING_STATUS_ID() {
        return building_status_id;
    }

    public void setfBUILDING_TYPE(String building_type) {
        this.building_type = building_type;
    }
    public String getfBUILDING_TYPE() {
        return building_type;
    }

    public void setfSTREET(String street) {
        this.street = street;
    }
    public String getfSTREET() {
        return street;
    }

    public void setfRT(String rt) {
        this.rt = rt;
    }
    public String getfRT() {
        return rt;
    }

    public void setfRW(String rw) {
        this.rw = rw;
    }
    public String getfRW() {
        return rw;
    }

    public void setfNO_RUMAH(String no_rumah) {
        this.no_rumah = no_rumah;
    }
    public String getfNO_RUMAH() {
        return no_rumah;
    }

    public void setfKELURAHAN(String kelurahan) {
        this.kelurahan = kelurahan;
    }
    public String getfKELURAHAN() {
        return kelurahan;
    }

    public void setfKECAMATAN(String kecamatan) {
        this.kecamatan = kecamatan;
    }
    public String getfKECAMATAN() {
        return kecamatan;
    }

    public void setfKOTA(String kota) {
        this.kota = kota;
    }
    public String getfKOTA() {
        return kota;
    }

    public void setfPROVINCE(String province) {
        this.province = province;
    }
    public String getfPROVINCE() {
        return province;
    }

    public void setfZIPCODE(String zipcode) {
        this.zipcode = zipcode;
    }
    public String getfZIPCODE() {
        return zipcode;
    }

    public void setfCREATED_BY(String created_by) {
        this.created_by = created_by;
    }
    public String getfCREATED_BY() {
        return created_by;
    }

    public void setfCREATED_DATE(String created_date) {
        this.created_date = created_date;
    }
    public String getfCREATED_DATE() {
        return created_date;
    }

    public void setfLAST_UPD_BY(String last_upd_by) {
        this.last_upd_by = last_upd_by;
    }
    public String getfLAST_UPD_BY() {
        return last_upd_by;
    }

    public void setfLAST_UPD_DATE(String last_upd_date) {
        this.last_upd_date = last_upd_date;
    }
    public String getfLAST_UPD_DATE() {
        return last_upd_date;
    }
}
