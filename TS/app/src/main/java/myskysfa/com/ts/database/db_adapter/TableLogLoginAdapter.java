package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.utils.DatabaseManager;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TableLogLoginAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TableLogLoginAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TableLogLogin> getDatabyCondition(Context context, String condition, Object param) {
        List<TableLogLogin> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableLogLogin.class);

            QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();
            Where<TableLogLogin, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TableLogLogin> getAllData(Context context) {
        List<TableLogLogin> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableLogLogin.class);

            QueryBuilder<TableLogLogin, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TableLogLogin table, String user_id, String user_name, String user_type,
                           String employee_id, String full_name, String role_code, String role_name,
                           String sfl_code, String brand, String branch_id, String branch,
                           String region_code, String region_name, String is_supervisor, String access_token) {
        try {
            table.setfUSER_ID(user_id);
            table.setfUSER_NAME(user_name);
            table.setfUSER_TYPE(user_type);
            table.setfEMPLOYEE_ID(employee_id);
            table.setfFULL_NAME(full_name);
            table.setfROLE_CODE(role_code);
            table.setfROLE_NAME(role_name);
            table.setfSFL_CODE(sfl_code);
            table.setfBRAND(brand);
            table.setfBRANCH_ID(branch_id);
            table.setfBRANCH(branch);
            table.setfREGION_CODE(region_code);
            table.setfREGION_NAME(region_name);
            table.setfIS_SUPERVISOR(is_supervisor);
            table.setfACCESS_TOKEN(access_token);
            getHelper().getTableLogLoginDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableLogLogin.class);

            UpdateBuilder<TableLogLogin, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String user_id, String user_name, String user_type,
                              String employee_id, String full_name, String role_code, String role_name,
                              String sfl_code, String brand, String branch_id, String branch,
                              String region_code, String region_name, String is_supervisor, String access_token,
                              String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableLogLogin.class);

            UpdateBuilder<TableLogLogin, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TableLogLogin.fUSER_ID, user_id);
            updateBuilder.updateColumnValue(TableLogLogin.fUSER_NAME, user_name);
            updateBuilder.updateColumnValue(TableLogLogin.fUSER_TYPE, user_type);
            updateBuilder.updateColumnValue(TableLogLogin.fEMPLOYEE_ID, employee_id);
            updateBuilder.updateColumnValue(TableLogLogin.fFULL_NAME, full_name);
            updateBuilder.updateColumnValue(TableLogLogin.fROLE_CODE, role_code);
            updateBuilder.updateColumnValue(TableLogLogin.fROLE_NAME, role_name);
            updateBuilder.updateColumnValue(TableLogLogin.fSFL_CODE, sfl_code);
            updateBuilder.updateColumnValue(TableLogLogin.fBRAND, brand);
            updateBuilder.updateColumnValue(TableLogLogin.fBRANCH_ID, branch_id);
            updateBuilder.updateColumnValue(TableLogLogin.fBRANCH, branch);
            updateBuilder.updateColumnValue(TableLogLogin.fREGION_CODE, region_code);
            updateBuilder.updateColumnValue(TableLogLogin.fREGION_NAME, region_name);
            updateBuilder.updateColumnValue(TableLogLogin.fIS_SUPERVISOR, is_supervisor);
            updateBuilder.updateColumnValue(TableLogLogin.fACCESS_TOKEN, access_token);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TableLogLogin.class);

            DeleteBuilder<TableLogLogin, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TableLogLogin> listTable = null;
        try {
            listTable = getHelper().getTableLogLoginDAO().queryForAll();
            getHelper().getTableLogLoginDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}