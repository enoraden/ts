package myskysfa.com.ts.main.rejoin;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.w3c.dom.Document;

import java.util.ArrayList;

import myskysfa.com.ts.R;
import myskysfa.com.ts.main.childview.ChildProfile;
import myskysfa.com.ts.utils.PermissionUtils;
import myskysfa.com.ts.utils.Utils;
import myskysfa.com.ts.widget.GMapDirection;
import myskysfa.com.ts.widget.WorkaroundMapFragment;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Profile extends Fragment implements OnMapReadyCallback {
    private ScrollView scrollview;
    private ImageView refresh;
    private LinearLayout layoutprofile;

    private GMapDirection md;
    private GoogleMap mMap;
    private SupportMapFragment fragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.r_profile, container, false);

        loadResources(viewGroup);
        loadMap();
        loadListener(viewGroup);

        return viewGroup;
    }

    private void loadResources(ViewGroup root) {
        scrollview      = (ScrollView) root.findViewById(R.id.pro_scrollview);
        refresh         = (ImageView) root.findViewById(R.id.pro_refresh);
        layoutprofile   = (LinearLayout) root.findViewById(R.id.pro_layoutprofile);

        refresh.setOnClickListener(_refresh);
    }

    private void loadListener(ViewGroup parent) {
        ChildProfile profile    = new ChildProfile(getActivity());
        View viewChild          = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_profile, parent, false);
        layoutprofile.addView(viewChild);
        profile.profileView(viewChild);
    }

    private View.OnClickListener _refresh = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new getDirection().execute();
        }
    };

    private void loadMap() {
        if (mMap == null) {
            FragmentManager fm = getChildFragmentManager();
            fragment = (WorkaroundMapFragment) fm.findFragmentById(R.id.pro_map);

            if (fragment == null) {
                fragment = SupportMapFragment.newInstance();
                fm.beginTransaction().replace(R.id.pro_map, fragment).commit();
            }

            ((WorkaroundMapFragment) fm.findFragmentById(R.id.pro_map)).setListener(new WorkaroundMapFragment.OnTouchListener() {
                @Override
                public void onTouch() {
                    scrollview.requestDisallowInterceptTouchEvent(true);
                }
            });

            fragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap    = googleMap;
        md      = new GMapDirection();
        enableMyLocation();
        new getDirection().execute();
    }

    private void enableMyLocation() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            PermissionUtils.requestPermission(getActivity(), 1, Manifest.permission.ACCESS_FINE_LOCATION, true);

        } else if (mMap != null) {
            mMap.setMyLocationEnabled(true);
        }
    }

    private class getDirection extends AsyncTask<String, String, String> {
        private Utils utils;
        private LatLng position, direction;
        private double latitude, longitude;
        private Document doc;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            utils = new Utils(getActivity());
            utils.getLatLong();
            latitude    = utils.latitude;
            longitude   = utils.longitude;

            utils.showDialogLoading("Loading . . .");

            Log.i("latlong", String.valueOf(latitude)+","+String.valueOf(longitude));
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                if (latitude == 0 || longitude == 0) {
                    doc = null;
                } else {
                    position    = new LatLng(latitude, longitude);
                    direction   = new LatLng(-6.227622, 106.796916);
                    doc         = md.getDocument(position, direction, GMapDirection.MODE_DRIVING);
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (utils.progressdialog.isShowing())
                    utils.progressdialog.dismiss();

                if (doc != null) {
                    ArrayList<LatLng> directionPoint    = md.getDirection(doc);
                    PolylineOptions rectLine            = new PolylineOptions().width(3).color(Color.RED);

                    for (int i = 0; i < directionPoint.size(); i++) {
                        rectLine.add(directionPoint.get(i));
                    }

                    mMap.addMarker(new MarkerOptions().position(position).title("Your Position"));
                    mMap.addMarker(new MarkerOptions().position(direction).title("direction"));
                    mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(position.latitude, position.longitude), 17.0f));
                    //mMap.moveCamera(CameraUpdateFactory.newLatLng(position));
                    mMap.addPolyline(rectLine);
                } else {
                    Utils utils = new Utils(getActivity());
                    utils.showErrorDialog("Koneksi Error", "Latitude dan Longitude kosong.\n");
                    Log.d("ErrorJSON", "GoogleMap");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}