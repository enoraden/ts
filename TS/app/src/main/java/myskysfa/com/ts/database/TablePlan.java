package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/7/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "plan_task")
public class TablePlan {
    public static final String TABLE_NAME       = "plan_task";

    public static final String fPLAN_ID         = "plan_id";
    public static final String fPLAN_DATE       = "plan_date";
    public static final String fZIP_CODE        = "zip_code";
    public static final String fAREA            = "area";
    public static final String fTS_CODE         = "ts_code";
    public static final String fTS_NAME         = "ts_name";
    public static final String fTARGET          = "target";
    public static final String fSFL_CODE        = "sfl_code";

    @DatabaseField(id = true)
    private String plan_id;

    @DatabaseField
    private String plan_date, zip_code, area, ts_code, ts_name, target, sfl_code;

    public void setfPLAN_ID(String plan_id) {
        this.plan_id = plan_id;
    }
    public String getfPLAN_ID() {
        return plan_id;
    }

    public void setfPLAN_DATE(String plan_date) {
        this.plan_date = plan_date;
    }
    public String getfPLAN_DATE() {
        return plan_date;
    }

    public void setfZIP_CODE(String zip_code) {
        this.zip_code = zip_code;
    }
    public String getfZIP_CODE() {
        return zip_code;
    }

    public void setfAREA(String area) {
        this.area = area;
    }
    public String getfAREA() {
        return area;
    }

    public void setfTS_CODE(String ts_code) {
        this.ts_code = ts_code;
    }
    public String getfTS_CODE() {
        return ts_code;
    }

    public void setfTS_NAME(String ts_name) {
        this.ts_name = ts_name;
    }
    public String getfTS_NAME() {
        return ts_name;
    }

    public void setfTARGET(String target) {
        this.target = target;
    }
    public String getfTARGET() {
        return target;
    }

    public void setfSFL_CODE(String sfl_code) {
        this.sfl_code = sfl_code;
    }
    public String getfSFL_CODE() {
        return sfl_code;
    }
}
