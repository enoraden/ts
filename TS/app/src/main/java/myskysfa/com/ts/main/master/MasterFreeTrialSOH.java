package myskysfa.com.ts.main.master;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.master.MasterFreeTrialSOHAdapter;
import myskysfa.com.ts.database.TableFreeTrialSOH;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableFreeTrialSOHAdapter;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.Utils;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/17/2016.
|---------------------------------------------------------------------------------------------------
*/
public class MasterFreeTrialSOH extends Fragment {
    private MasterFreeTrialSOHAdapter adapter;
    private TableFreeTrialSOHAdapter dbAdapterFreeTrialSOH;
    private List<TableFreeTrialSOH> listFreeTrialSOH;

    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;

    private TextView info;
    private RecyclerView recyclerview;
    private SwipeRefreshLayout swiperefresh;

    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.list_menu, container, false);

        info            = (TextView) viewGroup.findViewById(R.id.listmenu_info);
        recyclerview    = (RecyclerView) viewGroup.findViewById(R.id.listmenu_recyclerview);
        swiperefresh    = (SwipeRefreshLayout) viewGroup.findViewById(R.id.listmenu_swiperefresh);

        swiperefresh.setColorSchemeColors(Color.RED, Color.GREEN, Color.BLUE, Color.YELLOW);
        swiperefresh.setEnabled(true);
        swiperefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshItems();
            }
        });

        refreshItems();
        return viewGroup;
    }

    private void refreshItems() {
        utils = new Utils(getActivity());
        info.setVisibility(View.GONE);

        connDetect          = new ConnectionDetector(getActivity());
        isInternetPresent   = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            new getDataFreeTrialSOH().execute();
        } else {
            if (swiperefresh.isShown()) {
                swiperefresh.setRefreshing(false);
            }

            utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
        }
    }

    /*
    |---------------------------------------------------------------------------------------------------
    | Get Data SOH From Server
    |---------------------------------------------------------------------------------------------------
    */
    private class getDataFreeTrialSOH extends AsyncTask<String, String, String> {
        private Boolean _status;
        private String _data;
        private JSONArray _seralizearray;

        private int _signal;
        private String _imei;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            swiperefresh.setRefreshing(true);

            utils   = new Utils(getActivity());
            _signal = utils.checkSignal();
            _imei   = utils.getIMEI();
            _status = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(getActivity());
                listLogLogin        = dbAdapterLogLogin.getAllData(getActivity());

                if (listLogLogin.size() > 0) {
                    String _url         = ConnectionManager.CM_URL_MASTER_HARDWARE;
                    String _employee_id = listLogLogin.get(listLogLogin.size() - 1).getfEMPLOYEE_ID();
                    String _token       = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String paramurl     = _url + "/" + _employee_id;

                    String _response    = ConnectionManager.requestDataMaster(paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status             = jsonObject.getBoolean("status");
                        _data               = jsonObject.getString("data");
                        JSONObject objData  = new JSONObject(_data);

                        if (_status) {
                            dbAdapterFreeTrialSOH = new TableFreeTrialSOHAdapter(getActivity());
                            dbAdapterFreeTrialSOH.deleteAllData();

                            _seralizearray  = objData.getJSONArray("serialize");

                            if(_seralizearray != null) {
                                for (int i = 0; i < _seralizearray.length(); i++) {
                                    JSONObject objSeralize = _seralizearray.getJSONObject(i);

                                    String item_id      = objSeralize.getString("item_id");
                                    String item_code    = objSeralize.getString("item_code");
                                    String item_descr   = objSeralize.getString("item_descr");
                                    String serial_no    = objSeralize.getString("serial_no");
                                    String employee_id  = objSeralize.getString("employee_id");
                                    String qty          = objSeralize.getString("qty");

                                    dbAdapterFreeTrialSOH = new TableFreeTrialSOHAdapter(getActivity());
                                    dbAdapterFreeTrialSOH.insertData(new TableFreeTrialSOH(),
                                            item_id, item_code, item_descr, serial_no, employee_id, qty);
                                }
                            }
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (swiperefresh.isShown()) {
                    swiperefresh.setRefreshing(false);
                }

                if (_status) {
                    showList();

                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "FreeTrialSOH");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private void showList() {
            LinearLayoutManager layoutParams = new LinearLayoutManager(getActivity());
            layoutParams.setOrientation(LinearLayoutManager.VERTICAL);
            recyclerview.setLayoutManager(layoutParams);

            dbAdapterFreeTrialSOH   = new TableFreeTrialSOHAdapter(getActivity());
            listFreeTrialSOH        = dbAdapterFreeTrialSOH.getAllData(getActivity());
            adapter                 = new MasterFreeTrialSOHAdapter(listFreeTrialSOH);
            recyclerview.setAdapter(adapter);

            if(listFreeTrialSOH.size() > 0){
                info.setVisibility(View.GONE);
            } else {
                info.setVisibility(View.VISIBLE);
                info.setText("Tidak Ada Data SOH");
            }
        }
    }
}