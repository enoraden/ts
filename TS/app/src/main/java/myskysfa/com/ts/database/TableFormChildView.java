package myskysfa.com.ts.database;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 12/14/2016.
|---------------------------------------------------------------------------------------------------
*/
@DatabaseTable(tableName = "form_childview")
public class TableFormChildView {
    public static final String TABLE_NAME           = "form_childview";

    public static final String fNEW_ID              = "new_id";
    public static final String fNO_TICKET           = "no_ticket";
    public static final String fVALUE_PROFILE       = "value_profile";
    public static final String fVALUE_EMERGENCY     = "value_emergency";
    public static final String fVALUE_PAYMENT       = "value_payment";
    public static final String fVALUE_PACKAGE       = "value_package";

    @DatabaseField(generatedId = true)
    private int new_id;

    @DatabaseField
    private String no_ticket, value_profile, value_emergency, value_payment, value_package;

    public void setfNEW_ID(int new_id) {
        this.new_id = new_id;
    }
    public int getfNEW_ID() {
        return new_id;
    }

    public void setfNO_TICKET(String no_ticket) {
        this.no_ticket = no_ticket;
    }
    public String getfNO_TICKET() {
        return no_ticket;
    }

    public void setfVALUE_PROFILE(String value_profile) {
        this.value_profile = value_profile;
    }
    public String getfVALUE_PROFILE() {
        return value_profile;
    }

    public void setfVALUE_EMERGENCY(String value_emergency) {
        this.value_emergency = value_emergency;
    }
    public String getfVALUE_EMERGENCY() {
        return value_emergency;
    }

    public void setfVALUE_PAYMENT(String value_payment) {
        this.value_payment = value_payment;
    }
    public String getfVALUE_PAYMENT() {
        return value_payment;
    }

    public void setfVALUE_PACKAGE(String value_package) {
        this.value_package = value_package;
    }
    public String getfVALUE_PACKAGE() {
        return value_package;
    }
}