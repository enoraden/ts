package myskysfa.com.ts.main;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.Utils;

import org.json.JSONObject;

import java.util.List;
import java.util.Properties;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/10/2016.
|---------------------------------------------------------------------------------------------------
*/
public class LoginActivity extends AppCompatActivity {
    private TableLogLoginAdapter dbAdapterLogLogin;
    private List<TableLogLogin> listLogLogin;

    private Utils utils;

    private RelativeLayout mainlayout;
    private LinearLayout menulayout;
    private TextView nameapp;
    private EditText username, password;
    private Button login;
    private TextView version;

    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;

    private String _username="", _password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);

        setUpProxy();
        loadResources();
        loadKeyboardListener();
        loadListener();
    }

    private void setUpProxy() {
        try {
            Properties props = System.getProperties();
            System.out.println("proxy");
            System.out.println("before: " + props.toString());
            System.setProperty("http.proxyHost", "192.168.220.32");
            System.setProperty("http.proxyPort", "3128");
            System.out.println("after: " + props.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadResources() {
        utils = new Utils(LoginActivity.this);

        mainlayout  = (RelativeLayout) findViewById(R.id.loginact_mainlayout);
        menulayout  = (LinearLayout) findViewById(R.id.loginact_menulayout);
        nameapp     = (TextView) findViewById(R.id.loginact_nameapp);
        username    = (EditText) findViewById(R.id.loginact_username);
        password    = (EditText) findViewById(R.id.loginact_password);
        login       = (Button) findViewById(R.id.loginact_login);
        version     = (TextView) findViewById(R.id.loginact_version);

        username.requestFocus();
        login.setOnClickListener(login_listener);
        version.setText("Version "+ Config.version);
    }

    private void loadKeyboardListener() {
        mainlayout.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {

                Rect r = new Rect();
                mainlayout.getWindowVisibleDisplayFrame(r);
                int screenHeight = mainlayout.getRootView().getHeight();
                int keypadHeight = screenHeight - r.bottom;

                if (keypadHeight > screenHeight * 0.15) {
                    menulayout.setGravity(Gravity.TOP);
                    menulayout.setPadding(0, (int) 45.0, 0, 0); //7.5   = 5dp, 15.0  = 10dp, 22.5  = 15dp
                }
                else {
                    menulayout.setGravity(Gravity.CENTER);
                    menulayout.setPadding(0, 0, 0, 0); //7.5   = 5dp, 15.0  = 10dp, 22.5  = 15dp
                }
            }
        });
    }

    private void loadListener() {
        dbAdapterLogLogin   = new TableLogLoginAdapter(LoginActivity.this);
        listLogLogin        = dbAdapterLogLogin.getAllData(LoginActivity.this);

        if (listLogLogin.size() > 0) {
            _username = listLogLogin.get(listLogLogin.size() - 1).getfUSER_NAME();
            username.setText(_username);
            password.requestFocus();
        }
    }

    private View.OnClickListener login_listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (username.getText().toString().equalsIgnoreCase("")
                    || username.getText().toString().trim().length() < 1) {
                username.requestFocus();
                Toast.makeText(LoginActivity.this, "username tidak boleh kosong.!", Toast.LENGTH_SHORT).show();

            } else if (password.getText().toString().equalsIgnoreCase("")
                    || password.getText().toString().trim().length() < 1) {
                password.requestFocus();
                Toast.makeText(LoginActivity.this, "password tidak boleh kosong.!", Toast.LENGTH_SHORT).show();

            } else if (listLogLogin.size() > 0 && !username.getText().toString().equalsIgnoreCase(_username)) {
                username.setText(_username);
                password.setText("");
                password.requestFocus();
                Toast.makeText(LoginActivity.this, "Anda masih login dengan user : " + username, Toast.LENGTH_SHORT).show();

            } else {
                _username = username.getText().toString();
                _password = password.getText().toString();
                cekKoneksi();
            }
        }
    };

    private void cekKoneksi() {
        connDetect          = new ConnectionDetector(LoginActivity.this);
        isInternetPresent   = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            new getDataLogin().execute();
        } else {
            utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get data from server
    |-----------------------------------------------------------------------------------------------
    */
    private class getDataLogin extends AsyncTask<String, String, String> {
        private Boolean _status;
        private String _data, _access_token;

        private int _signal;
        private String _imei;
        private SharedPreferences preferences;
        private String _fcm_token;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            utils       = new Utils(LoginActivity.this);
            preferences = getSharedPreferences("fcm_token", Context.MODE_PRIVATE);
            _signal     = utils.checkSignal();
            _imei       = utils.getIMEI();
            _status     = false;

            utils.showDialogLoading("Loading . . .");
        }

        @Override
        protected String doInBackground(String... strings) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(LoginActivity.this);
                listLogLogin        = dbAdapterLogLogin.getAllData(LoginActivity.this);

                String _url         = ConnectionManager.CM_URL_LOGIN;
                _fcm_token          = preferences.getString("fcm_token", null);
                String _response    = ConnectionManager.requestLogin(_url, _username, _password, _imei, Config.version, String.valueOf(_signal), _fcm_token);
                Log.d("TSApp", "response= " + _response);

                JSONObject jsonObject = new JSONObject(_response.toString());
                if (jsonObject != null) {
                    _status         = jsonObject.getBoolean("status");
                    _data           = jsonObject.getString("data");
                    _access_token   = jsonObject.getString("access_token");

                    if (_status) {
                        dbAdapterLogLogin = new TableLogLoginAdapter(LoginActivity.this);
                        dbAdapterLogLogin.deleteAllData();

                        JSONObject objData      = new JSONObject(_data.toString());

                        String user_id          = objData.getString("user_id");
                        String user_name        = objData.getString("user_name");
                        String user_type        = "T";
                        //TDT String user_type        = objData.getString("user_type");
                        String employee_id      = objData.getString("employee_id");
                        String full_name        = objData.getString("full_name");
                        String role_code        = objData.getString("role_code");
                        String role_name        = objData.getString("role_name");
                        String sfl_code         = objData.getString("sfl_code");
                        String brand            = objData.getString("brand");
                        String branch_id        = objData.getString("branch_id");
                        String branch           = objData.getString("branch");
                        String region_code      = objData.getString("region_code");
                        String region_name      = objData.getString("region_name");
                        String is_supervisor    = objData.getString("is_supervisor");

                        if (user_type.toString().equalsIgnoreCase("T")) {
                            dbAdapterLogLogin = new TableLogLoginAdapter(LoginActivity.this);
                            dbAdapterLogLogin.insertData(new TableLogLogin(), user_id, user_name, user_type,
                                    employee_id, full_name, role_code, role_name, sfl_code, brand,
                                    branch_id, branch, region_code, region_name, is_supervisor, _access_token);
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (utils.progressdialog.isShowing())
                utils.progressdialog.dismiss();

            if (_status) {
                dbAdapterLogLogin   = new TableLogLoginAdapter(LoginActivity.this);
                listLogLogin        = dbAdapterLogLogin.getAllData(LoginActivity.this);

                if (listLogLogin.size() > 0) {
                    String usertype = listLogLogin.get(listLogLogin.size() - 1).getfUSER_TYPE().toUpperCase();

                    Log.i("Cobacoba", usertype);
                    if (usertype.toString().equalsIgnoreCase("T")) {
                        Intent i = new Intent(LoginActivity.this, MainMenuActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        username.setText("");
                        password.setText("");
                        username.requestFocus();
                        Toast.makeText(LoginActivity.this, "Login hanya dapat dilakukan oleh Teknisi", Toast.LENGTH_LONG).show();
                    }
                } else {
                    username.setText("");
                    password.setText("");
                    username.requestFocus();
                    Toast.makeText(LoginActivity.this, "Login hanya dapat dilakukan oleh Teknisi", Toast.LENGTH_LONG).show();
                }

            } else {
                if (_data == null || _data == "")
                {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                } else {
                    utils.showErrorDialog("Oops...", _data+"\n");
                }
            }
        }
    }
}