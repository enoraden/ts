package myskysfa.com.ts.main.sliding;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.db_adapter.TableLogLoginAdapter;
import myskysfa.com.ts.utils.ConnectionDetector;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.MasterAPI;
import myskysfa.com.ts.utils.Utils;

import org.json.JSONObject;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/27/2016.
|---------------------------------------------------------------------------------------------------
*/
public class Package extends Fragment {
    private Utils utils;
    private TextView brand, type, promo;

    private String _prospect_nbr;
    private SharedPreferences preferences;
    private ConnectionDetector connDetect;
    private Boolean isInternetPresent = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup viewGroup = (ViewGroup) inflater.inflate(R.layout.add_package, container, false);

        loadResources(viewGroup);
        loadListener();

        return viewGroup;
    }

    private void loadResources(ViewGroup root) {
        utils       = new Utils(getActivity());

        brand       = (TextView) root.findViewById(R.id.addpkg_brand);
        type        = (TextView) root.findViewById(R.id.addpkg_type);
        promo       = (TextView) root.findViewById(R.id.addpkg_promo);
    }

    private void loadListener() {
        brand.setText("-");
        type.setText("-");
        promo.setText("-");

        preferences         = getActivity().getSharedPreferences(getActivity().getString(R.string.detail_id), Context.MODE_PRIVATE);
        _prospect_nbr       = preferences.getString("ticket_number", null);

        connDetect          = new ConnectionDetector(getActivity());
        isInternetPresent   = connDetect.isConnectingToInternet();

        if (isInternetPresent) {
            new getChildPackage().execute();
        } else {
            utils.showErrorDialog("Offline", "Anda tidak memiliki koneksi internet, Silahkan cek koneksi internet anda dan dicoba kembali.\n");
        }
    }


    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data Child Package Server
    |-----------------------------------------------------------------------------------------------
    */
    private class getChildPackage extends AsyncTask<String, String, String> {
        private TableLogLoginAdapter dbAdapterLogLogin;
        private List<TableLogLogin> listLogLogin;

        private Boolean _status;
        private int _signal;
        private String _imei;

        private String js_brand, js_type, js_promo;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            utils       = new Utils(getActivity());
            _signal     = utils.checkSignal();
            _imei       = utils.getIMEI();
            _status     = false;
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                dbAdapterLogLogin   = new TableLogLoginAdapter(getActivity());
                listLogLogin        = dbAdapterLogLogin.getAllData(getActivity());

                if (listLogLogin.size() > 0) {
                    String _url             = ConnectionManager.CM_URL_DETAIL_PROFILE;
                    String _token           = listLogLogin.get(listLogLogin.size() - 1).getfACCESS_TOKEN();
                    String _paramurl        = _url + "/" + _prospect_nbr;

                    //String _response  = ConnectionManager.requestDataMaster(_paramurl, _imei, _token, Config.version, String.valueOf(_signal));
                    MasterAPI api       = new MasterAPI();
                    String _response    = api.detail_data_package();
                    Log.d("TSApp", "response= " + _response);

                    JSONObject jsonObject = new JSONObject(_response.toString());
                    if (jsonObject != null) {
                        _status = jsonObject.getBoolean("status");

                        if (_status) {
                            String _data        = jsonObject.getString("data");
                            JSONObject objData  = new JSONObject(_data.toString());

                            js_brand    = objData.getString("brand");
                            js_type     = objData.getString("type");
                            js_promo    = objData.getString("promo");
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_status) {
                    brand.setText(js_brand);
                    type.setText(js_type);
                    promo.setText(js_promo);
                } else {
                    utils.showErrorDialog("Koneksi Error", "Gagal terhubung ke server, silahkan dicoba kembali.\n");
                    Log.d("ErrorJSON", "detail_inst_troub");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}