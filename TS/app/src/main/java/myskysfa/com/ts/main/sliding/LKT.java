package myskysfa.com.ts.main.sliding;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceActivity;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import myskysfa.com.ts.R;
import myskysfa.com.ts.adapter.SpinnerArrayAdapter;
import myskysfa.com.ts.database.TableFormApp;
import myskysfa.com.ts.database.db_adapter.TableFormAppAdapter;
import myskysfa.com.ts.utils.AlbumStorageDirFactory;
import myskysfa.com.ts.utils.BaseAlbumDirFactory;
import myskysfa.com.ts.utils.Config;
import myskysfa.com.ts.utils.ConnectionManager;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/3/2016.
|---------------------------------------------------------------------------------------------------
*/
public class LKT extends Fragment {
    private Utils utils;
    private ViewGroup viewGroup;
    private TableFormAppAdapter  mFormAppAdapter;
    private String[] array_lokasiodu, array_ketinggianodu, array_tayanganuhf, array_instalasikabel,
            array_typelnbf, array_kondisidsd, array_resolvecode;

    private SpinnerArrayAdapter spinnerLokasiodu, spinnerKetinggianodu, spinnerTayanganuhf, spinnerInstalasikabel,
            spinnerTypelnbf, spinnerKondisidsd, spinnerResolvecode;
    private TextView lkt_jam_konfirmasi, lkt_jam_sd, lkt_kondisiawal, lkt_lokasiodu_awl, lkt_ketinggianodu_awl,
            lkt_tayanganuhf_awl, lkt_instalasikabel_awl, lkt_signalstrenght_awl, lkt_signalquality_awl,
            lkt_typelnbf_awl, lkt_kondisidsd_awl, lkt_perbaikansebelumnya_awl, lkt_inforeschedule_awl;

    private TextView dtl_jam_konfmulai, dtl_jam_konfselesai;
    private EditText dtl_signalstrenght_awl, dtl_signalquality_awl, dtl_perbaikansebelumnya_awl,
            dtl_inforeschedule_awl;

    private Spinner dtl_lokasiodu_awl, dtl_ketinggianodu_awl, dtl_tayanganuhf_awl, dtl_instalasikabel_awl, dtl_typelnbf_awl,
            dtl_kondisidsd_awl;
    private TextView lkt_kondisiakhir, lkt_lokasiodu_ahr, lkt_ketinggianodu_ahr, lkt_tayanganuhf_ahr,
            lkt_instalasikabel_ahr, lkt_signalstrenght_ahr, lkt_signalquality_ahr, lkt_typelnbf_ahr,
            lkt_kondisidsd_ahr, lkt_ticketstatus_ahr, lkt_resolvenote_ahr;
    private String signalstrenght_awl, signalquality_awl, perbaikansebelumnya_awl, inforeschedule_awl,
            signalstrenght_ahr, signalquality_ahr, resolvenote_ahr,
            dtl_lokasiodu_awlstring, dtl_ketinggianodu_awlstring, dtl_tayanganuhf_awlstring, dtl_instalasikabel_awlstring,
            dtl_typelnbf_awlstring, dtl_kondisidsd_awlstring, dtl_lokasiodu_ahr_string,
            dtl_ketinggianodu_ahr_string, dtl_tayanganuhf_ahr_string, dtl_instalasikabel_ahr_string,
            dtl_typelnbf_ahr_string, dtl_kondisidsd_ahr_string, dtl_ticketstatus_ahr_string,
            dtl_jam_konfirmasi_awl,dtl_jam_konfirmasi_ahr;
    private String jsonLKT;
    private EditText dtl_signalstrenght_ahr, dtl_signalquality_ahr, dtl_resolvenote_ahr;
    private Spinner dtl_lokasiodu_ahr, dtl_ketinggianodu_ahr, dtl_tayanganuhf_ahr, dtl_instalasikabel_ahr,
            dtl_typelnbf_ahr, dtl_kondisidsd_ahr, dtl_ticketstatus_ahr;
    private Button dtl_save;
    private File f;
    private TextView lkt_uploadphoto, lkt_instalasi, lkt_lainnya;
    private LinearLayout dtl_instalasi, dtl_lainnya;
    private ImageButton btn_instalasi, btn_lainnya;

    private Dialog dialogPreview;
    private TextView imgprv_tittle;
    private ImageView imgprv_preview;
    private Button imgprv_no, imgprv_yes;

    private LinearLayout mLayoutPackage;
    private static final int TAKE_PICTURE_INSTALASI = 333;
    private static final int TAKE_PICTURE_LAINNYA = 111;
    private static final int PICK_IMAGE_INSTALASI = 313;
    private static final int PICK_IMAGE_LAINNYA = 121;

    public static final int MEDIA_TYPE_IMAGE = 1;

    private Boolean isSendImage = false;
    private Uri fileUri;
    private static AlbumStorageDirFactory mAlbumStorageDirFactory = null;
    private String filePath = null, imageName, picturePath;
    private File myFile;
    private Bitmap bitmap;


    private static Bitmap bitmapInstalasi, bitmapLainnya;

    private static List<PathListItem> listPathInstalasi;
    private static List<PathListItem> listPathLainnya;


    private static ArrayList<String> instalasiArray = new ArrayList<String>();
    private static ArrayList<String> lainnyaArray = new ArrayList<String>();

    private static ArrayList<String> lainnya_name = new ArrayList<String>();
    private static ArrayList<String> instalasi_name = new ArrayList<String>();
    private String no_ticket;
    private String value_lkt;
    private int dataCount = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        viewGroup = (ViewGroup) inflater.inflate(R.layout.add_lkt, container, false);

        initView(viewGroup);
        loadListener();

        return viewGroup;
    }

    private void initView(ViewGroup root) {
        utils = new Utils(getActivity());
        mAlbumStorageDirFactory = new BaseAlbumDirFactory();

        lkt_jam_konfirmasi      = (TextView) root.findViewById(R.id.add_lkt_jam_konfirmasi);
        lkt_jam_sd              = (TextView) root.findViewById(R.id.add_lkt_jam_sd);

        lkt_uploadphoto         = (TextView) root.findViewById(R.id.add_lkt_uploadphoto);
        lkt_instalasi           = (TextView) root.findViewById(R.id.add_lkt_instalasi);
        dtl_instalasi           = (LinearLayout) root.findViewById(R.id.add_lktdtl_instalasi);
        btn_instalasi           = (ImageButton) root.findViewById(R.id.add_lktbtn_instalasi);

        lkt_lainnya             = (TextView) root.findViewById(R.id.add_lkt_lainnya);
        dtl_lainnya             = (LinearLayout) root.findViewById(R.id.add_lktdtl_lainnya);
        btn_lainnya             = (ImageButton) root.findViewById(R.id.add_lktbtn_lainnya);

        lkt_kondisiawal         = (TextView) root.findViewById(R.id.add_lkt_kondisiawal);
        lkt_lokasiodu_awl       = (TextView) root.findViewById(R.id.add_lkt_lokasiodu_awl);
        lkt_ketinggianodu_awl   = (TextView) root.findViewById(R.id.add_lkt_ketinggianodu_awl);
        lkt_tayanganuhf_awl     = (TextView) root.findViewById(R.id.add_lkt_tayanganuhf_awl);
        lkt_instalasikabel_awl  = (TextView) root.findViewById(R.id.add_lkt_instalasikabel_awl);
        lkt_signalstrenght_awl  = (TextView) root.findViewById(R.id.add_lkt_signalstrenght_awl);
        lkt_signalquality_awl   = (TextView) root.findViewById(R.id.add_lkt_signalquality_awl);
        lkt_typelnbf_awl        = (TextView) root.findViewById(R.id.add_lkt_typelnbf_awl);
        lkt_kondisidsd_awl      = (TextView) root.findViewById(R.id.add_lkt_kondisidsd_awl);
        lkt_perbaikansebelumnya_awl = (TextView) root.findViewById(R.id.add_lkt_perbaikansebelumnya_awl);
        lkt_inforeschedule_awl      = (TextView) root.findViewById(R.id.add_lkt_inforeschedule_awl);

        dtl_jam_konfmulai       = (TextView) root.findViewById(R.id.add_lktdtl_jam_konfmulai);
        dtl_jam_konfselesai     = (TextView) root.findViewById(R.id.add_lktdtl_jam_konfselesai);
        dtl_lokasiodu_awl       = (Spinner) root.findViewById(R.id.add_lktdtl_lokasiodu_awl);
        dtl_ketinggianodu_awl   = (Spinner) root.findViewById(R.id.add_lktdtl_ketinggianodu_awl);
        dtl_tayanganuhf_awl     = (Spinner) root.findViewById(R.id.add_lktdtl_tayanganuhf_awl);
        dtl_instalasikabel_awl  = (Spinner) root.findViewById(R.id.add_lktdtl_instalasikabel_awl);
        dtl_signalstrenght_awl  = (EditText) root.findViewById(R.id.add_lktdtl_signalstrenght_awl);
        dtl_signalquality_awl   = (EditText) root.findViewById(R.id.add_lktdtl_signalquality_awl);
        dtl_typelnbf_awl        = (Spinner) root.findViewById(R.id.add_lktdtl_typelnbf_awl);
        dtl_kondisidsd_awl      = (Spinner) root.findViewById(R.id.add_lktdtl_kondisidsd_awl);
        dtl_perbaikansebelumnya_awl = (EditText) root.findViewById(R.id.add_lktdtl_perbaikansebelumnya_awl);
        dtl_inforeschedule_awl      = (EditText) root.findViewById(R.id.add_lktdtl_inforeschedule_awl);

        lkt_kondisiakhir        = (TextView) root.findViewById(R.id.add_lkt_kondisiakhir);
        lkt_lokasiodu_ahr       = (TextView) root.findViewById(R.id.add_lkt_lokasiodu_ahr);
        lkt_ketinggianodu_ahr   = (TextView) root.findViewById(R.id.add_lkt_ketinggianodu_ahr);
        lkt_tayanganuhf_ahr     = (TextView) root.findViewById(R.id.add_lkt_tayanganuhf_ahr);
        lkt_instalasikabel_ahr  = (TextView) root.findViewById(R.id.add_lkt_instalasikabel_ahr);
        lkt_signalstrenght_ahr  = (TextView) root.findViewById(R.id.add_lkt_signalstrenght_ahr);
        lkt_signalquality_ahr   = (TextView) root.findViewById(R.id.add_lkt_signalquality_ahr);
        lkt_typelnbf_ahr        = (TextView) root.findViewById(R.id.add_lkt_typelnbf_ahr);
        lkt_kondisidsd_ahr      = (TextView) root.findViewById(R.id.add_lkt_kondisidsd_ahr);
        lkt_ticketstatus_ahr    = (TextView) root.findViewById(R.id.add_lkt_ticketstatus_ahr);
        lkt_resolvenote_ahr     = (TextView) root.findViewById(R.id.add_lkt_resolvenote_ahr);

        dtl_lokasiodu_ahr       = (Spinner) root.findViewById(R.id.add_lktdtl_lokasiodu_ahr);
        dtl_ketinggianodu_ahr   = (Spinner) root.findViewById(R.id.add_lktdtl_ketinggianodu_ahr);
        dtl_tayanganuhf_ahr     = (Spinner) root.findViewById(R.id.add_lktdtl_tayanganuhf_ahr);
        dtl_instalasikabel_ahr  = (Spinner) root.findViewById(R.id.add_lktdtl_instalasikabel_ahr);
        dtl_signalstrenght_ahr  = (EditText) root.findViewById(R.id.add_lktdtl_signalstrenght_ahr);
        dtl_signalquality_ahr   = (EditText) root.findViewById(R.id.add_lktdtl_signalquality_ahr);
        dtl_typelnbf_ahr        = (Spinner) root.findViewById(R.id.add_lktdtl_typelnbf_ahr);
        dtl_kondisidsd_ahr      = (Spinner) root.findViewById(R.id.add_lktdtl_kondisidsd_ahr);
        dtl_ticketstatus_ahr    = (Spinner) root.findViewById(R.id.add_lktdtl_ticketstatus_ahr);
        dtl_resolvenote_ahr     = (EditText) root.findViewById(R.id.add_lktdtl_resolvenote_ahr);

        dtl_save = (Button) root.findViewById(R.id.add_lktdtl_save);

        setFontType();
        setSpinner();
    }

    private void setFontType() {
        lkt_jam_konfirmasi.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_jam_sd.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));

        lkt_uploadphoto.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
        lkt_instalasi.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_lainnya.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));

        lkt_kondisiawal.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
        lkt_lokasiodu_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_ketinggianodu_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_tayanganuhf_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_instalasikabel_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_signalstrenght_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_signalquality_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_typelnbf_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_kondisidsd_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_perbaikansebelumnya_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_inforeschedule_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));

        dtl_jam_konfmulai.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_jam_konfselesai.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_signalstrenght_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_signalquality_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_perbaikansebelumnya_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_inforeschedule_awl.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));

        lkt_kondisiakhir.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
        lkt_lokasiodu_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_ketinggianodu_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_tayanganuhf_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_instalasikabel_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_signalstrenght_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_signalquality_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_typelnbf_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_kondisidsd_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_ticketstatus_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        lkt_resolvenote_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));

        dtl_signalstrenght_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_signalquality_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_resolvenote_ahr.setTypeface(utils.setFontType("ChampagneLimousines", "NORMAL"));
        dtl_save.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
    }

    private void setSpinner() {
        array_lokasiodu         = getResources().getStringArray(R.array.array_lokasiodu);
        spinnerLokasiodu        = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_lokasiodu);
        spinnerLokasiodu.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dtl_lokasiodu_awl.setAdapter(spinnerLokasiodu);
        dtl_lokasiodu_awl.setSelection(0);

        array_ketinggianodu     = getResources().getStringArray(R.array.array_ketinggianodu);
        spinnerKetinggianodu    = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_ketinggianodu);
        spinnerKetinggianodu.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dtl_ketinggianodu_awl.setAdapter(spinnerKetinggianodu);
        dtl_ketinggianodu_awl.setSelection(0);

        array_tayanganuhf       = getResources().getStringArray(R.array.array_tayanganuhf);
        spinnerTayanganuhf      = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_tayanganuhf);
        spinnerTayanganuhf.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dtl_tayanganuhf_awl.setAdapter(spinnerTayanganuhf);
        dtl_tayanganuhf_awl.setSelection(0);

        array_instalasikabel    = getResources().getStringArray(R.array.array_instalasikabel);
        spinnerInstalasikabel   = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_instalasikabel);
        spinnerInstalasikabel.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dtl_instalasikabel_awl.setAdapter(spinnerInstalasikabel);
        dtl_instalasikabel_awl.setSelection(0);

        array_typelnbf          = getResources().getStringArray(R.array.array_typelnbf);
        spinnerTypelnbf         = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_typelnbf);
        spinnerTypelnbf.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dtl_typelnbf_awl.setAdapter(spinnerTypelnbf);
        dtl_typelnbf_awl.setSelection(0);

        array_kondisidsd        = getResources().getStringArray(R.array.array_kondisidsd);
        spinnerKondisidsd       = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_kondisidsd);
        spinnerKondisidsd.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        dtl_kondisidsd_awl.setAdapter(spinnerKondisidsd);
        dtl_kondisidsd_awl.setSelection(0);
        dtl_lokasiodu_ahr.setAdapter(spinnerLokasiodu);
        dtl_lokasiodu_ahr.setSelection(0);
        dtl_ketinggianodu_ahr.setAdapter(spinnerKetinggianodu);
        dtl_ketinggianodu_ahr.setSelection(0);
        dtl_tayanganuhf_ahr.setAdapter(spinnerTayanganuhf);
        dtl_tayanganuhf_ahr.setSelection(0);
        dtl_instalasikabel_ahr.setAdapter(spinnerInstalasikabel);
        dtl_instalasikabel_ahr.setSelection(0);
        dtl_typelnbf_ahr.setAdapter(spinnerTypelnbf);
        dtl_typelnbf_ahr.setSelection(0);
        dtl_kondisidsd_ahr.setAdapter(spinnerKondisidsd);
        dtl_kondisidsd_ahr.setSelection(0);

        array_resolvecode   = getResources().getStringArray(R.array.array_resolvecode);
        spinnerResolvecode  = new SpinnerArrayAdapter(getActivity(), R.layout.spinner_textview, array_resolvecode);
        spinnerResolvecode.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dtl_ticketstatus_ahr.setAdapter(spinnerResolvecode);
        dtl_ticketstatus_ahr.setSelection(0);
    }

    private void loadListener() {
        dtl_jam_konfmulai.setOnClickListener(_dtl_jam_konfmulai);
        dtl_jam_konfselesai.setOnClickListener(_dtl_jam_konfselesai);
        btn_instalasi.setOnClickListener(_btn_instalasi);
        btn_lainnya.setOnClickListener(_btn_lainnya);
        dtl_save.setOnClickListener(_btn_save);
    }

    private View.OnClickListener _dtl_jam_konfmulai = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setTimeField(dtl_jam_konfmulai);
        }
    };

    private View.OnClickListener _dtl_jam_konfselesai = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            utils.setTimeField(dtl_jam_konfselesai);
        }
    };

    private View.OnClickListener _btn_instalasi = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TakeChooseImage(v);
        }
    };

    private View.OnClickListener _btn_lainnya = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            TakeChooseImage(v);
        }
    };
    private View.OnClickListener _btn_save = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
checkMandatoryForm();
        }
    };



    private void TakeChooseImage(final View view) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setMessage("Type of Upload")
                .setPositiveButton("Choose From File", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (view.getId()) {
                            case R.id.add_lktbtn_instalasi:
                                selectImageFromGallery(PICK_IMAGE_INSTALASI);
                                break;
                            case R.id.add_lktbtn_lainnya:
                                selectImageFromGallery(PICK_IMAGE_LAINNYA);
                                break;
                        }
                    }
                })
                .setNegativeButton("Take Picture", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (view.getId()) {
                            case R.id.add_lktbtn_instalasi:
                                takeImage(TAKE_PICTURE_INSTALASI);
                                break;
                            case R.id.add_lktbtn_lainnya:
                                takeImage(TAKE_PICTURE_LAINNYA);
                                break;
                        }
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }



    public void selectImageFromGallery(int code) {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"),
                code);
    }


    public void takeImage(int code) {
        try {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            f = null;
            f = createImageFile(code);
            picturePath = f.getAbsolutePath();
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
            startActivityForResult(takePictureIntent, code);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private File createImageFile(int code) throws IOException {
        String imageFileName = "";
        switch (code) {
            case TAKE_PICTURE_INSTALASI:
                imageFileName = no_ticket + "_instalasi" + "_";
                break;
            case TAKE_PICTURE_LAINNYA:
                imageFileName = no_ticket + "_lainnya" + "_";
                break;
        }

        File albumF = getAlbumDir();
        File imageF = File.createTempFile(imageFileName, ".jpg", albumF);
        return imageF;
    }

    private File getAlbumDir() {
        File storageDir = null;

        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            storageDir = mAlbumStorageDirFactory.getAlbumStorageDir("TS");
            if (storageDir != null) {
                if (!storageDir.mkdirs()) {
                    if (!storageDir.exists()) {
                        Log.e("CameraSample", "failed to create directory");
                        return null;
                    }
                }
            }

        } else {
            Log.e(getString(R.string.app_name)+"Aa", "External storage is not mounted READ/WRITE.");
        }

        return storageDir;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("requestCode", String.valueOf(requestCode));
        switch (requestCode) {
            case (PICK_IMAGE_INSTALASI):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(requestCode, data);
                }
                break;
            case (PICK_IMAGE_LAINNYA):
                if (resultCode == Activity.RESULT_OK) {
                    GetPickImage(requestCode, data);
                }
                break;
            case (TAKE_PICTURE_INSTALASI):
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathInstalasi = new ArrayList<PathListItem>();
                        setPic(bitmapInstalasi, TAKE_PICTURE_INSTALASI);
                        galleryAddPic();
                    }
                }
                break;
            case TAKE_PICTURE_LAINNYA:
                if (resultCode == Activity.RESULT_OK) {
                    {
                        listPathLainnya = new ArrayList<PathListItem>();
                        setPic(bitmapLainnya, TAKE_PICTURE_LAINNYA);
                        galleryAddPic();

                    }
                }
                break;
        }
    }


    private void setPic(Bitmap bitmap, final int code) {
        /* Get the size of the image */
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(picturePath, bmOptions);
        bmOptions.inJustDecodeBounds = false;
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = bmOptions.outWidth, height_tmp = bmOptions.outHeight;
        final int finalWidth_tmp = width_tmp;
        final int finalHeight_tmp = height_tmp;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }

        bmOptions.inSampleSize = scale;
        /* Decode the JPEG file into a Bitmap */
        bitmap = BitmapFactory.decodeFile(picturePath, bmOptions);

        if (finalWidth_tmp > Config.width && finalHeight_tmp > Config.height) {
            Toast.makeText(getActivity(), "Image harus 2 MP", Toast.LENGTH_SHORT).show();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View root = inflater.inflate(R.layout.image_preview, null);
            imgprv_preview = (ImageView) root.findViewById(R.id.imgprv_preview);
            LinearLayout imgprv_button_lyt = (LinearLayout)root.findViewById(R.id.imgprv_button_lyt);
            imgprv_button_lyt.setVisibility(View.GONE);
            alertDialog.setView(root);
            alertDialog.setTitle("Image Preview");
            alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    File file = new File(picturePath);
                    imageName = file.getName();
                    switch (code) {
                        case TAKE_PICTURE_INSTALASI:
                            addViewImageList(TAKE_PICTURE_INSTALASI, instalasiArray, imageName, picturePath);
                            break;
                        case TAKE_PICTURE_LAINNYA:
                            addViewImageList(TAKE_PICTURE_LAINNYA, lainnyaArray, imageName, picturePath);
                            break;
                    }

                }
            });
            alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //Cancel set Visible Bitmap
                }
            });

            imgprv_preview.setImageBitmap(bitmap);
            imgprv_preview.setVisibility(View.VISIBLE);
            alertDialog.show();
        }
    }

    private void galleryAddPic() {
        Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
        File f = new File(picturePath);
        Uri contentUri = Uri.fromFile(f);
        mediaScanIntent.setData(contentUri);
        getActivity().sendBroadcast(mediaScanIntent);
    }

    private void GetPickImage(int requestCode, Intent data) {
        if (data != null) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn,
                    null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picturePath = cursor.getString(columnIndex);
            cursor.close();
            //Get Image Name
            File f = new File(picturePath);
            imageName = f.getName();
            //afterName = f.getName();
            decodeFile(picturePath, requestCode);
        }

    }

    public void decodeFile(final String filePath, final int code) {
        Log.d("LKT", "code=" + code);
        // Decode ukuran gambar
        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filePath, o);
// The new size we want to scale to
        final int REQUIRED_SIZE = 1024;
        // Find the correct scale value. It should be the power of 2.
        int width_tmp = o.outWidth, height_tmp = o.outHeight;
        final int finalWidth_tmp = width_tmp;
        final int finalHeight_tmp = height_tmp;
        int scale = 1;
        while (true) {
            if (width_tmp < REQUIRED_SIZE && height_tmp < REQUIRED_SIZE)
                break;
            width_tmp /= 2;
            height_tmp /= 2;
            scale *= 2;
        }
// Decode with inSampleSize
        final BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;

        if (finalWidth_tmp > Config.width && finalHeight_tmp > Config.height) {
            Toast.makeText(getActivity(), "Image harus 2 MP", Toast.LENGTH_SHORT).show();
        } else {
            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            final View root = inflater.inflate(R.layout.image_preview, null);
            LinearLayout imgprv_button_lyt = (LinearLayout)root.findViewById(R.id.imgprv_button_lyt);
            imgprv_button_lyt.setVisibility(View.GONE);
            alertDialog.setView(root);
            alertDialog.setTitle("Image Preview");
            alertDialog.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    if (code == PICK_IMAGE_INSTALASI) {
                        listPathInstalasi = new ArrayList<PathListItem>();
                        addViewImageList(PICK_IMAGE_INSTALASI, instalasiArray,
                                imageName, picturePath);
                    } else if (code == PICK_IMAGE_LAINNYA) {
                        listPathLainnya = new ArrayList<PathListItem>();
                        addViewImageList(PICK_IMAGE_LAINNYA, lainnyaArray,
                                imageName, picturePath);
                    } else {
                    }
                }
            });
            alertDialog.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    //Cancel set Visible Bitmap
                }
            });
            imgprv_preview = (ImageView) root.findViewById(R.id.imgprv_preview);
            imgprv_preview.setVisibility(View.VISIBLE);
            if (code == PICK_IMAGE_INSTALASI) {
                bitmapInstalasi = BitmapFactory.decodeFile(filePath, o2);
                imgprv_preview.setImageBitmap(bitmapInstalasi);
            } else if (code == PICK_IMAGE_LAINNYA) {
                bitmapLainnya = BitmapFactory.decodeFile(filePath, o2);
                imgprv_preview.setImageBitmap(bitmapLainnya);
            }
            alertDialog.show();
        }
    }



    private void addViewImageList(int code, final ArrayList<String> arrayListImage, String imageName, String picturePath) {
        switch (code) {
            case PICK_IMAGE_INSTALASI:
                mLayoutPackage = (LinearLayout) viewGroup.findViewById(R.id.add_lktdtl_instalasi);
                break;
            case PICK_IMAGE_LAINNYA:
                mLayoutPackage = (LinearLayout) viewGroup.findViewById(R.id.add_lktdtl_lainnya);
                break;
             case TAKE_PICTURE_INSTALASI:
                mLayoutPackage = (LinearLayout) viewGroup.findViewById(R.id.add_lktdtl_instalasi);
                break;
            case TAKE_PICTURE_LAINNYA:
                mLayoutPackage = (LinearLayout) viewGroup.findViewById(R.id.add_lktdtl_lainnya);
                break;
               }
        final LinearLayout Mainlinear = new LinearLayout(getContext());
        Mainlinear.setBackgroundColor(getResources().getColor(R.color.white));
        Mainlinear.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams pLinearPackage = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        pLinearPackage.setMargins(0, 20, 0, 0);
        Mainlinear.setLayoutParams(pLinearPackage);
        mLayoutPackage.addView(Mainlinear);


        //Liner Title Image
        LinearLayout.LayoutParams pLineTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        LinearLayout linearTitle = new LinearLayout(getContext());
        linearTitle.setLayoutParams(pLineTitle);
        linearTitle.setOrientation(LinearLayout.HORIZONTAL);
        linearTitle.setPadding(10, 0, 10, 0);
        Mainlinear.addView(linearTitle);

        LinearLayout.LayoutParams pTxtTitle = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.7f);
        final TextView txtTitle = new TextView(getContext());
        txtTitle.setLayoutParams(pTxtTitle);
        txtTitle.setText(imageName);
        txtTitle.setTag(picturePath);
        txtTitle.setTextSize(10);
        txtTitle.setFreezesText(true);
        txtTitle.setPadding(5, 25, 0, 0);
        linearTitle.addView(txtTitle);


        //Linear Cancel Button
        LinearLayout.LayoutParams pLineCancel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.3f);
        LinearLayout linearCancel = new LinearLayout(getContext());
        linearTitle.setLayoutParams(pLineCancel);
        linearTitle.setOrientation(LinearLayout.HORIZONTAL);
        linearTitle.addView(linearCancel);

        LinearLayout.LayoutParams pBtnCancel = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        ImageButton btnCancel = new ImageButton(getContext());
        btnCancel.setLayoutParams(pBtnCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String m = txtTitle.getTag().toString();
                Mainlinear.removeAllViews();
                int i = arrayListImage.indexOf(m);
                arrayListImage.remove(i);
            }
        });
        btnCancel.setBackgroundResource(R.drawable.ic_close_light);
        btnCancel.setPadding(0, 16, 0, 16);
        ((LinearLayout) linearCancel).addView(btnCancel);
        PathListItem item = new PathListItem();
        item.setPath(picturePath);
        item.setImageName(imageName);
        switch (code) {
            case PICK_IMAGE_INSTALASI:
                listPathInstalasi.add(item);
                ListInstalasi();
                break;
            case PICK_IMAGE_LAINNYA:
                listPathLainnya.add(item);
                ListLainnya();
                break;
            case TAKE_PICTURE_INSTALASI:
                listPathInstalasi.add(item);
                ListInstalasi();
                break;
            case TAKE_PICTURE_LAINNYA:
                listPathLainnya.add(item);
                ListLainnya();
                break;
        }
    }


    private void ListInstalasi() {
        try {
            instalasiArray.add(picturePath);
            instalasi_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void ListLainnya() {
        try {
            lainnyaArray.add(picturePath);
            lainnya_name.add(imageName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public class PathListItem {
        String imageName;
        String path;

        public PathListItem() {
            super();
        }

        public void setPath(String path) {
            this.path = path;
        }

        public void setImageName(String imageName) {
            this.imageName = imageName;
        }

        public String getImageName() {
            return imageName;
        }

        public String getPath() {
            return path;
        }
    }

    private void saveImage() {
        String fileName, filePath;
        if (instalasiArray.size() > 0) {
            LinearLayout mInstal = (LinearLayout) viewGroup.findViewById(R.id.add_lktdtl_instalasi);
            for (int a = 0; a < instalasiArray.size(); a++) {
                filePath = instalasiArray.get(a);
                fileName = instalasi_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtInstal = (TextView) mInstal.findViewWithTag(filePath);
                txtInstal.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtInstal);
            }
        }
        if (lainnyaArray.size() > 0) {
            LinearLayout mLing = (LinearLayout) viewGroup.findViewById(R.id.add_lktdtl_lainnya);
            for (int a = 0; a < lainnyaArray.size(); a++) {
                filePath = lainnyaArray.get(a);
                fileName = lainnya_name.get(a);
                System.out.println("running " + fileName);
                System.out.println("filePath " + filePath);
                TextView txtLing = (TextView) mLing.findViewWithTag(filePath);
                txtLing.setText(fileName + " 0%");
                uploadImage(filePath, fileName, txtLing);
            }
        }

    }


       /*private void checkImage(String fileName, Boolean status) {
        Boolean findLainnya = utils.findStringImage(fileName, "lainnya");
        Boolean findInstall = utils.findStringImage(fileName, "instalasi");


        if (findLainnya) {
            if (status) {
                InstalasiAdd.isExistLainnya = true;
            } else {
                InstalasiAdd.isExistLainnya = false;
            }
        }

        if (findInstall) {
            if (status) {
                InstalasiAdd.isExistInstall = true;
            } else {
                InstalasiAdd.isExistInstall = false;
            }
        }
    }*/



    private void uploadImage(final String filePath, final String fileName, final TextView txtCent) {
        try {
            String url = ConnectionManager.CM_URL_UPLOAD_IMAGE + no_ticket;
            System.out.println(url);
            final File myFile = new File(filePath);
            RequestParams params = new RequestParams();
            params.put("image", myFile);
            AsyncHttpClient client = new AsyncHttpClient();
            client.setTimeout(300000);
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onProgress(long bytesWritten, long totalSize) {
                    super.onProgress(bytesWritten, totalSize);
                    int writen = (int) bytesWritten;
                    float proportionCorrect = ((float) writen) / ((float) myFile.length());
                    float percentFl = proportionCorrect * 100;
                    System.out.println("percentFl " + String.format("%.0f", percentFl));
                    if (!String.format("%.0f", percentFl).equalsIgnoreCase("0")) {
                        if (percentFl > 100) {
                            percentFl = 100;
                            txtCent.setText(fileName + " " + String.format("%.0f", percentFl) + " %");
                        }

                    }
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    isSendImage = true;
                    System.out.println("onSuccess " + filePath);
                    Toast.makeText(getContext(), "Success upload " + fileName, Toast.LENGTH_LONG).show();
                    //checkImage(fileName, true);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    System.out.println("onFailure " + filePath);
                    txtCent.setText(fileName + " 0%");
                    String responseString = "Error occurred! Http Status Code: " + statusCode + " " + fileName;
                    Toast.makeText(getContext(), responseString, Toast.LENGTH_LONG).show();
                    //checkImage(fileName, false);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private JSONArray CreateImageJson() {
        JSONArray array = new JSONArray();
        try {


            for (int j = 0; j < instalasi_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "instalasi");
                list.put("image_name", instalasi_name.get(j));
                array.put(list);
            }

            for (int j = 0; j < lainnya_name.size(); j++) {
                JSONObject list = new JSONObject();
                list.put("image_type_id", "lainnya");
                list.put("image_name", lainnya_name.get(j));
                array.put(list);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return array;
    }


    private void checkMandatoryForm() {
        dtl_jam_konfirmasi_awl = dtl_jam_konfmulai.getText().toString();
        dtl_jam_konfirmasi_ahr = dtl_jam_konfselesai.getText().toString();

        signalstrenght_awl = dtl_signalquality_awl.getText().toString();
        signalquality_awl = dtl_signalquality_awl.getText().toString();
        perbaikansebelumnya_awl = dtl_perbaikansebelumnya_awl.getText().toString();
        inforeschedule_awl = dtl_inforeschedule_awl.getText().toString();

        signalstrenght_ahr = dtl_signalstrenght_ahr.getText().toString();
        signalquality_ahr = dtl_signalstrenght_ahr.getText().toString();
        resolvenote_ahr = dtl_signalquality_ahr.getText().toString();

        dtl_lokasiodu_awlstring = dtl_lokasiodu_awl.getSelectedItem().toString();
        dtl_ketinggianodu_awlstring = dtl_ketinggianodu_awl.getSelectedItem().toString();
        dtl_tayanganuhf_awlstring = dtl_tayanganuhf_awl.getSelectedItem().toString();
        dtl_instalasikabel_awlstring = dtl_instalasikabel_awl.getSelectedItem().toString();
        dtl_typelnbf_awlstring = dtl_typelnbf_awl.getSelectedItem().toString();
        dtl_kondisidsd_awlstring = dtl_kondisidsd_awl.getSelectedItem().toString();
        dtl_lokasiodu_ahr_string = dtl_lokasiodu_ahr.getSelectedItem().toString();
        dtl_ketinggianodu_ahr_string = dtl_ketinggianodu_ahr.getSelectedItem().toString();
        dtl_tayanganuhf_ahr_string = dtl_tayanganuhf_ahr.getSelectedItem().toString();
        dtl_instalasikabel_ahr_string = dtl_instalasikabel_ahr.getSelectedItem().toString();
        dtl_typelnbf_ahr_string = dtl_typelnbf_ahr.getSelectedItem().toString();
        dtl_kondisidsd_ahr_string = dtl_kondisidsd_ahr.getSelectedItem().toString();
        dtl_ticketstatus_ahr_string = dtl_ticketstatus_ahr.getSelectedItem().toString();
        if (dtl_jam_konfirmasi_awl == null || dtl_jam_konfirmasi_awl.equalsIgnoreCase("00:00") || dtl_jam_konfirmasi_awl.equals("00:00")) {
            Toast.makeText(getActivity(), "Silahkan pilih Waktu Mulai.", Toast.LENGTH_SHORT).show();
            return;
        } else if (dtl_jam_konfirmasi_ahr == null || dtl_jam_konfirmasi_ahr.equalsIgnoreCase("00:00") || dtl_jam_konfirmasi_ahr.equals("00:00")) {
            Toast.makeText(getActivity(), "Silahkan pilih Waktu Selesai.", Toast.LENGTH_SHORT).show();
            return;
        } else if (signalstrenght_awl == null || signalstrenght_awl.equalsIgnoreCase("") || signalstrenght_awl.length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukan signal strenght (SS).", Toast.LENGTH_SHORT).show();
            return;
        } else if (signalquality_awl == null || signalquality_awl.equalsIgnoreCase("") || signalquality_awl.length() <= 0) {
            Toast.makeText(getActivity(), "Silahkan masukan signal quality (SQ).", Toast.LENGTH_SHORT).show();
            return;
        } else if (perbaikansebelumnya_awl == null || perbaikansebelumnya_awl.equalsIgnoreCase("") || perbaikansebelumnya_awl.length() < 0) {
            Toast.makeText(getActivity(), "Silahkan masukan perbaikan sebelumnya.", Toast.LENGTH_SHORT).show();
            return;
        } else if (inforeschedule_awl == null || inforeschedule_awl.equalsIgnoreCase("") || inforeschedule_awl.length() < 0) {
            Toast.makeText(getActivity(), "Silahkan masukan info reschedule.", Toast.LENGTH_SHORT).show();
            return;
        } else if (signalstrenght_ahr == null || signalstrenght_ahr.equalsIgnoreCase("") || signalstrenght_ahr.length() < 0) {
            Toast.makeText(getActivity(), "Silahkan masukan signal strenght (SS).", Toast.LENGTH_SHORT).show();
            return;
        } else if (signalquality_ahr == null || signalquality_ahr.equalsIgnoreCase("") || signalquality_ahr.length() < 0) {
            Toast.makeText(getActivity(), "Silahkan masukan signal quality (SQ).", Toast.LENGTH_SHORT).show();
            return;
        } else if (resolvenote_ahr == null || resolvenote_ahr.equalsIgnoreCase("") || resolvenote_ahr.length() < 0) {
            Toast.makeText(getActivity(), "Silahkan masukan Resolve note.", Toast.LENGTH_SHORT).show();
            return;
        }  else {

            try {
                jsonLKT = jsonValueCreator();
            } catch (Exception e) {
                e.printStackTrace();
            }

            Toast.makeText(getActivity(), jsonLKT, Toast.LENGTH_SHORT).show();

            try {
                if (no_ticket != null) {
                    mFormAppAdapter = new TableFormAppAdapter(getActivity());
                    Long count = mFormAppAdapter.getDataCount(TableFormApp.fNO_TICKET, no_ticket);
                    if (count != null) {
                        dataCount = (int) (long) count;
                    } else {
                        dataCount = 0;
                    }
                } else {
                    utils.showErrorDialog("Gagal", "Silahkan isi data LKT terlebih dahulu.");
                    return;
                }

                if (dataCount == 0) {
                    utils.showErrorDialog("Gagal", "Silahkan isi data LKT terlebih dahulu.");
                } else {
                    value_lkt = jsonValueCreator();
                    mFormAppAdapter.updatePartial(getActivity(), TableFormApp.fVALUES_LKT, value_lkt,
                            TableFormApp.fNO_TICKET, no_ticket);
                    saveImage();
                    Toast.makeText(getActivity(), "Data Saved", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }




    private String jsonValueCreator() throws Exception {
        JSONObject Mainobj = new JSONObject();
        JSONObject obj = new JSONObject();

        try {


            obj.put("no_ticket", no_ticket);
            obj.put("dtl_jam_konfirmasi_awl", dtl_jam_konfirmasi_awl);
            obj.put("dtl_jam_konfirmasi_ahr",dtl_jam_konfirmasi_ahr);
            obj.put("signalstrenght_awal", dtl_signalquality_awl.getText().toString());
            obj.put("signalquality_awal", dtl_signalquality_awl.getText().toString());
            obj.put("perbaikansebelumnya_awl", dtl_perbaikansebelumnya_awl.getText().toString());
            obj.put("inforeschedule_awl", dtl_inforeschedule_awl.getText().toString());

            obj.put("signalstrenght_ahr", dtl_signalstrenght_ahr.getText().toString());
            obj.put("signalquality_ahr", dtl_signalstrenght_ahr.getText().toString());
            obj.put("resolvenote_ahr", dtl_signalquality_ahr.getText().toString());


            obj.put("dtl_lokasiodu_awl", dtl_lokasiodu_awl.getSelectedItem().toString());
            obj.put("dtl_ketinggianodu_awl", dtl_ketinggianodu_awl.getSelectedItem().toString());
            obj.put("dtl_tayanganuhf_awl", dtl_tayanganuhf_awl.getSelectedItem().toString());
            obj.put("dtl_instalasikabel_awl", dtl_instalasikabel_awl.getSelectedItem().toString());
            obj.put("dtl_typelnbf_awl", dtl_typelnbf_awl.getSelectedItem().toString());
            obj.put("dtl_kondisidsd_awl", dtl_kondisidsd_awl.getSelectedItem().toString());
            obj.put("dtl_lokasiodu_ahr ", dtl_lokasiodu_ahr.getSelectedItem().toString());
            obj.put("dtl_ketinggianodu_ahr",dtl_ketinggianodu_ahr.getSelectedItem().toString());
            obj.put("dtl_tayanganuhf_ahr", dtl_tayanganuhf_ahr.getSelectedItem().toString());
            obj.put("dtl_instalasikabel_ahr", dtl_instalasikabel_ahr.getSelectedItem().toString());
            obj.put("dtl_typelnbf_ahr", dtl_typelnbf_ahr.getSelectedItem().toString());
            obj.put("dtl_kondisidsd_ahr", dtl_kondisidsd_ahr.getSelectedItem().toString());
            obj.put("dtl_ticketstatus_ahr", dtl_ticketstatus_ahr.getSelectedItem().toString());
            obj.put("image", CreateImageJson());
            Mainobj.put("lkt",obj);

            Log.e("json_LKT",Mainobj.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return obj.toString();
    }


    public void setHideKeyBoard() {
        utils.setHideKeyboard(getActivity(), viewGroup);
    }

    public void setNoTicket() {
        SharedPreferences sm = getActivity().getSharedPreferences(getString(R.string.no_ticket),
                Context.MODE_PRIVATE);
        no_ticket = sm.getString("no_ticket", null);
    }



}
