package myskysfa.com.ts.database.db_adapter;

import android.content.Context;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.DeleteBuilder;
import com.j256.ormlite.stmt.QueryBuilder;
import com.j256.ormlite.stmt.UpdateBuilder;
import com.j256.ormlite.stmt.Where;
import myskysfa.com.ts.database.TableLogLogin;
import myskysfa.com.ts.database.TablePlan;
import myskysfa.com.ts.utils.DatabaseManager;

import java.util.List;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/7/2016.
|---------------------------------------------------------------------------------------------------
*/
public class TablePlanAdapter {
    private Dao dao;
    private DatabaseManager helper;

    public TablePlanAdapter(Context ctx) {
        helper = new DatabaseManager(ctx);
    }

    private DatabaseManager getHelper() {
        return helper;
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Get Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public List<TablePlan> getDatabyCondition(Context context, String condition, Object param) {
        List<TablePlan> listTable = null;

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TablePlan.class);

            QueryBuilder<TablePlan, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePlan, Integer> where = queryBuilder.where();
            where.eq(condition, param);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public List<TablePlan> getAllData(Context context) {
        List<TablePlan> listTable = null;
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TablePlan.class);

            QueryBuilder<TablePlan, Integer> queryBuilder = dao.queryBuilder();

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }

    public  List<TablePlan> getDatabySfl(Context context, String condition, Object param) {
        List<TablePlan> listTable = null;

        TableLogLoginAdapter dbAdapter      = new TableLogLoginAdapter(context);
        List<TableLogLogin> listLogLogin    = dbAdapter.getAllData(context);
        String SFL_CODE = "";
        if (listLogLogin.size() > 0) {
            SFL_CODE = listLogLogin.get(listLogLogin.size() - 1).getfUSER_NAME();
        }

        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TablePlan.class);

            QueryBuilder<TablePlan, Integer> queryBuilder = dao.queryBuilder();
            Where<TablePlan, Integer> where = queryBuilder.where();
            where.eq(TablePlan.fSFL_CODE, SFL_CODE).and().eq(condition, param);
            queryBuilder.orderBy(TablePlan.fPLAN_ID, false);

            listTable = queryBuilder.query();

        } catch (Exception e) {
            e.printStackTrace();
        }

        return listTable;
    }


    /*
    |-----------------------------------------------------------------------------------------------
    | Insert Data
    |-----------------------------------------------------------------------------------------------
    */
    public void insertData(TablePlan table, String plan_id, String plan_date, String zip_code, String area,
                           String ts_code, String ts_name, String target, String sfl_code) {
        try {
            table.setfPLAN_ID(plan_id);
            table.setfPLAN_DATE(plan_date);
            table.setfZIP_CODE(zip_code);
            table.setfAREA(area);
            table.setfTS_CODE(ts_code);
            table.setfTS_NAME(ts_name);
            table.setfTARGET(target);
            table.setfSFL_CODE(sfl_code);
            getHelper().getTablePlanDAO().create(table);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Update Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void updateDatabyCondition(Context context, String column, Object value, String condition,
                                      Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TablePlan.class);

            UpdateBuilder<TablePlan, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.updateColumnValue(column, value);
            updateBuilder.where().eq(condition, param);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateAllData(Context context, String plan_id, String plan_date, String zip_code,
                              String area, String ts_code, String ts_name, String target, String sfl_code,
                              String condition, Object param) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TablePlan.class);

            UpdateBuilder<TablePlan, Integer> updateBuilder = dao.updateBuilder();
            updateBuilder.where().eq(condition, param);
            updateBuilder.updateColumnValue(TablePlan.fPLAN_ID, plan_id);
            updateBuilder.updateColumnValue(TablePlan.fPLAN_DATE, plan_date);
            updateBuilder.updateColumnValue(TablePlan.fZIP_CODE, zip_code);
            updateBuilder.updateColumnValue(TablePlan.fAREA, area);
            updateBuilder.updateColumnValue(TablePlan.fTS_CODE, ts_code);
            updateBuilder.updateColumnValue(TablePlan.fTS_NAME, ts_name);
            updateBuilder.updateColumnValue(TablePlan.fTARGET, target);
            updateBuilder.updateColumnValue(TablePlan.fSFL_CODE, sfl_code);
            updateBuilder.update();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    |-----------------------------------------------------------------------------------------------
    | Delete Data by Condition
    |-----------------------------------------------------------------------------------------------
    */
    public void deleteDatabyCondition(Context context, String condition, Object value) {
        try {
            helper  = OpenHelperManager.getHelper(context, DatabaseManager.class);
            dao     = helper.getDao(TablePlan.class);

            DeleteBuilder<TablePlan, Integer> deleteBuilder = dao.deleteBuilder();
            deleteBuilder.where().eq(condition, value);
            deleteBuilder.delete();
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    public void deleteAllData() {
        List<TablePlan> listTable = null;
        try {
            listTable = getHelper().getTablePlanDAO().queryForAll();
            getHelper().getTablePlanDAO().delete(listTable);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}