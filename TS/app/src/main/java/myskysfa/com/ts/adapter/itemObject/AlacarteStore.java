package myskysfa.com.ts.adapter.itemObject;

import java.util.ArrayList;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 11/7/2016.
|---------------------------------------------------------------------------------------------------
*/
public class AlacarteStore {
    private ArrayList<String> alaList            = new ArrayList<String>();
    private ArrayList<ArrayList<String>> alaSave = new ArrayList<>();

    public AlacarteStore() {
    }

    public void setList(String alaData) {
        this.alaList.add(alaData);
    }

    public void setAllList(String alaData, int position) {
        this.alaList.add(alaData);
        this.alaSave.add(position, alaList);
    }

    public ArrayList<String> getList() {
        return alaList;
    }

    public ArrayList<ArrayList<String>> getAllList() {
        return alaSave;
    }

    public void removeAlacarte(String remData) {
        for (int i = 0; i < alaList.size(); i++) {
            if (alaList.get(i).toString().equals(remData)) {
                this.alaList.remove(i);
            }
        }
    }

    public void clearAll() {
        this.alaList.clear();
    }

}