package myskysfa.com.ts.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;

import android.widget.RelativeLayout;
import android.widget.TextView;

import myskysfa.com.ts.R;
import myskysfa.com.ts.main.childview.ChildEmergency;
import myskysfa.com.ts.main.childview.ChildPackage;
import myskysfa.com.ts.main.childview.ChildPayment;
import myskysfa.com.ts.main.childview.ChildProfile;
import myskysfa.com.ts.utils.Utils;

/*
|---------------------------------------------------------------------------------------------------
| Created by TDT on 10/25/2016.
|---------------------------------------------------------------------------------------------------
*/
public class ListMainDetailAdapter extends BaseExpandableListAdapter {
    private Context context;
    private String[] groupsItemData;
    private String[][] childItemData;

    public ListMainDetailAdapter(Context _context, String[] _groupsItemData, String[][] _childItemData) {
        this.context        = _context;
        this.groupsItemData = _groupsItemData;
        this.childItemData  = _childItemData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return childItemData[groupPosition][childPosition];
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return childItemData[groupPosition].length;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        ViewHolder holder;
        View viewChild;
        Utils utils = new Utils(context);

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_m_dtl_child, parent, false);
            holder      = new ViewHolder();
            holder.child_layoutchild        = (RelativeLayout) convertView.findViewById(R.id.child_layoutchild);
            holder.child_textview           = (TextView) convertView.findViewById(R.id.child_textview);
            holder.child_textview.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        String childitem = String.valueOf(childItemData[groupPosition][childPosition]);

        if (childitem.toString().equalsIgnoreCase("profile")){
            holder.child_layoutchild.removeAllViews();
            holder.child_layoutchild.setVisibility(View.VISIBLE);
            holder.child_textview.setVisibility(View.GONE);

            ChildProfile profile    = new ChildProfile(context);
            viewChild               = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_profile, parent, false);
            holder.child_layoutchild.addView(viewChild);
            profile.profileView(viewChild);

        } else if (childitem.toString().equalsIgnoreCase("emergency")) {
            holder.child_layoutchild.removeAllViews();
            holder.child_layoutchild.setVisibility(View.VISIBLE);
            holder.child_textview.setVisibility(View.GONE);

            ChildEmergency emergency    = new ChildEmergency(context);
            viewChild                   = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_emergency, parent, false);
            holder.child_layoutchild.addView(viewChild);
            emergency.emergencyView(viewChild);

        } else if (childitem.toString().equalsIgnoreCase("package")) {
            holder.child_layoutchild.removeAllViews();
            holder.child_layoutchild.setVisibility(View.VISIBLE);
            holder.child_textview.setVisibility(View.GONE);

            viewChild               = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_package, parent, false);
            ChildPackage cpackage   = new ChildPackage(context, viewChild);
            holder.child_layoutchild.addView(viewChild);
            cpackage.packageView();

        } else if (childitem.toString().equalsIgnoreCase("payment")) {
            holder.child_layoutchild.removeAllViews();
            holder.child_layoutchild.setVisibility(View.VISIBLE);
            holder.child_textview.setVisibility(View.GONE);

            ChildPayment payment    = new ChildPayment(context);
            viewChild               = LayoutInflater.from(parent.getContext()).inflate(R.layout.child_payment, parent, false);
            holder.child_layoutchild.addView(viewChild);
            payment.paymentView(viewChild);

        } else {
            holder.child_layoutchild.removeAllViews();
            holder.child_layoutchild.setVisibility(View.GONE);
            holder.child_textview.setVisibility(View.VISIBLE);

            holder.child_textview.setText("NO DATA");
        }

        return convertView;
    }

    //----------------------------------------------------------------------------------------------
    @Override
    public Object getGroup(int groupPosition) {
        return groupsItemData[groupPosition];
    }

    @Override
    public int getGroupCount() {
        return groupsItemData.length;
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        ViewHolder holder;
        Utils utils 		= new Utils(context);
        String groupitem 	= getGroup(groupPosition).toString();

        if (convertView == null) {
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_m_dtl_group, parent, false);
            holder 		= new ViewHolder();
            holder.group_textview = (TextView) convertView.findViewById(R.id.group_textview);
            holder.group_textview.setTypeface(utils.setFontType("ChampagneLimousines", "BOLD"));
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        holder.group_textview.setText(groupitem);
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    private class ViewHolder {
        private TextView group_textview;
        private TextView child_textview;
        private RelativeLayout child_layoutchild;
    }
}